package com.n4.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;

import org.bson.types.ObjectId;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

public class GridFS {
	private static Mongo mongo;
	static {
		try {
			mongo = new Mongo("localhost", 27017);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	private static DB db = mongo.getDB("dox");

	public static ObjectId saveFile(CommonsMultipartFile cmf, String collection) {
		ObjectId objectId = null;

		try {
			objectId = saveFile(cmf.getInputStream(), cmf.getOriginalFilename(), collection);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return objectId;
	}

	public static ObjectId saveFile(InputStream is, String fileName, String collection) {
		com.mongodb.gridfs.GridFS gridFs = new com.mongodb.gridfs.GridFS(db, collection);
		GridFSInputFile gridFsInputFile = gridFs.createFile(is);
		gridFsInputFile.setFilename(fileName);
		gridFsInputFile.save();

		return (ObjectId) gridFsInputFile.getId();
	}

	public static InputStream getInputStream(ObjectId id, String collection) {
		com.mongodb.gridfs.GridFS gridFs = new com.mongodb.gridfs.GridFS(db, collection);
		return gridFs.findOne(id).getInputStream();
	}

	public static GridFSDBFile getGridFSDBFile(ObjectId id, String collection) {
		com.mongodb.gridfs.GridFS gridFs = new com.mongodb.gridfs.GridFS(db, collection);
		return gridFs.findOne(id);
	}

	public static void writeOut(ObjectId id, String dest, String collection) {
		com.mongodb.gridfs.GridFS gridFs = new com.mongodb.gridfs.GridFS(db, collection);
		try {
			gridFs.findOne(id).writeTo(dest);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

	}
}
