package com.n4.utils.reports;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gson.Gson;
import com.n4.model.view.reports.Admin;
import com.n4.model.view.reports.AltReporting;
import com.n4.model.view.reports.ReportingList;
import com.n4.utils.Database;

/**
 * Some payers require that reports get sent to alternate location for
 * processing. i.e. First Group NY requires the report gets sent to NY E. Filing
 * board.
 * 
 * @author hoppho
 *
 */
public class AltEmailReporting {
	Gson gson = new Gson();
	LinkedHashMap<Integer, String> es = new LinkedHashMap<Integer, String>();
	public static void main(String[] args) {
		AltEmailReporting AltEmailReporting = new AltEmailReporting();
		
		
		AltEmailReporting.generateReport(AltEmailReporting.getReport().getAltReporting());
		
	}
	private void getEncounterStatus(){
		es.put(1, "Active");
		es.put(9, "In billing");
		es.put(6, "Closed");
	}
	private void generateReport(List<AltReporting> arList) {
		DataOutputStream out = null;
		getEncounterStatus();
		try {
//			out = new DataOutputStream(new FileOutputStream("/reporting/SpecialPayerReporting.csv"));
			out = new DataOutputStream(new FileOutputStream("/Users/hoppho/Desktop/report/SpecialPayerReporting.csv"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			out.writeBytes("Scanpass,Encounter Status, Receive Date, Payer Name, Status\n");
			System.out.println(arList.size());
			int c = 1;
			for (AltReporting ar : arList) {
				checkReport(ar);
				
				if (!ar.getIsGood()) {
					out.writeBytes(ar.getScanpass()+","+es.get(ar.getEncounterstatusid())+",\""+ar.getReceivedate()+"\","+"\""+ar.getPayername()+"\","+ar.getStatus()+"\n");
							//",=HYPERLINK(\"https://secure.nextimagemedical.com:8443/winstaff/nim3/tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID="+ar.getCaseid()+"&SelectedEncounterID="+ar.getEncounterid()+"&KM=p+"\")\n");
					//tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID="+ar.getCaseid()+"&SelectedEncounterID="+ar.getEncounterid()+"&KM=p
				}
				System.out.println(c++);
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Boolean checkReport(AltReporting ar) {
		List<Integer> CMI_Walmart = Arrays.asList(101, 105);
		List<Integer> Pegasus = Arrays.asList(1622);
		List<Integer> First_Group_NY = Arrays.asList(1682);
		List<Integer> Johns_Eastern = Arrays.asList(264);
		List<Integer> First_Group = Arrays.asList(1689,1678,1670,1675,1677,1685,1687,1688,1686,1674,1683,1680,1681,1684,1671,1676,1679,1690);
		List<Integer> Concentra = Arrays.asList(472, 1358, 1366, 1367, 1368, 1365);
		List<Integer> USHW = Arrays.asList(1701, 1299, 1232, 1700, 1257, 1703, 1247, 1702, 1093, 1094, 1265, 839, 1256, 1262, 1091, 1263, 1238, 1230, 1228, 1229, 1231, 1233, 1272, 1235, 1237, 1239, 1240, 1244, 1245, 1267, 1261, 1264, 1260, 1271, 1270, 1268, 1269, 1276, 1258, 1250, 1251, 1252, 1253, 1255, 1259, 1254, 1283, 1275, 1236, 1274, 1277, 1279, 1280, 1278, 1292, 1248, 1290, 1291, 1294, 1295, 1297, 1296, 1298, 1301, 1300, 1289, 1309, 1242, 1282, 1096, 1243, 1704, 1017, 1241, 1095, 1273,
				1234, 1528, 1546, 1293, 1281, 1246, 1548, 1557, 1560, 1561, 1562, 1559, 1551, 1552, 1553, 1555, 1556, 1554, 1563, 1564, 1565, 1558, 1576, 1581, 1582, 1580, 1583, 1584, 1586, 1585, 1587, 1588, 1589, 1590, 1591, 1592, 1593, 1594, 1595, 1596, 1597, 1092, 1249, 1266, 1502, 1284, 1710, 1665);

		Integer CMI_Walmart_User = 33121;
		Integer Pegasus_User = 33324;
		Integer NY_E_Filing = 37359;
		Integer First_Group_User = 37641;
		Integer Johns_Eastern_User = 33522;
		List<Integer> Concentra_User = Arrays.asList(27823,27825,27821,27824,16740,27822);
		List<Integer> USHW_User = Arrays.asList(25210,25213,25209,25216,25214,25231,25225,25270,25267,25279,25280,25268,25197,25649,25647,25648,25652,25653,25656,25277,25199,25203,37730,37732,33394,37734,25218,25219,25272,25202,25195,25262,25207,25205,25211,25208,32042,32610,33025,25171,25206,25184,25189,25192,25196,25204,25221,25220,25215,25226,25223,25224,25230,25237,25259,25260,25263,25266,25271,25275,25273,25276,25269,25274,25281,25646,25651,25650,25654,25657,25655,25658,25848,25278,33222,25282,25261,33385,33386,33387,33390,33388,25882,33396,33395,33397,33398,33399,33400,33401,25187,25236,25198,25264,33899,33974,33973,33978,33983,33984,33986,33985,33987,33988,33990,33991,33996,33997,33998,33999,34000,33995,19370,33989,25201,25222,25212,25265,25241,36710,38168);

		Boolean sentRp = false;
		Boolean hasUser = false;
		try {
			if (CMI_Walmart.contains(ar.getPayerid())) {
				for(Admin admin : ar.getAdmins()){
					if(CMI_Walmart_User.equals(admin.getUserid())){
						hasUser = true;
						if(admin.getSentreport()){
							sentRp = true;
							break;
						}
					}
				}
			} else if(Pegasus.contains(ar.getPayerid())){
				for(Admin admin : ar.getAdmins()){
					if(Pegasus_User.equals(admin.getUserid())){
						hasUser = true;
						if(admin.getSentreport()){
							sentRp = true;
							break;
						}
					}
				}
			} else if(First_Group_NY.contains(ar.getPayerid())){
				for(Admin admin : ar.getAdmins()){
					if(NY_E_Filing.equals(admin.getUserid())){
						for(Admin admin2 : ar.getAdmins()){
							if(First_Group_User.equals(admin2.getUserid())){
								hasUser = true;
								if(admin.getSentreport() && admin2.getSentreport()){
									sentRp = true;
									break;
								}
							}
						}
					}
				}
			} else if(Johns_Eastern.contains(ar.getPayerid())){
				for(Admin admin : ar.getAdmins()){
					if(Johns_Eastern_User.equals(admin.getUserid())){
						hasUser = true;
						if(admin.getSentreport()){
							sentRp = true;
							break;
						}
					}
				}
			} else if(First_Group.contains(ar.getPayerid())){
				for(Admin admin : ar.getAdmins()){
					if(First_Group_User.equals(admin.getUserid())){
						hasUser = true;
						if(admin.getSentreport()){
							sentRp = true;
							break;
						}
					}
				}
			} else if(Concentra.contains(ar.getPayerid())){
				for(Admin admin : ar.getAdmins()){
					if(Concentra_User.contains(admin.getUserid())){
						hasUser = true;
						if(admin.getSentreport()){
							sentRp = true;
							break;
						}
					}
				}
			} else if(USHW.contains(ar.getPayerid())){
				for(Admin admin : ar.getAdmins()){
					if(USHW_User.contains(admin.getUserid())){
						hasUser = true;
						if(admin.getSentreport()){
							sentRp = true;
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(!hasUser){
			ar.setStatus("No reporting user linked to case.");
		} else if(!sentRp){
			ar.setStatus("Report has not been sent.");
		} else if (hasUser && sentRp){
			ar.setStatus("All good");
		}
		
		ar.setIsGood((hasUser && sentRp));
		return sentRp;
	}

	private ReportingList getReport() {
		Database db = new Database(Database.RW);
		ResultSet rs = db.getRs(getQuery());
//		System.out.println(getQuery());
//		List<AltReporting> arList = new ArrayList<AltReporting>();
		ReportingList rl = new ReportingList();
		try {
			while (rs.next()) {
//				System.out.println(rs.getString(1));
				rl = gson.fromJson(rs.getString(1), ReportingList.class);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rl;

	}

	private String getQuery() {
		return "SELECT\n" +
				"	row_to_json (results) altReporting\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			array_to_json (\n" +
				"				ARRAY_AGG (row_to_json(results))\n" +
				"			) altReporting\n" +
				"		FROM\n" +
				"			(\n" +
				"				SELECT\n" +
				"					ca.caseid,\n" +
				"					en.encounterid,\n" +
				"					en.scanpass,\n" +
				"					en.encounterstatusid,\n" +
				"					to_char(rf.receivedate, 'Mon dd, yyyy') receivedate,\n" +
				"					pm.payerid,\n" +
				"					pm.parentpayerid,\n" +
				"					pm.payername,\n" +
				"					(\n" +
				"						SELECT\n" +
				"							array_to_json (\n" +
				"								ARRAY_AGG (row_to_json(ua_json))\n" +
				"							) admins\n" +
				"						FROM\n" +
				"							(\n" +
				"								SELECT\n" +
				"									ca.adjusterid userid,\n" +
				"									CASE\n" +
				"								WHEN to_char(en.SentToAdj, 'mmddyyyy') != '01011800' THEN\n" +
				"									TRUE\n" +
				"								ELSE\n" +
				"									FALSE\n" +
				"								END sentreport\n" +
				"								UNION ALL\n" +
				"									SELECT\n" +
				"										ca.nursecasemanagerid userid,\n" +
				"										CASE\n" +
				"									WHEN to_char(en.SentTo_RP_NCM, 'mmddyyyy') != '01011800' THEN\n" +
				"										TRUE\n" +
				"									ELSE\n" +
				"										FALSE\n" +
				"									END sentreport\n" +
				"									UNION ALL\n" +
				"										SELECT\n" +
				"											rf.referringphysicianid userid,\n" +
				"											CASE\n" +
				"										WHEN to_char(en.SentToRefDr, 'mmddyyyy') != '01011800' THEN\n" +
				"											TRUE\n" +
				"										ELSE\n" +
				"											FALSE\n" +
				"										END sentreport\n" +
				"										UNION ALL\n" +
				"											SELECT\n" +
				"												ca.caseadministratorid userid,\n" +
				"												CASE\n" +
				"											WHEN to_char(en.SentToAdm, 'mmddyyyy') != '01011800' THEN\n" +
				"												TRUE\n" +
				"											ELSE\n" +
				"												FALSE\n" +
				"											END sentreport\n" +
				"											UNION ALL\n" +
				"												SELECT\n" +
				"													ca.caseadministrator2id userid,\n" +
				"													CASE\n" +
				"												WHEN to_char(en.SentTo_RP_Adm2, 'mmddyyyy') != '01011800' THEN\n" +
				"													TRUE\n" +
				"												ELSE\n" +
				"													FALSE\n" +
				"												END sentreport\n" +
				"												UNION ALL\n" +
				"													SELECT\n" +
				"														ca.caseadministrator3id userid,\n" +
				"														CASE\n" +
				"													WHEN to_char(en.SentTo_RP_Adm3, 'mmddyyyy') != '01011800' THEN\n" +
				"														TRUE\n" +
				"													ELSE\n" +
				"														FALSE\n" +
				"													END sentreport\n" +
				"													UNION ALL\n" +
				"														SELECT\n" +
				"															ca.caseadministrator4id userid,\n" +
				"															CASE\n" +
				"														WHEN to_char(en.SentTo_RP_Adm4, 'mmddyyyy') != '01011800' THEN\n" +
				"															TRUE\n" +
				"														ELSE\n" +
				"															FALSE\n" +
				"														END sentreport\n" +
				"							) ua_json\n" +
				"						WHERE\n" +
				"							ua_json.userid != 0\n" +
				"					)\n" +
				"				FROM\n" +
				"					tnim3_encounter en\n" +
				"				JOIN tnim3_referral rf ON rf.referralid = en.referralid\n" +
				"				JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"				JOIN tnim3_payermaster pm ON pm.payerid = ca.payerid\n" +
				"				WHERE\n" +
				"					(\n" +
				"						pm.payerid IN (101, 151) -- CMI Walmart \n" +
				"						OR pm.payerid IN (1682) -- First Group NY\n" +
				"						OR pm.payerid IN (264) -- Jonhs Eastern\n" +
				"						OR pm.payerid IN (1622) -- Pegasus Care West\n" +
				"						OR pm.payerid IN (\n" +
				"							SELECT\n" +
				"								payerid\n" +
				"							FROM\n" +
				"								tnim3_payermaster\n" +
				"							WHERE\n" +
				"								LOWER (payername) ~ 'us healthworks'\n" +
				"							OR LOWER (payername) ~ 'concentra'\n" +
				"						) -- USHW / Concentra\n" +
				"					)\n" +
				"				AND en.encounterstatusid != 5\n" +
				"			) results\n" +
				"	) results";
	}
}
