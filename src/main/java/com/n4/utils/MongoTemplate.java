package com.n4.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;

import com.n4.config.MongoConfiguration;
/**
 * Do not modify.
 * @author hoppho
 *
 */
public interface MongoTemplate {
	ApplicationContext ctx = new AnnotationConfigApplicationContext(MongoConfiguration.class);
    MongoOperations mongoOperation = (MongoOperations)ctx.getBean("mongoTemplate");
}
