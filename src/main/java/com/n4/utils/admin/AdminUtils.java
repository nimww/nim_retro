package com.n4.utils.admin;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.n4.model.view.admin.UserView;
import com.n4.security.Encrypt;
import com.n4.utils.Database;

public class AdminUtils {

	public UserView checkAuth(String name, String password) {

		Database db = new Database();
		String query = "SELECT\n" + "	userid,\n" + "	contactfirstname || ' ' || contactlastname \"name\",\n" + "	contactfirstname firstname,\n" + "	contactlastname lastname,\n" + "	contactphone phone,\n" + "	contactfax fax,\n" + "	contactemail email,\n" + "	status,\n" + "	accounttype\n" + "FROM\n" + "	tuseraccount\n" + "WHERE lower(logonusername) = '" + name.toLowerCase() + "' and logonuserpassword = '" + Encrypt.getMD5Base64(password) + "'";
		Integer status = 0;
		Integer userid = 0;
		String role = "";
		String iname = "";
		String firstname = "";
		String lastname = "";
		String phone = "";
		String fax = "";
		String email = "";
		ResultSet rs = db.getRs(query);

		try {
			while (rs.next()) {
				userid = rs.getInt("userid");
				status = rs.getInt("status");
				role = rs.getString("accounttype");
				iname = rs.getString("name");
				firstname = rs.getString("firstname");
				lastname = rs.getString("lastname");
				phone = rs.getString("phone");
				fax = rs.getString("fax");
				email = rs.getString("email");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		UserView uv = new UserView();

		uv.setAuth((status != 0));
		uv.setId(userid);
		uv.setRole(role);
		uv.setName(iname);
		uv.setFirstname(firstname);
		uv.setLastname(lastname);
		uv.setPhone(phone);
		uv.setFax(fax);
		uv.setEmail(email);

		return uv;
	}

	public UserView getUA(Integer id) {

		Database db = new Database();
		String query = "select userid, status, accounttype, contactfirstname||' '||contactlastname from tuseraccount where userid = " + id;
		Integer status = 0;
		Integer userid = 0;
		String role = "";
		String name = "403";
		ResultSet rs = db.getRs(query);

		try {
			while (rs.next()) {
				userid = rs.getInt(1);
				status = rs.getInt(2);
				role = rs.getString(3);
				name = rs.getString(4);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		UserView uv = new UserView();

		uv.setAuth((status != 0));
		uv.setId(userid);
		uv.setRole(role);
		uv.setName(name);
		return uv;
	}

	public String getAllUa() {
		Database db = new Database();
		String query = "SELECT\n" + "	array_to_json (\n" + "		ARRAY_AGG (row_to_json(doc_json))\n" + "	)\n" + "FROM\n" + "	(\n" + "		SELECT\n" + "			(\n" + "				SELECT\n" + "					row_to_json (ua_json)\n" + "				FROM\n" + "					(SELECT ua.*) ua_json\n" + "			) useraccount,\n" + "			(\n" + "				SELECT\n" + "					row_to_json (pm_json)\n" + "				FROM\n" + "					(SELECT pm.*) pm_json\n" + "			) payeraccount\n" + "		FROM\n" + "			tuseraccount ua\n"
				+ "		JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" + "		WHERE\n" + "			accounttype IN (\n" + "				'AdjusterID1',\n" + "				'AdjusterID2',\n" + "				'AdjusterID3',\n" + "				'PhysicianID1',\n" + "				'PhysicianID2',\n" + "				'PhysicianID3'\n" + "			)\n" + "	) doc_json";

		ResultSet rs = db.getRs(query);

		try {
			while (rs.next()) {
				return rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void main(String[] args) {

	}

}
