package com.n4.utils.netdev;

import java.text.SimpleDateFormat;
import java.util.Collections;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.google.gson.Gson;
import com.n4.model.collection.FS;
import com.n4.model.collection.RegionCode;
import com.n4.model.collection.Zip;
import com.n4.model.collection.support.FeeData;
import com.n4.utils.MongoTemplate;

public class FSUtils {
	
	public FS getFS(String rc, Integer fsid, String mod, String cpt){
		String q = "fee."+rc+"."+fsid+"."+mod+"."+cpt;

		FS fs = MongoTemplate.mongoOperation.findOne(new Query().addCriteria(Criteria.where(q).exists(true)), FS.class);
		
//		if(fs==null){
//			System.out.println("null");
//		} else
//		System.out.println(fs.getFee().get(rc).get(fsid).get(mod).get(cpt).get(fs.getFee().get(rc).get(fsid).get(mod).get(cpt).size()-1).getFee());
		
		return fs;
	}
	
	public Double getFee(String rc, Integer fsid, String mod, String cpt, String effDate){
		String q = "fee."+rc+"."+fsid+"."+mod+"."+cpt;
		System.out.println(q);
		
		Double fee = null;
		FS fs = MongoTemplate.mongoOperation.findOne(new Query().addCriteria(Criteria.where(q).exists(true)), FS.class);
		
		try {
			if(effDate==null){
				fee = fs.getFee().get(rc).get(fsid.toString()).get(mod).get(cpt).get(0).getFee();
				//System.out.println("no effDate fee:" + fee);
			} else{
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				Collections.sort(fs.getFee().get(rc).get(fsid.toString()).get(mod).get(cpt),new FeeData());
				for(FeeData feedata : fs.getFee().get(rc).get(fsid.toString()).get(mod).get(cpt)){
					if(sdf.parse(effDate).after(feedata.getEffDate())){
						fee = feedata.getFee();
						//System.out.println("effDate compare: "+fee+" date: "+feedata.getEffDate());
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fee;
	}
	
	public String getRc(String zip, Integer fsid){
		Query query = new Query();
		query.addCriteria(Criteria.where("fsid").is(fsid));
		query.addCriteria(Criteria.where("zip").is(zip));
		String rc = null;
		try {
			rc = MongoTemplate.mongoOperation.findOne(query, RegionCode.class).getRc();
		} catch (Exception e) {
		}
		try{
			if(rc==null){
				rc = MongoTemplate.mongoOperation.findOne(new Query().addCriteria(Criteria.where("zip").is(zip)), Zip.class).getState();
			}
		} catch (Exception e) {
		}
		
		
		return rc;
	}
	
	public String checkError(String zip,Integer fsid,String mod,String cpt,String effDate){
		
		if(getRc( zip,  fsid)==null){
			return "Bad Zip";
		} else if(!checkFSID(getRc( zip,  fsid),fsid, mod, cpt).isEmpty()){
			return checkFSID(getRc( zip,  fsid),fsid, mod, cpt);
		} 
		return null;
	}
	
	private String checkFSID(String rc, Integer fsid, String mod, String cpt){
		String q = "fee."+rc+"."+fsid;
		
		String res = "";
		boolean isMod = false;
		boolean isFsid = false;
		boolean isCpt = false;
		
		if(MongoTemplate.mongoOperation.findOne(new Query().addCriteria(Criteria.where(q).exists(true)), FS.class)!=null){
			//return "Bad Fee Schedule ID";
			System.out.println("checkFSID "+q);
			isFsid = true;
		}
		q = "fee."+rc+"."+fsid+"."+mod;
		
		if(MongoTemplate.mongoOperation.findOne(new Query().addCriteria(Criteria.where(q).exists(true)), FS.class)!=null){
			//return "Bad Modifier";
			System.out.println("checkFSID "+q);
			isMod = true;
		} 
		
		q = "fee."+rc+"."+fsid+"."+mod+"."+cpt;
		
		if(MongoTemplate.mongoOperation.findOne(new Query().addCriteria(Criteria.where(q).exists(true)), FS.class)!=null){
			//return "Bad CPT";
			System.out.println("checkFSID "+q);
			isCpt = true;
		} 
		if(!isCpt){
			res = "Bad CPT";
		}
		if(isFsid && !isMod){
			res = "Bad Modifier";
		}
		if (!isFsid){
			res = "Bad Fee Schedule ID";
		}
		return res;
	
	}
	public static void main(String[] args) {
		FSUtils FSUtils = new FSUtils();
		String zip = "92126";
		Integer fsid = 2;
		String mod = "Global";
		String cpt = "95904";
		String rc = FSUtils.getRc(zip, fsid);
		
		System.out.println(FSUtils.getFS(rc, fsid, mod, cpt).getFee().get(rc).get(fsid.toString()).get(mod).get(cpt).get(0).getFee());
	}
}
