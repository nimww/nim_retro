package com.n4.utils.netdev;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.GeoResult;
import org.springframework.data.mongodb.core.geo.GeoResults;
import org.springframework.data.mongodb.core.geo.Metrics;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.mongodb.core.index.GeospatialIndex;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.n4.model.api.geocode.Geocode;
import com.n4.model.collection.Practice;
import com.n4.model.collection.support.BackupPricing;
import com.n4.model.collection.support.ModalPricing;
import com.n4.model.collection.support.Modality;
import com.n4.model.enums.ContractStatus;
import com.n4.model.view.netdev.Cords;
import com.n4.model.view.netdev.Cpt;
import com.n4.model.view.netdev.FS;
import com.n4.model.view.netdev.Gmap;
import com.n4.model.view.netdev.Markers;
import com.n4.model.view.netdev.ModalManager;
import com.n4.model.view.netdev.PracticeAdjView;
import com.n4.model.view.netdev.PracticeQuery;
import com.n4.model.view.netdev.PracticeView;
import com.n4.utils.Database;
import com.n4.utils.MongoTemplate;

public class NetdevUtils {
	public final List<String> inNetwork = Arrays.asList(ContractStatus.Contracted.toString(), ContractStatus.OTA.toString(), ContractStatus.ContractedSelect.toString());
	private final String API_KEY = "AIzaSyDaLVwN2ytziCPv7CvTFgFaY_6vHJgwRIU";
	private Gson gson = new Gson();
	Geocode geocode = null;
	private LinkedHashMap<String, ModalManager> modalManager = getModalManager();
	private LinkedHashMap<String, com.n4.model.collection.FS> fs = new LinkedHashMap<String,  com.n4.model.collection.FS>();
	public List<Markers> getMarkers(List<PracticeView> pvList){
		List<Markers> markersList = new ArrayList<Markers>();
		for(PracticeView pv : pvList){
			Markers markers = new Markers();
			Cords cords = new Cords();
			cords.setLongitude(pv.getPractice().getGeocode()[0]);
			cords.setLatitude(pv.getPractice().getGeocode()[1]);
			markers.setCords(cords);
			markers.setCompany(pv.getPractice().getContact().getCompany());
			markers.setId(pv.getPractice().getPracticeid());
			markersList.add(markers);
		}
		
		return markersList;
	}
	public Markers getMarker(PracticeView pv){
			Markers markers = new Markers();
			Cords cords = new Cords();
			cords.setLongitude(pv.getPractice().getGeocode()[0]);
			cords.setLatitude(pv.getPractice().getGeocode()[1]);
			markers.setCords(cords);
			markers.setCompany(pv.getPractice().getContact().getCompany());
			markers.setId(pv.getPractice().getPracticeid());
		return markers;
	}
	
	public List<PracticeView> getPractice(Integer miles, Integer eId) {
		PracticeQuery pq = getEnPractice(eId);
		getPayerAllow(pq);
		return getPractice(miles, pq);
	}
	
	public List<PracticeView> getPractice(Integer miles, PracticeQuery pq) {
		MongoTemplate.mongoOperation.indexOps(Practice.class).ensureIndex(new GeospatialIndex("geocode"));
		
		System.out.println("Payer Allow: " + pq.getPayerallow());
		System.out.println("Center Address: " + pq.getFulladdress());

		
		try {
			geocode = getGeocode(pq.getFulladdress()+" "+pq.getZip());
			System.out.println(geocode.getResults().get(0).getGeometry().getLocation().getLat()+" "+geocode.getResults().get(0).getGeometry().getLocation().getLng());
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
		List<Criteria> orCriteriaList = new ArrayList<Criteria>();
		for (Cpt cpt : pq.getCpts()) {
			orCriteriaList.add(Criteria.where("modality." + cpt.getModality()).exists(true));
		}
		Query query = new Query();
		query.addCriteria(new Criteria().orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()])));
		query.addCriteria(Criteria.where("contractStatus").in(inNetwork));

		System.out.println(query);

		Point point = new Point(geocode.getResults().get(0).getGeometry().getLocation().getLng(), geocode.getResults().get(0).getGeometry().getLocation().getLat());
		// Point point = new
		// Point(-78.785663900000002968d,35.739719200000003241d);
		NearQuery nearQuery = NearQuery.near(point, Metrics.MILES).maxDistance(miles, Metrics.MILES).distanceMultiplier(Metrics.MILES).spherical(true).query(query);

		// System.out.println(nearQuery);
		GeoResults<Practice> geoResults = MongoTemplate.mongoOperation.geoNear(nearQuery, Practice.class);
		System.out.println("results: "+geoResults.getContent().size());
		List<PracticeView> pvList = new ArrayList<PracticeView>();
		for (GeoResult<Practice> result : geoResults.getContent()) {
			PracticeView pv = new PracticeView();
			pv.setPractice(result.getContent());
			pv.setDistance(result.getDistance());
			pv.setCost(getPracticeCost(pq, pv));
			pv.setMacScore(getMacScore(result.getDistance(), pq.getPayerallow().doubleValue(), pv.getCost().doubleValue()));
			Markers marker = getMarker(pv);
			pv.setId(marker.getId());
			pv.setCords(marker.getCords());
			pv.setCompany(marker.getCompany());
			pvList.add(pv);
		}
		Collections.sort(pvList, new PracticeView());
		
		
		
		return pvList;
	}

	public PracticeAdjView getPracticeAdjView(String address, String zip){
		PracticeAdjView practiceAdjView = new PracticeAdjView();
		
		PracticeQuery pq = new PracticeQuery();
		
		pq.setFulladdress(address);
		pq.setZip(zip);
		pq.setPayerallow(999d);
		Cpt cpt = new Cpt();
		cpt.setCpt("72148");
		cpt.setQty(1);
		cpt.setMod("");
		cpt.setModality("MRI");
		pq.setCpts(Arrays.asList(cpt));
		practiceAdjView.setResults(getPractice(50, pq));
		
		Gmap gmap = new Gmap();
		Cords center = new Cords();
		center.setLatitude(geocode.getResults().get(0).getGeometry().getLocation().getLat());
		center.setLongitude(geocode.getResults().get(0).getGeometry().getLocation().getLng());
		practiceAdjView.setMarkers(getMarkers(practiceAdjView.getResults()));
		
		gmap.setCenter(center);
		gmap.setZoom(9);
		practiceAdjView.setGmap(gmap);
		
		return practiceAdjView;
	}
	
	
	private String getMacScore(Distance distance, double payerAllow, double practiceCost) {
		//System.out.println(distance.getValue() + " "+ payerAllow + " " + practiceCost);
		
		Double macScore = 0d;
		double maxDistance = 60d;
		Integer minPrice = 150;
		Integer Weight_Price = 8;
		//Integer Weight_BlueStar = 5;
		Integer Weight_LocalCongestion = 3;
		double bestScore = (1 * Weight_LocalCongestion) + (1 * Weight_Price);
		NumberFormat numberformat = new DecimalFormat("#0");    
		practiceCost = (practiceCost<minPrice&&practiceCost>0 ? minPrice:practiceCost);
		practiceCost = (practiceCost>payerAllow ? payerAllow:practiceCost);
		practiceCost = (practiceCost<0.01 ? payerAllow:practiceCost);
		Weight_LocalCongestion = (practiceCost<1 ? 1:Weight_LocalCongestion);
		
		macScore = ((1-(distance.getValue()/maxDistance)) * Weight_LocalCongestion) + ((1-((practiceCost-minPrice)/(payerAllow-minPrice))) * Weight_Price);
		
		return numberformat.format((macScore/bestScore)*10000);
	}

	private Double getPracticeCost(PracticeQuery pq, PracticeView pv) {
		//System.out.println("Get PracticeCost for "+pv.getPractice().getPracticename());
		Double practiceCost = 0d;
		FSUtils fsUtils = new FSUtils();
		System.out.println("\tGet Modal Price: ");
		for (Cpt cpt : pq.getCpts()) {
			ModalManager modalManager = this.modalManager.get(cpt.getCpt());
			try {
				Class modalPricing = Class.forName(ModalPricing.class.getName());
				cpt.setPracticeCost((Double) modalPricing.getMethod("get" + modalManager.getContrast()).invoke(pv.getPractice().getModalPricing().get(modalManager.getModal()), null) * cpt.getQty());
				System.out.println("\t\tCPT: "+cpt.getCpt()+" : "+cpt.getPracticeCost());
				practiceCost += cpt.getPracticeCost();
				
			} catch (Exception e) {
				cpt.setPracticeCost(0d);
			}
		}
		System.out.println("\tGet FS price: ");
		for (Cpt cpt : pq.getCpts()) {
			if(cpt.getPracticeCost() == 0d){
				for (BackupPricing bup : pv.getPractice().getBackupPricing()) {
					String rc = fsUtils.getRc(pq.getZip(), bup.getFsId());
					Integer fsid = bup.getFsId();
					String mod = cpt.getMod();
					String cptCode = cpt.getCpt();
					try {
						cpt.setPracticeCost((fs.get(rc).getFee().get(rc).get(fsid.toString()).get(mod).get(cptCode).get(0).getFee() * bup.getFsPercentage()) * cpt.getQty());
						System.out.println("\t\tCPT: "+cpt.getCpt()+" : "+cpt.getPracticeCost());
						practiceCost += cpt.getPracticeCost();
						break;
					} catch (Exception e) {
						System.out.println("\t\t** FS: recalc **");
						fs.put(rc, fsUtils.getFS(rc, fsid, mod, cptCode));
						if (fs.get(rc) != null) {
							cpt.setPracticeCost((fs.get(rc).getFee().get(rc).get(fsid.toString()).get(mod).get(cptCode).get(0).getFee() * bup.getFsPercentage()) * cpt.getQty());
							System.out.println("\t\tCPT: "+cpt.getCpt()+" : "+cpt.getPracticeCost());
							practiceCost += cpt.getPracticeCost();
							break;
						}
					}
					
				}
				if(cpt.getPracticeCost() == 0d){
					Integer fsid = 1;
					String rc = fsUtils.getRc(pq.getZip(), fsid);
					String mod = cpt.getMod();
					String cptCode = cpt.getCpt();
					Double percent = .75;
					try {
						cpt.setPracticeCost((fs.get(rc).getFee().get(rc).get(fsid.toString()).get(mod).get(cptCode).get(0).getFee() * percent) * cpt.getQty());
						System.out.println("\t\tCPT: "+cpt.getCpt()+" : "+cpt.getPracticeCost());
						practiceCost += cpt.getPracticeCost();
						break;
					} catch (Exception e) {
						System.out.println("\t\t** FS: recalc **");
						fs.put(rc, fsUtils.getFS(rc, fsid, mod, cptCode));
						if (fs.get(rc) != null) {
							cpt.setPracticeCost((fs.get(rc).getFee().get(rc).get(fsid.toString()).get(mod).get(cptCode).get(0).getFee() * percent) * cpt.getQty());
							System.out.println("\t\tCPT: "+cpt.getCpt()+" : "+cpt.getPracticeCost());
							practiceCost += cpt.getPracticeCost();
							break;
						}
					}
				}
			}
		}
		return practiceCost;
	}
	/**
	 * auto generated with modalmanger generator query
	 * @return
	 */
	private LinkedHashMap<String, ModalManager> getModalManager(){
		LinkedHashMap<String, ModalManager> modalManager = new LinkedHashMap<String, ModalManager>();
		
		modalManager.put("70450",new ModalManager("ct","WithOut"));
		modalManager.put("70460",new ModalManager("ct","With"));
		modalManager.put("70470",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("70480",new ModalManager("ct","WithOut"));
		modalManager.put("70481",new ModalManager("ct","With"));
		modalManager.put("70482",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("70486",new ModalManager("ct","WithOut"));
		modalManager.put("70487",new ModalManager("ct","With"));
		modalManager.put("70488",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("70490",new ModalManager("ct","WithOut"));
		modalManager.put("70491",new ModalManager("ct","With"));
		modalManager.put("70492",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("70496",new ModalManager("ct",""));
		modalManager.put("70498",new ModalManager("ct",""));
		modalManager.put("70540",new ModalManager("mri","WithOut"));
		modalManager.put("70542",new ModalManager("mri","With"));
		modalManager.put("70543",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("70544",new ModalManager("mri","WithOut"));
		modalManager.put("70545",new ModalManager("mri","With"));
		modalManager.put("70546",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("70547",new ModalManager("mri","WithOut"));
		modalManager.put("70548",new ModalManager("mri","With"));
		modalManager.put("70549",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("70551",new ModalManager("mri","WithOut"));
		modalManager.put("70552",new ModalManager("mri","With"));
		modalManager.put("70553",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("71250",new ModalManager("ct","WithOut"));
		modalManager.put("71260",new ModalManager("ct","With"));
		modalManager.put("71270",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("71275",new ModalManager("ct",""));
		modalManager.put("71550",new ModalManager("mri","WithOut"));
		modalManager.put("71551",new ModalManager("mri","With"));
		modalManager.put("71552",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("71555",new ModalManager("mri","With"));
		modalManager.put("72125",new ModalManager("ct","WithOut"));
		modalManager.put("72126",new ModalManager("ct","With"));
		modalManager.put("72127",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("72128",new ModalManager("ct","WithOut"));
		modalManager.put("72129",new ModalManager("ct","With"));
		modalManager.put("72130",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("72131",new ModalManager("ct","WithOut"));
		modalManager.put("72132",new ModalManager("ct","With"));
		modalManager.put("72133",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("72141",new ModalManager("mri","WithOut"));
		modalManager.put("72142",new ModalManager("mri","With"));
		modalManager.put("72146",new ModalManager("mri","WithOut"));
		modalManager.put("72147",new ModalManager("mri","With"));
		modalManager.put("72148",new ModalManager("mri","WithOut"));
		modalManager.put("72149",new ModalManager("mri","With"));
		modalManager.put("72156",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("72157",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("72158",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("72159",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("72191",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("72192",new ModalManager("ct","WithOut"));
		modalManager.put("72193",new ModalManager("ct","With"));
		modalManager.put("72194",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("72195",new ModalManager("mri","WithOut"));
		modalManager.put("72196",new ModalManager("mri","With"));
		modalManager.put("72197",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("72198",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("73200",new ModalManager("ct","WithOut"));
		modalManager.put("73201",new ModalManager("ct","With"));
		modalManager.put("73202",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("73206",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("73218",new ModalManager("mri","WithOut"));
		modalManager.put("73219",new ModalManager("mri","With"));
		modalManager.put("73220",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("73221",new ModalManager("mri","WithOut"));
		modalManager.put("73222",new ModalManager("mri","With"));
		modalManager.put("73223",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("73225",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("73700",new ModalManager("ct","WithOut"));
		modalManager.put("73701",new ModalManager("ct","With"));
		modalManager.put("73702",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("73706",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("73718",new ModalManager("mri","WithOut"));
		modalManager.put("73719",new ModalManager("mri","With"));
		modalManager.put("73720",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("73721",new ModalManager("mri","WithOut"));
		modalManager.put("73722",new ModalManager("mri","With"));
		modalManager.put("73723",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("73725",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("74150",new ModalManager("ct","WithOut"));
		modalManager.put("74160",new ModalManager("ct","With"));
		modalManager.put("74170",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("74174",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("74175",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("74176",new ModalManager("ct","WithOut"));
		modalManager.put("74177",new ModalManager("ct","With"));
		modalManager.put("74178",new ModalManager("ct","WithAndWithOut"));
		modalManager.put("74181",new ModalManager("mri","WithOut"));
		modalManager.put("74182",new ModalManager("mri","With"));
		modalManager.put("74183",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("74185",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("74261",new ModalManager("ct","With"));
		modalManager.put("74262",new ModalManager("ct","WithOut"));
		modalManager.put("75151",new ModalManager("mri","With"));
		modalManager.put("75152",new ModalManager("mri","WithAndWithOut"));
		modalManager.put("75557",new ModalManager("mri","WithOut"));
		modalManager.put("75559",new ModalManager("mri","WithOut"));
		modalManager.put("75561",new ModalManager("mri","With"));
		modalManager.put("75563",new ModalManager("mri","With"));
		modalManager.put("75574",new ModalManager("ct",""));
		modalManager.put("75635",new ModalManager("ct",""));
		modalManager.put("76516",new ModalManager("us","WithOut"));
		modalManager.put("76536",new ModalManager("us","WithOut"));
		modalManager.put("76604",new ModalManager("us","WithOut"));
		modalManager.put("76645",new ModalManager("us","WithOut"));
		modalManager.put("76700",new ModalManager("us","WithOut"));
		modalManager.put("76705",new ModalManager("echo",""));
		modalManager.put("76770",new ModalManager("us","WithOut"));
		modalManager.put("76775",new ModalManager("us",""));
		modalManager.put("76776",new ModalManager("us",""));
		modalManager.put("76800",new ModalManager("us","WithOut"));
		modalManager.put("76801",new ModalManager("us","WithOut"));
		modalManager.put("76802",new ModalManager("us","WithOut"));
		modalManager.put("76805",new ModalManager("us","WithOut"));
		modalManager.put("76816",new ModalManager("us","WithOut"));
		modalManager.put("76817",new ModalManager("us","WithOut"));
		modalManager.put("76818",new ModalManager("us",""));
		modalManager.put("76830",new ModalManager("us","WithOut"));
		modalManager.put("76831",new ModalManager("echo",""));
		modalManager.put("76856",new ModalManager("us","WithOut"));
		modalManager.put("76857",new ModalManager("us",""));
		modalManager.put("76870",new ModalManager("us",""));
		modalManager.put("76881",new ModalManager("us","WithOut"));
		modalManager.put("77058",new ModalManager("mri",""));
		modalManager.put("77059",new ModalManager("mri",""));
		modalManager.put("78814",new ModalManager("pet/ct","WithOut"));
		modalManager.put("78815",new ModalManager("pet/ct","WithAndWithOut"));
		modalManager.put("78816",new ModalManager("pet/ct","WithAndWithOut"));
		modalManager.put("93880",new ModalManager("us",""));
		modalManager.put("93925",new ModalManager("us","WithOut"));
		modalManager.put("93926",new ModalManager("us","WithOut"));
		modalManager.put("93970",new ModalManager("us","WithOut"));
		modalManager.put("93971",new ModalManager("us","WithOut"));
		modalManager.put("95860",new ModalManager("emg",""));
		modalManager.put("95861",new ModalManager("emg",""));
		modalManager.put("95863",new ModalManager("emg",""));
		modalManager.put("95864",new ModalManager("emg",""));
		modalManager.put("95867",new ModalManager("emg",""));
		modalManager.put("95868",new ModalManager("emg",""));
		modalManager.put("95869",new ModalManager("emg",""));
		modalManager.put("95900",new ModalManager("emg",""));
		modalManager.put("95903",new ModalManager("emg",""));
		modalManager.put("95904",new ModalManager("emg",""));
		modalManager.put("95934",new ModalManager("emg",""));
		modalManager.put("95936",new ModalManager("emg",""));
		modalManager.put("95937",new ModalManager("emg",""));
		modalManager.put("95999",new ModalManager("emg",""));
		
		return modalManager;
	}
	
	private void getPayerAllow(PracticeQuery pq) {
		System.out.println("Get Payer Allow");
		Double payerAllow = 0d;

		FSUtils fsUtils = new FSUtils();
		for (Cpt cpt : pq.getCpts()) {
			System.out.println("\tChecking CPT: "+cpt.getCpt());
			for (FS ifs : pq.getPayerfs()) {
				String rc = fsUtils.getRc(pq.getZip(), ifs.getFsid());
				Integer fsid = ifs.getFsid();
				String mod = cpt.getMod();
				String cptCode = cpt.getCpt();

				System.out.println("\t\t"+rc + "." + fsid + "." + mod + "." + cptCode);
				
				try {
					Double allow = (fs.get(rc).getFee().get(rc).get(fsid.toString()).get(mod).get(cptCode).get(0).getFee() * ifs.getFspercentage()) * cpt.getQty();
					payerAllow += allow;
					System.out.println("\t\t\tAllow: "+allow);
					break;
				} catch (Exception e) {
					fs.put(rc, fsUtils.getFS(rc, fsid, mod, cptCode));
					try {
						Double allow = (fs.get(rc).getFee().get(rc).get(fsid.toString()).get(mod).get(cptCode).get(0).getFee() * ifs.getFspercentage()) * cpt.getQty();
						payerAllow += allow;
						System.out.println("\t\t\tAllow: "+allow);
						break;
					} catch (Exception e2) {
					}
					
				}
			}
		}
		pq.setPayerallow(payerAllow);
	}

	private PracticeQuery getEnPractice(Integer eId) {
		Database db = new Database(Database.RW);
		// System.out.println(getEnPracticeQuery(eId));
		ResultSet rs = db.getRs(getEnPracticeQuery(eId));
		PracticeQuery pq = null;
		try {
			while (rs.next()) {
				pq = gson.fromJson(rs.getString(1), PracticeQuery.class);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pq;
	}

	public static void main(String[] args) {
		NetdevUtils NetdevUtils = new NetdevUtils();
		System.out.println(new Date());

		PracticeAdjView pvl = NetdevUtils.getPracticeAdjView("3390 carmel mt rd", "92121");
		
		System.out.println(new Gson().toJson(pvl));
		
		
		System.out.println(new Date());
	}

	private Geocode getGeocode(String address) throws MalformedURLException {
		String apiURL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(address) + "&key=" + API_KEY;
		// System.out.println(apiURL);
		URL url = new URL(apiURL);
		String json = "";
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
			for (String line; (line = reader.readLine()) != null;) {
				json += line;
			}
		} catch (Exception e) {

		}
		return gson.fromJson(json, Geocode.class);
	}

	private String getEnPracticeQuery(Integer eId) {
		return "SELECT\n" +
				"	row_to_json (encounter_json)\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			(\n" +
				"				SELECT\n" +
				"					array_to_json (\n" +
				"						ARRAY_AGG (row_to_json(pm_json))\n" +
				"					) payerfs\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							pm.contract1_feescheduleid fsid,\n" +
				"							pm.contract1_feepercentage fspercentage\n" +
				"						UNION ALL\n" +
				"						SELECT\n" +
				"							pm.contract2_feescheduleid fsid,\n" +
				"							pm.contract2_feepercentage fspercentage\n" +
				"						UNION ALL\n" +
				"						SELECT\n" +
				"							pm.contract3_feescheduleid fsid,\n" +
				"							pm.contract3_feepercentage fspercentage\n" +
				"						UNION ALL\n" +
				"						SELECT\n" +
				"							par.contract1_feescheduleid fsid,\n" +
				"							par.contract1_feepercentage fspercentage\n" +
				"						UNION ALL\n" +
				"						SELECT\n" +
				"							par.contract2_feescheduleid fsid,\n" +
				"							par.contract2_feepercentage fspercentage\n" +
				"						UNION ALL\n" +
				"						SELECT\n" +
				"							par.contract3_feescheduleid fsid,\n" +
				"							par.contract3_feepercentage fspercentage\n" +
				"					) pm_json\n" +
				"			),\n" +
				"			ca.patientaddress1 || ' ' || ca.patientcity || ' ' || st.shortstate || ' ' || ca.patientzip fulladdress, trim(ca.patientzip) zip,\n" +
				"			(\n" +
				"				SELECT\n" +
				"					array_to_json (\n" +
				"						ARRAY_AGG (row_to_json(service_json))\n" +
				"					) cpts\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							cpt, cptqty qty, \n" +
				"							CASE\n" +
				"							WHEN TRIM (sr.cptmodifier) = '26' THEN\n" +
				"								'26'\n" +
				"							WHEN LOWER (TRIM(sr.cptmodifier)) = 'tc' THEN\n" +
				"								'TC'\n" +
				"							ELSE\n" +
				"								'Global'\n" +
				"							END \"mod\",							\n" +
				"							UPPER (\n" +
				"								REPLACE (\n" +
				"									REPLACE (\n" +
				"										REPLACE (\n" +
				"											REPLACE (\n" +
				"												REPLACE (\n" +
				"													REPLACE (C .\"CPTGroup\", 'mr_wo', 'MRI'),\n" +
				"													'mr_w',\n" +
				"													'MRI'\n" +
				"												),\n" +
				"												'mr_wwo',\n" +
				"												'MRI'\n" +
				"											),\n" +
				"											'ct_wo',\n" +
				"											'CT'\n" +
				"										),\n" +
				"										'ct_w',\n" +
				"										'CT'\n" +
				"									),\n" +
				"									'ct_wwo',\n" +
				"									'CT'\n" +
				"								)\n" +
				"							) modality\n" +
				"						FROM\n" +
				"							tnim3_service sr\n" +
				"						JOIN \"CPTGroup\" C ON C .\"CPT\" :: VARCHAR = sr.cpt\n" +
				"						WHERE\n" +
				"							sr.encounterid = en.encounterid\n" +
				"							and sr.servicestatusid = 1\n" +
				"					) service_json\n" +
				"			)\n" +
				"		FROM\n" +
				"			tnim3_encounter en\n" +
				"		JOIN tnim3_referral rf ON rf.referralid = en.referralid\n" +
				"		JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"		JOIN tstateli st ON st.stateid = ca.patientstateid\n" +
				"		JOIN tnim3_payermaster pm ON pm.payerid = ca.payerid\n" +
				"		JOIN tnim3_payermaster par ON par.payerid = pm.parentpayerid\n" +
				"		WHERE\n" +
				"			en.encounterid = "+eId+"\n" +
				"	) encounter_json";
	}
}
