package com.n4.utils.accounting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.n4.model.view.accounting.PrePaymentWorklist;
import com.n4.utils.Database;
import com.n4.utils.MongoTemplate;
import com.n4.utils.Utils;

public class PrePaymentUtils extends Utils implements MongoTemplate {
	
	public List<PrePaymentWorklist> getWorklist(){
		String query = "SELECT\n" +
				" 	ca.caseid,\n" +
				"	ca.patientfirstname||' '||ca.patientlastname patient,\n" +
				"	caseclaimnumber,\n" +
				"	pm.payername,\n" +
				"	to_char(en.dateofservice, 'mm/dd/yyyy') dos,\n" +
				"	prc.practicename,\n" +
				"	'status' status\n" +
				"FROM\n" +
				"	tnim3_caseaccount ca\n" +
				"JOIN tnim3_payermaster pm ON pm.payerid = ca.payerid\n" +
				"JOIN tnim3_referral rf ON rf.caseid = ca.caseid\n" +
				"JOIN tnim3_encounter en ON en.referralid = rf.referralid\n" +
				"JOIN tnim3_appointment ap ON ap.appointmentid = en.appointmentid\n" +
				"AND en.appointmentid > 0\n" +
				"join tpracticemaster prc on prc.practiceid = ap.providerid\n" +
				"WHERE\n" +
				"	ca.payerid IN (\n" +
				"		SELECT\n" +
				"			payerid\n" +
				"		FROM\n" +
				"			tnim3_payermaster\n" +
				"		WHERE\n" +
				"			parentpayerid = 790\n" +
				"	)\n" +
				"limit 20";
		Database db = new Database();
		ResultSet rs = db.getRs(query);
		
		List<PrePaymentWorklist> PrePaymentWorklistList = new ArrayList<PrePaymentWorklist>();
		try {
			while(rs.next()){
				PrePaymentWorklist prePaymentWorklist = new PrePaymentWorklist();
				prePaymentWorklist.setCaseid(rs.getString("caseid"));
				prePaymentWorklist.setPatient(rs.getString("patient"));
				prePaymentWorklist.setClaim(rs.getString("caseclaimnumber"));
				prePaymentWorklist.setDos(rs.getString("dos"));
				prePaymentWorklist.setImageCenter(rs.getString("practicename"));
				prePaymentWorklist.setPayer(rs.getString("payername"));
				prePaymentWorklist.setStatus("");
				
				PrePaymentWorklistList.add(prePaymentWorklist);	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return PrePaymentWorklistList;
	}
}
