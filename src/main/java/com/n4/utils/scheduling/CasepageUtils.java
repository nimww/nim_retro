package com.n4.utils.scheduling;

import java.sql.ResultSet;
import java.util.List;

import com.google.gson.Gson;
import com.n4.model.view.scheduling.*;
import com.n4.utils.Database;
import com.n4.utils.MongoTemplate;
import com.n4.utils.Utils;

public class CasepageUtils extends Utils {
	private Gson gson = new Gson();
	
	
	public Caseaccount getCasepage(Integer cId, String key) {
		Caseaccount ca = null;
		if (verifyAPIkey(key)) {
			Database db = new Database();
			String query = getQueryFull(cId);
			ResultSet rs = db.getRs(query);
			try {
				while (rs.next()) {
					ca = gson.fromJson(rs.getString(1), Caseaccount.class);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			db.closeAll();
		}

		return ca;
	}
	
	public Caseaccount getCasepageLite(Integer cId, String key) {
		Caseaccount ca = null;
		if (verifyAPIkey(key)) {
			Database db = new Database();
			String query = getQueryLite(cId);
			ResultSet rs = db.getRs(query);
			try {
				while (rs.next()) {
					ca = gson.fromJson(rs.getString(1), Caseaccount.class);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			db.closeAll();
		}
		return ca;
	}
	
	private String getQueryFull(Integer cId){
		return "SELECT\n" +
				"	row_to_json (ca_json)\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			ca.caseid,\n" +
				"			ca.caseclaimnumber,\n" +
				"			ca.payerid,\n" +
				"			payer.payername,\n" +
				"			payer.comments payernotes,\n" +
				"			case when payer.parentpayerid = 951 then 'gh' else 'wc' end clienttype,\n" +
				"			to_char(ca.dateofinjury, 'mm-dd-yy') dateofinjury,\n" +
				"			to_char(ca.patientdob, 'mm-dd-yy') dateofbirth,\n" +
				"			ca.patientssn ssn,\n" +
				"			ca.patientgender gender,\n" +
				"			ca.injurydescription,\n" +
				"			ca.comments notes,\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (pt_json) patient\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							pt.patientfirstname \"firstname\",\n" +
				"							pt.patientlastname \"lastname\",\n" +
				"							(\n" +
				"								SELECT\n" +
				"									row_to_json (pt_addy_json) address\n" +
				"								FROM\n" +
				"									(\n" +
				"										SELECT\n" +
				"											pt_addy.patientaddress1 address,\n" +
				"											pt_addy.patientcity city,\n" +
				"											pt_addy_state.shortstate \"state\",\n" +
				"											pt_addy.patientzip zip\n" +
				"										FROM\n" +
				"											tnim3_caseaccount pt_addy\n" +
				"										JOIN tstateli pt_addy_state ON pt_addy_state.stateid = pt_addy.patientstateid\n" +
				"										WHERE\n" +
				"											pt_addy.caseid = ca.caseid\n" +
				"									) pt_addy_json\n" +
				"							),\n" +
				"							pt.patienthomephone \"phone\",\n" +
				"							pt.patientcellphone \"cellphone\",\n" +
				"							pt.patientemail \"email\"\n" +
				"						FROM\n" +
				"							tnim3_caseaccount pt\n" +
				"						WHERE\n" +
				"							pt.caseid = ca.caseid\n" +
				"					) pt_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (em_json) employer\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							em.employername company,\n" +
				"							em.employerphone phone,\n" +
				"							em.employerfax fax\n" +
				"						FROM\n" +
				"							tnim3_caseaccount em\n" +
				"						WHERE\n" +
				"							em.caseid = ca.caseid\n" +
				"					) em_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) assignedTo\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.assignedtoid\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) adjuster\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.adjusterid\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) nursecasemanager\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.nursecasemanagerid\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) caseAdministrator1\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.caseAdministratorid\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) caseAdministrator2\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.caseAdministrator2id\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) caseAdministrator3\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.caseAdministrator3id\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) caseAdministrator4\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.caseAdministrator4id\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (prescreen_json) prescreen\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							CASE\n" +
				"						WHEN ps.patientisclaus = 1 THEN\n" +
				"							TRUE\n" +
				"						ELSE\n" +
				"							FALSE\n" +
				"						END claustrophobic,\n" +
				"						CASE\n" +
				"					WHEN ps.iscurrentlyonmeds = 1 THEN\n" +
				"						TRUE\n" +
				"					ELSE\n" +
				"						FALSE\n" +
				"					END onMeds,\n" +
				"					ps.iscurrentlyonmedsdesc onmedsdesc,\n" +
				"					CASE\n" +
				"				WHEN ps.patienthasmetalinbody = 1 THEN\n" +
				"					TRUE\n" +
				"				ELSE\n" +
				"					FALSE\n" +
				"				END hasmetal,\n" +
				"				ps.patienthasmetalinbodydesc hasmetaldesc,\n" +
				"				CASE\n" +
				"			WHEN ps.patienthaspreviousmris = 1 THEN\n" +
				"				TRUE\n" +
				"			ELSE\n" +
				"				FALSE\n" +
				"			END hadpreviousimage,\n" +
				"			ps.patienthaspreviousmris hadpreviousimagedesc,\n" +
				"			CASE\n" +
				"		WHEN ps.patientispregnant = 1 THEN\n" +
				"			TRUE\n" +
				"		ELSE\n" +
				"			FALSE\n" +
				"		END pregnant,\n" +
				"		ps.patientispregnant pregnantdesc,\n" +
				"		ps.patientheight height,\n" +
				"		ps.patientweight weight\n" +
				"	FROM\n" +
				"		tnim3_caseaccount ps\n" +
				"	WHERE\n" +
				"		ps.caseid = ca.caseid\n" +
				"					) prescreen_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					array_to_json (\n" +
				"						ARRAY_AGG (row_to_json(rf_json))\n" +
				"					) referrals\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							rf.referralid,\n" +
				"							(\n" +
				"								SELECT\n" +
				"									row_to_json (ua_json) referringphysician\n" +
				"								FROM\n" +
				"									(\n" +
				"										SELECT\n" +
				"											ua.userid,\n" +
				"											ua.contactfirstname \"firstname\",\n" +
				"											ua.contactlastname \"lastname\",\n" +
				"											ua.contactphone \"phone\",\n" +
				"											ua.contactfax \"cellphone\",\n" +
				"											ua.contactemail \"email\",\n" +
				"											pm.payername\n" +
				"										FROM\n" +
				"											tuseraccount ua\n" +
				"										JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"										WHERE\n" +
				"											ua.userid = rf.referringphysicianid\n" +
				"									) ua_json\n" +
				"							),\n" +
				"							(\n" +
				"								SELECT\n" +
				"									row_to_json (ua_json) referredbycontact\n" +
				"								FROM\n" +
				"									(\n" +
				"										SELECT\n" +
				"											ua.userid,\n" +
				"											ua.contactfirstname \"firstname\",\n" +
				"											ua.contactlastname \"lastname\",\n" +
				"											ua.contactphone \"phone\",\n" +
				"											ua.contactfax \"cellphone\",\n" +
				"											ua.contactemail \"email\",\n" +
				"											pm.payername\n" +
				"										FROM\n" +
				"											tuseraccount ua\n" +
				"										JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"										WHERE\n" +
				"											ua.userid = rf.referredbycontactid\n" +
				"									) ua_json\n" +
				"							),\n" +
				"							CASE\n" +
				"						WHEN rf.referralstatusid = 1 THEN\n" +
				"							'Approved'\n" +
				"						ELSE\n" +
				"							'Not Approved'\n" +
				"						END referralStatus,\n" +
				"						rf.receivedate,\n" +
				"						rf.rxfileid,\n" +
				"						rf.orderfileid,\n" +
				"						rf.referralmethod,\n" +
				"						(\n" +
				"							SELECT\n" +
				"								array_to_json (\n" +
				"									ARRAY_AGG (row_to_json(doc_json))\n" +
				"								) documents\n" +
				"							FROM\n" +
				"								(\n" +
				"									SELECT\n" +
				"										doc.documentid,\n" +
				"										doc.filename,\n" +
				"										doc.uniquecreatedate,\n" +
				"										doc.docname,\n" +
				"										(\n" +
				"											SELECT\n" +
				"												row_to_json (ua_json) uploader\n" +
				"											FROM\n" +
				"												(\n" +
				"													SELECT\n" +
				"														ua.userid,\n" +
				"														ua.contactfirstname \"firstname\",\n" +
				"														ua.contactlastname \"lastname\",\n" +
				"														ua.contactphone \"phone\",\n" +
				"														ua.contactfax \"cellphone\",\n" +
				"														ua.contactemail \"email\",\n" +
				"														pm.payername\n" +
				"													FROM\n" +
				"														tuseraccount ua\n" +
				"													JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"													WHERE\n" +
				"														ua.userid = doc.uploaduserid\n" +
				"												) ua_json\n" +
				"										)\n" +
				"									FROM\n" +
				"										tnim3_document doc\n" +
				"									WHERE\n" +
				"										doc.referralid = rf.referralid\n" +
				"								) doc_json\n" +
				"						),\n" +
				"						(\n" +
				"							SELECT\n" +
				"								array_to_json (\n" +
				"									ARRAY_AGG (row_to_json(en_json))\n" +
				"								) encounters\n" +
				"							FROM\n" +
				"								(\n" +
				"									SELECT\n" +
				"										en.encounterid,\n" +
				"										en.scanpass,\n" +
				"										en.encountertypeid,\n" +
				"										en.dateofservice,\n" +
				"										case when en.nextactiondate::varchar = '1800-01-01 00:00:00' then false else true end isnextaction,\n" +
				"										en.nextactiondate,\n" +
				"										en.nextactionnotes,\n" +
				"										(\n" +
				"											SELECT\n" +
				"												row_to_json (eps_json) prescreen\n" +
				"											FROM\n" +
				"												(\n" +
				"													SELECT\n" +
				"														eps.patientavailability,\n" +
				"														CASE\n" +
				"													WHEN eps.isstat = 1 THEN\n" +
				"														TRUE\n" +
				"													ELSE\n" +
				"														FALSE\n" +
				"													END stat,\n" +
				"													CASE\n" +
				"												WHEN eps.requiresfilms = 1 THEN\n" +
				"													TRUE\n" +
				"												ELSE\n" +
				"													FALSE\n" +
				"												END requiresfilms,\n" +
				"												CASE\n" +
				"											WHEN eps.requiresageinjury = 1 THEN\n" +
				"												TRUE\n" +
				"											ELSE\n" +
				"												FALSE\n" +
				"											END requiresageinjury,\n" +
				"											CASE\n" +
				"										WHEN eps.iscourtesy = 1 THEN\n" +
				"											TRUE\n" +
				"										ELSE\n" +
				"											FALSE\n" +
				"										END courtesy,\n" +
				"										CASE\n" +
				"									WHEN eps.requireshandcarrycd = 1 THEN\n" +
				"										TRUE\n" +
				"									ELSE\n" +
				"										FALSE\n" +
				"									END requireshandcarrycd,\n" +
				"									CASE\n" +
				"								WHEN eps.isretro = 1 THEN\n" +
				"									TRUE\n" +
				"								ELSE\n" +
				"									FALSE\n" +
				"								END retro,\n" +
				"								CASE\n" +
				"							WHEN eps.isvip = 1 THEN\n" +
				"								TRUE\n" +
				"							ELSE\n" +
				"								FALSE\n" +
				"							END vip,\n" +
				"							CASE\n" +
				"						WHEN eps.requiresonlineimage = 1 THEN\n" +
				"							TRUE\n" +
				"						ELSE\n" +
				"							FALSE\n" +
				"						END requiresonlineimage\n" +
				"						FROM\n" +
				"							tnim3_encounter eps\n" +
				"						WHERE\n" +
				"							eps.encounterid = en.encounterid\n" +
				"												) eps_json\n" +
				"										),\n" +
				"										CASE\n" +
				"									WHEN to_char(\n" +
				"										en.timetrack_reqrxreview,\n" +
				"										'yyyy-mm-dd'\n" +
				"									) != '1800-01-01' THEN\n" +
				"										TRUE\n" +
				"									ELSE\n" +
				"										FALSE\n" +
				"									END rxreview,\n" +
				"									CASE\n" +
				"								WHEN to_char(\n" +
				"									en.timetrack_reqrpreview,\n" +
				"									'yyyy-mm-dd'\n" +
				"								) != '1800-01-01' THEN\n" +
				"									TRUE\n" +
				"								ELSE\n" +
				"									FALSE\n" +
				"								END rpreview,\n" +
				"								(\n" +
				"									SELECT\n" +
				"										array_to_json (\n" +
				"											ARRAY_AGG (row_to_json(doc_json))\n" +
				"										) documents\n" +
				"									FROM\n" +
				"										(\n" +
				"											SELECT\n" +
				"												doc.documentid,\n" +
				"												doc.filename,\n" +
				"												doc.uniquecreatedate,\n" +
				"												doc.docname,\n" +
				"												(\n" +
				"													SELECT\n" +
				"														row_to_json (ua_json) uploader\n" +
				"													FROM\n" +
				"														(\n" +
				"															SELECT\n" +
				"																ua.userid,\n" +
				"																ua.contactfirstname \"firstname\",\n" +
				"																ua.contactlastname \"lastname\",\n" +
				"																ua.contactphone \"phone\",\n" +
				"																ua.contactfax \"cellphone\",\n" +
				"																ua.contactemail \"email\",\n" +
				"																pm.payername\n" +
				"															FROM\n" +
				"																tuseraccount ua\n" +
				"															JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"															WHERE\n" +
				"																ua.userid = doc.uploaduserid\n" +
				"														) ua_json\n" +
				"												)\n" +
				"											FROM\n" +
				"												tnim3_document doc\n" +
				"											WHERE\n" +
				"												doc.encounterid = en.encounterid\n" +
				"										) doc_json\n" +
				"								),\n" +
				"								(\n" +
				"									SELECT\n" +
				"										array_to_json (\n" +
				"											ARRAY_AGG (row_to_json(notifications))\n" +
				"										) notifications\n" +
				"									FROM\n" +
				"										(\n" +
				"											SELECT\n" +
				"												7 notificationsort,\n" +
				"												ca.adjusterid userid,\n" +
				"												CASE\n" +
				"											WHEN enchild.sentto_reqrec_adj :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"												TRUE\n" +
				"											ELSE\n" +
				"												FALSE\n" +
				"											END conf,\n" +
				"											CASE\n" +
				"										WHEN enchild.sentto_sp_adj :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"											TRUE\n" +
				"										ELSE\n" +
				"											FALSE\n" +
				"										END sp,\n" +
				"										CASE\n" +
				"									WHEN enchild.senttoadj :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"										TRUE\n" +
				"									ELSE\n" +
				"										FALSE\n" +
				"									END rp,\n" +
				"									'Adjuster' \"role\",\n" +
				"									(\n" +
				"										SELECT\n" +
				"											row_to_json (ua_json) \"user\"\n" +
				"										FROM\n" +
				"											(\n" +
				"												SELECT\n" +
				"													ua.userid,\n" +
				"													ua.contactfirstname \"firstname\",\n" +
				"													ua.contactlastname \"lastname\",\n" +
				"													ua.contactphone \"phone\",\n" +
				"													ua.contactfax \"cellphone\",\n" +
				"													ua.contactemail \"email\",\n" +
				"													pm.payername\n" +
				"												FROM\n" +
				"													tuseraccount ua\n" +
				"												JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"												WHERE\n" +
				"													ua.userid = ca.adjusterid\n" +
				"											) ua_json\n" +
				"									)\n" +
				"								FROM\n" +
				"									tnim3_encounter enchild\n" +
				"								JOIN tnim3_referral rf ON enchild.referralid = rf.referralid\n" +
				"								JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"								WHERE\n" +
				"									enchild.encounterid = en.encounterid\n" +
				"								UNION ALL\n" +
				"									SELECT\n" +
				"										6 notificationsort,\n" +
				"										ca.nursecasemanagerid userid,\n" +
				"										CASE\n" +
				"									WHEN enchild.sentto_reqrec_ncm :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"										TRUE\n" +
				"									ELSE\n" +
				"										FALSE\n" +
				"									END conf,\n" +
				"									CASE\n" +
				"								WHEN enchild.sentto_sp_ncm :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"									TRUE\n" +
				"								ELSE\n" +
				"									FALSE\n" +
				"								END sp,\n" +
				"								CASE\n" +
				"							WHEN enchild.sentto_rp_ncm :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"								TRUE\n" +
				"							ELSE\n" +
				"								FALSE\n" +
				"							END rp,\n" +
				"							'NCM' \"role\",\n" +
				"							(\n" +
				"								SELECT\n" +
				"									row_to_json (ua_json) \"user\"\n" +
				"								FROM\n" +
				"									(\n" +
				"										SELECT\n" +
				"											ua.userid,\n" +
				"											ua.contactfirstname \"firstname\",\n" +
				"											ua.contactlastname \"lastname\",\n" +
				"											ua.contactphone \"phone\",\n" +
				"											ua.contactfax \"cellphone\",\n" +
				"											ua.contactemail \"email\",\n" +
				"											pm.payername\n" +
				"										FROM\n" +
				"											tuseraccount ua\n" +
				"										JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"										WHERE\n" +
				"											ua.userid = ca.nursecasemanagerid\n" +
				"									) ua_json\n" +
				"							)\n" +
				"						FROM\n" +
				"							tnim3_encounter enchild\n" +
				"						JOIN tnim3_referral rf ON enchild.referralid = rf.referralid\n" +
				"						JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"						WHERE\n" +
				"							enchild.encounterid = en.encounterid\n" +
				"						UNION ALL\n" +
				"							SELECT\n" +
				"								5 notificationsort,\n" +
				"								ca.caseadministratorid userid,\n" +
				"								CASE\n" +
				"							WHEN enchild.sentto_reqrec_adm :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"								TRUE\n" +
				"							ELSE\n" +
				"								FALSE\n" +
				"							END conf,\n" +
				"							CASE\n" +
				"						WHEN enchild.sentto_sp_adm :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"							TRUE\n" +
				"						ELSE\n" +
				"							FALSE\n" +
				"						END sp,\n" +
				"						CASE\n" +
				"					WHEN enchild.senttoadm :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"						TRUE\n" +
				"					ELSE\n" +
				"						FALSE\n" +
				"					END rp,\n" +
				"					'Admin 1' \"role\",\n" +
				"					(\n" +
				"						SELECT\n" +
				"							row_to_json (ua_json) \"user\"\n" +
				"						FROM\n" +
				"							(\n" +
				"								SELECT\n" +
				"									ua.userid,\n" +
				"									ua.contactfirstname \"firstname\",\n" +
				"									ua.contactlastname \"lastname\",\n" +
				"									ua.contactphone \"phone\",\n" +
				"									ua.contactfax \"cellphone\",\n" +
				"									ua.contactemail \"email\",\n" +
				"									pm.payername\n" +
				"								FROM\n" +
				"									tuseraccount ua\n" +
				"								JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"								WHERE\n" +
				"									ua.userid = ca.caseadministratorid\n" +
				"							) ua_json\n" +
				"					)\n" +
				"				FROM\n" +
				"					tnim3_encounter enchild\n" +
				"				JOIN tnim3_referral rf ON enchild.referralid = rf.referralid\n" +
				"				JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"				WHERE\n" +
				"					enchild.encounterid = en.encounterid\n" +
				"				UNION ALL\n" +
				"					SELECT\n" +
				"						4 notificationsort,\n" +
				"						ca.caseadministrator2id userid,\n" +
				"						CASE\n" +
				"					WHEN enchild.sentto_reqrec_adm2 :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"						TRUE\n" +
				"					ELSE\n" +
				"						FALSE\n" +
				"					END conf,\n" +
				"					CASE\n" +
				"				WHEN enchild.sentto_sp_adm2 :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"					TRUE\n" +
				"				ELSE\n" +
				"					FALSE\n" +
				"				END sp,\n" +
				"				CASE\n" +
				"			WHEN enchild.sentto_rp_adm2 :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"				TRUE\n" +
				"			ELSE\n" +
				"				FALSE\n" +
				"			END rp,\n" +
				"			'Admin 2' \"role\",\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) \"user\"\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.caseadministrator2id\n" +
				"					) ua_json\n" +
				"			)\n" +
				"		FROM\n" +
				"			tnim3_encounter enchild\n" +
				"		JOIN tnim3_referral rf ON enchild.referralid = rf.referralid\n" +
				"		JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"		JOIN tuseraccount ua ON ua.userid = ca.caseadministrator2id\n" +
				"		WHERE\n" +
				"			enchild.encounterid = en.encounterid\n" +
				"		UNION ALL\n" +
				"			SELECT\n" +
				"				3 notificationsort,\n" +
				"				ca.caseadministrator3id userid,\n" +
				"				CASE\n" +
				"			WHEN enchild.sentto_reqrec_adm3 :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"				TRUE\n" +
				"			ELSE\n" +
				"				FALSE\n" +
				"			END conf,\n" +
				"			CASE\n" +
				"		WHEN enchild.sentto_sp_adm3 :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"			TRUE\n" +
				"		ELSE\n" +
				"			FALSE\n" +
				"		END sp,\n" +
				"		CASE\n" +
				"	WHEN enchild.sentto_rp_adm3 :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"		TRUE\n" +
				"	ELSE\n" +
				"		FALSE\n" +
				"	END rp,\n" +
				"	'Admin 3' \"role\",\n" +
				"	(\n" +
				"		SELECT\n" +
				"			row_to_json (ua_json) \"user\"\n" +
				"		FROM\n" +
				"			(\n" +
				"				SELECT\n" +
				"					ua.userid,\n" +
				"					ua.contactfirstname \"firstname\",\n" +
				"					ua.contactlastname \"lastname\",\n" +
				"					ua.contactphone \"phone\",\n" +
				"					ua.contactfax \"cellphone\",\n" +
				"					ua.contactemail \"email\",\n" +
				"					pm.payername\n" +
				"				FROM\n" +
				"					tuseraccount ua\n" +
				"				JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"				WHERE\n" +
				"					ua.userid = ca.caseadministrator3id\n" +
				"			) ua_json\n" +
				"	)\n" +
				"FROM\n" +
				"	tnim3_encounter enchild\n" +
				"JOIN tnim3_referral rf ON enchild.referralid = rf.referralid\n" +
				"JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"JOIN tuseraccount ua ON ua.userid = ca.caseadministrator3id\n" +
				"WHERE\n" +
				"	enchild.encounterid = en.encounterid\n" +
				"UNION ALL\n" +
				"	SELECT\n" +
				"		2 notificationsort,\n" +
				"		ca.caseadministrator4id userid,\n" +
				"		CASE\n" +
				"	WHEN enchild.sentto_reqrec_adm4 :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"		TRUE\n" +
				"	ELSE\n" +
				"		FALSE\n" +
				"	END conf,\n" +
				"	CASE\n" +
				"WHEN enchild.sentto_sp_adm4 :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"	TRUE\n" +
				"ELSE\n" +
				"	FALSE\n" +
				"END sp,\n" +
				" CASE\n" +
				"WHEN enchild.sentto_rp_adm4 :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"	TRUE\n" +
				"ELSE\n" +
				"	FALSE\n" +
				"END rp,\n" +
				" 'Admin 4' \"role\",\n" +
				" (\n" +
				"	SELECT\n" +
				"		row_to_json (ua_json) \"user\"\n" +
				"	FROM\n" +
				"		(\n" +
				"			SELECT\n" +
				"				ua.userid,\n" +
				"				ua.contactfirstname \"firstname\",\n" +
				"				ua.contactlastname \"lastname\",\n" +
				"				ua.contactphone \"phone\",\n" +
				"				ua.contactfax \"cellphone\",\n" +
				"				ua.contactemail \"email\",\n" +
				"				pm.payername\n" +
				"			FROM\n" +
				"				tuseraccount ua\n" +
				"			JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"			WHERE\n" +
				"				ua.userid = ca.caseadministrator4id\n" +
				"		) ua_json\n" +
				")\n" +
				"FROM\n" +
				"	tnim3_encounter enchild\n" +
				"JOIN tnim3_referral rf ON enchild.referralid = rf.referralid\n" +
				"JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"JOIN tuseraccount ua ON ua.userid = ca.caseadministrator4id\n" +
				"WHERE\n" +
				"	enchild.encounterid = en.encounterid\n" +
				"UNION ALL\n" +
				"	SELECT\n" +
				"		1 notificationsort,\n" +
				"		rf.referringphysicianid userid,\n" +
				"		CASE\n" +
				"	WHEN enchild.sentto_reqrec_refdr :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"		TRUE\n" +
				"	ELSE\n" +
				"		FALSE\n" +
				"	END conf,\n" +
				"	CASE\n" +
				"WHEN enchild.sentto_sp_refdr :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"	TRUE\n" +
				"ELSE\n" +
				"	FALSE\n" +
				"END sp,\n" +
				" CASE\n" +
				"WHEN enchild.senttorefdr :: VARCHAR != '1800-01-01 00:00:00' THEN\n" +
				"	TRUE\n" +
				"ELSE\n" +
				"	FALSE\n" +
				"END rp,\n" +
				" 'Physician' \"role\",\n" +
				" (\n" +
				"	SELECT\n" +
				"		row_to_json (ua_json) \"user\"\n" +
				"	FROM\n" +
				"		(\n" +
				"			SELECT\n" +
				"				ua.userid,\n" +
				"				ua.contactfirstname \"firstname\",\n" +
				"				ua.contactlastname \"lastname\",\n" +
				"				ua.contactphone \"phone\",\n" +
				"				ua.contactfax \"cellphone\",\n" +
				"				ua.contactemail \"email\",\n" +
				"				pm.payername\n" +
				"			FROM\n" +
				"				tuseraccount ua\n" +
				"			JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"			WHERE\n" +
				"				ua.userid = rf.referringphysicianid\n" +
				"		) ua_json\n" +
				")\n" +
				"FROM\n" +
				"	tnim3_encounter enchild\n" +
				"JOIN tnim3_referral rf ON enchild.referralid = rf.referralid\n" +
				"JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"JOIN tuseraccount ua ON ua.userid = rf.referringphysicianid\n" +
				"WHERE\n" +
				"	enchild.encounterid = en.encounterid\n" +
				"										) notifications\n" +
				"									WHERE\n" +
				"										notifications.userid != 0\n" +
				"								),\n" +
				"								COALESCE (\n" +
				"									(\n" +
				"										SELECT\n" +
				"											array_to_json (\n" +
				"												ARRAY_AGG (row_to_json(appt_json))\n" +
				"											) appointments\n" +
				"										FROM\n" +
				"											(\n" +
				"												SELECT\n" +
				"													appt.appointmentid,\n" +
				"													appt.appointmenttime,\n" +
				"													appt.comments,\n" +
				"													CASE\n" +
				"												WHEN appt.istatus = 1 THEN\n" +
				"													'Active'\n" +
				"												WHEN appt.istatus = 4 THEN\n" +
				"													'Canceled'\n" +
				"												ELSE\n" +
				"													'void'\n" +
				"												END status,\n" +
				"												(\n" +
				"													SELECT\n" +
				"														row_to_json (prc_json) practice\n" +
				"													FROM\n" +
				"														(\n" +
				"															SELECT\n" +
				"																pm.practiceid,\n" +
				"																pm.practicename,\n" +
				"																pm.officeaddress1 address,\n" +
				"																pm.officeaddress2 ste,\n" +
				"																pm.officecity city,\n" +
				"																pstate.shortstate \"state\",\n" +
				"																pm.officezip zip,\n" +
				"																pm.officephone phone,\n" +
				"																pm.officefaxno fax\n" +
				"															FROM\n" +
				"																tpracticemaster pm\n" +
				"															JOIN tstateli pstate ON pstate.stateid = pm.officestateid\n" +
				"															WHERE\n" +
				"																pm.practiceid = appt.providerid\n" +
				"														) prc_json\n" +
				"												),\n" +
				"												(\n" +
				"													SELECT\n" +
				"														row_to_json (ua_json) scheduler\n" +
				"													FROM\n" +
				"														(\n" +
				"															SELECT\n" +
				"																ua.userid,\n" +
				"																ua.contactfirstname \"firstname\",\n" +
				"																ua.contactlastname \"lastname\",\n" +
				"																ua.contactphone \"phone\",\n" +
				"																ua.contactfax \"cellphone\",\n" +
				"																ua.contactemail \"email\",\n" +
				"																pm.payername\n" +
				"															FROM\n" +
				"																tuseraccount ua\n" +
				"															JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"															WHERE\n" +
				"																ua.userid = appt.scheduler_userid\n" +
				"														) ua_json\n" +
				"												)\n" +
				"											FROM\n" +
				"												tnim3_encounter enApp\n" +
				"											join tnim3_appointment appt on enApp.appointmentid = appt.appointmentid\n" +
				"											WHERE\n" +
				"												enApp.encounterid = en.encounterid\n" +
				"											) appt_json\n" +
				"									),\n" +
				"									'{}'\n" +
				"								) appointments,\n" +
				"								COALESCE (\n" +
				"									(\n" +
				"										SELECT\n" +
				"											array_to_json (\n" +
				"												ARRAY_AGG (row_to_json(ct_json))\n" +
				"											) commtracks\n" +
				"										FROM\n" +
				"											(\n" +
				"												SELECT\n" +
				"													ct.commtrackid,\n" +
				"													ct.uniquecreatedate createdate,\n" +
				"													ct.messagetext msgbody,\n" +
				"													ct.commreferenceid,\n" +
				"													(\n" +
				"														SELECT\n" +
				"															row_to_json (ua_json) ctuser\n" +
				"														FROM\n" +
				"															(\n" +
				"																SELECT\n" +
				"																	ua.userid,\n" +
				"																	ua.contactfirstname \"firstname\",\n" +
				"																	ua.contactlastname \"lastname\",\n" +
				"																	ua.contactphone \"phone\",\n" +
				"																	ua.contactfax \"cellphone\",\n" +
				"																	ua.contactemail \"email\",\n" +
				"																	pm.payername\n" +
				"																FROM\n" +
				"																	tuseraccount ua\n" +
				"																JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"																WHERE\n" +
				"																	ua.userid = ct.intuserid\n" +
				"															) ua_json\n" +
				"													)\n" +
				"												FROM\n" +
				"													tnim3_commtrack ct\n" +
				"												WHERE\n" +
				"													(\n" +
				"														ct.caseid = ca.caseid\n" +
				"														OR ct.referralid = rf.referralid\n" +
				"														OR ct.encounterid = en.encounterid\n" +
				"													)\n" +
				"												AND ct.commtypeid != 55\n" +
				"												ORDER BY\n" +
				"													ct.commtrackid DESC\n" +
				"											) ct_json\n" +
				"									),\n" +
				"									'{}'\n" +
				"								) commtracks,\n" +
				"								(\n" +
				"									SELECT\n" +
				"										array_to_json (\n" +
				"											ARRAY_AGG (row_to_json(service_json))\n" +
				"										) services\n" +
				"									FROM\n" +
				"										(\n" +
				"											SELECT\n" +
				"												service.serviceid,\n" +
				"												service.cpt,\n" +
				"												service.cptmodifier,\n" +
				"												service.cptqty,\n" +
				"												service.cptbodypart,\n" +
				"												service.dcpt1,\n" +
				"												service.dcpt2,\n" +
				"												service.dcpt3,\n" +
				"												service.dcpt4,\n" +
				"												CASE\n" +
				"											WHEN service.servicetypeid = 0 THEN\n" +
				"												'N/A'\n" +
				"											WHEN service.servicetypeid = 1 THEN\n" +
				"												'Professional'\n" +
				"											WHEN service.servicetypeid = 2 THEN\n" +
				"												'Tech'\n" +
				"											WHEN service.servicetypeid = 3 THEN\n" +
				"												'Global'\n" +
				"											ELSE\n" +
				"												'Other'\n" +
				"											END servicetype,\n" +
				"											CASE\n" +
				"										WHEN service.servicestatusid = 0 THEN\n" +
				"											'N/A'\n" +
				"										WHEN service.servicestatusid = 5 THEN\n" +
				"											'Void'\n" +
				"										ELSE\n" +
				"											'Active'\n" +
				"										END \"status\"\n" +
				"										FROM\n" +
				"											tnim3_service service\n" +
				"										WHERE\n" +
				"											service.encounterid = en.encounterid\n" +
				"										) service_json\n" +
				"								)\n" +
				"							FROM\n" +
				"								tnim3_encounter en\n" +
				"							WHERE\n" +
				"								en.referralid = rf.referralid\n" +
				"								) en_json\n" +
				"						)\n" +
				"					FROM\n" +
				"						tnim3_referral rf\n" +
				"					WHERE\n" +
				"						rf.caseid = ca.caseid\n" +
				"					) rf_json\n" +
				"			)\n" +
				"		FROM\n" +
				"			tnim3_caseaccount ca\n" +
				"		JOIN tnim3_payermaster payer ON payer.payerid = ca.payerid\n" +
				"		WHERE\n" +
				"			caseid = "+cId+"\n" +
				"	) ca_json";
	}
	
	private String getQueryLite(Integer cId){
		return "SELECT\n" +
				"	row_to_json (ca_json)\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			ca.caseid,\n" +
				"			ca.caseclaimnumber,\n" +
				"			ca.payerid,\n" +
				"			payer.payername,\n" +
				"			payer.comments payernotes,\n" +
				"			case when payer.parentpayerid = 951 then 'gh' else 'wc' end clienttype,\n" +
				"			to_char(ca.dateofinjury, 'mm-dd-yy') dateofinjury,\n" +
				"			to_char(ca.patientdob, 'mm-dd-yy') dateofbirth,\n" +
				"			ca.patientssn ssn,\n" +
				"			ca.patientgender gender,\n" +
				"			ca.injurydescription,\n" +
				"			ca.comments notes,\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (pt_json) patient\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							pt.patientfirstname \"firstname\",\n" +
				"							pt.patientlastname \"lastname\",\n" +
				"							(\n" +
				"								SELECT\n" +
				"									row_to_json (pt_addy_json) address\n" +
				"								FROM\n" +
				"									(\n" +
				"										SELECT\n" +
				"											pt_addy.patientaddress1 address,\n" +
				"											pt_addy.patientcity city,\n" +
				"											pt_addy_state.shortstate \"state\",\n" +
				"											pt_addy.patientzip zip\n" +
				"										FROM\n" +
				"											tnim3_caseaccount pt_addy\n" +
				"										JOIN tstateli pt_addy_state ON pt_addy_state.stateid = pt_addy.patientstateid\n" +
				"										WHERE\n" +
				"											pt_addy.caseid = ca.caseid\n" +
				"									) pt_addy_json\n" +
				"							),\n" +
				"							pt.patienthomephone \"phone\",\n" +
				"							pt.patientcellphone \"cellphone\",\n" +
				"							pt.patientemail \"email\"\n" +
				"						FROM\n" +
				"							tnim3_caseaccount pt\n" +
				"						WHERE\n" +
				"							pt.caseid = ca.caseid\n" +
				"					) pt_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (em_json) employer\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							em.employername company,\n" +
				"							em.employerphone phone,\n" +
				"							em.employerfax fax\n" +
				"						FROM\n" +
				"							tnim3_caseaccount em\n" +
				"						WHERE\n" +
				"							em.caseid = ca.caseid\n" +
				"					) em_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) assignedTo\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.assignedtoid\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) adjuster\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.adjusterid\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) nursecasemanager\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.nursecasemanagerid\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) caseAdministrator1\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.caseAdministratorid\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) caseAdministrator2\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.caseAdministrator2id\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) caseAdministrator3\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.caseAdministrator3id\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (ua_json) caseAdministrator4\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							ua.userid,\n" +
				"							ua.contactfirstname \"firstname\",\n" +
				"							ua.contactlastname \"lastname\",\n" +
				"							ua.contactphone \"phone\",\n" +
				"							ua.contactfax \"cellphone\",\n" +
				"							ua.contactemail \"email\",\n" +
				"							pm.payername\n" +
				"						FROM\n" +
				"							tuseraccount ua\n" +
				"						JOIN tnim3_payermaster pm ON pm.payerid = ua.payerid\n" +
				"						WHERE\n" +
				"							ua.userid = ca.caseAdministrator4id\n" +
				"					) ua_json\n" +
				"			),\n" +
				"			(\n" +
				"				SELECT\n" +
				"					row_to_json (prescreen_json) prescreen\n" +
				"				FROM\n" +
				"					(\n" +
				"						SELECT\n" +
				"							CASE\n" +
				"						WHEN ps.patientisclaus = 1 THEN\n" +
				"							TRUE\n" +
				"						ELSE\n" +
				"							FALSE\n" +
				"						END claustrophobic,\n" +
				"						CASE\n" +
				"					WHEN ps.iscurrentlyonmeds = 1 THEN\n" +
				"						TRUE\n" +
				"					ELSE\n" +
				"						FALSE\n" +
				"					END onMeds,\n" +
				"					ps.iscurrentlyonmedsdesc onmedsdesc,\n" +
				"					CASE\n" +
				"				WHEN ps.patienthasmetalinbody = 1 THEN\n" +
				"					TRUE\n" +
				"				ELSE\n" +
				"					FALSE\n" +
				"				END hasmetal,\n" +
				"				ps.patienthasmetalinbodydesc hasmetaldesc,\n" +
				"				CASE\n" +
				"			WHEN ps.patienthaspreviousmris = 1 THEN\n" +
				"				TRUE\n" +
				"			ELSE\n" +
				"				FALSE\n" +
				"			END hadpreviousimage,\n" +
				"			ps.patienthaspreviousmris hadpreviousimagedesc,\n" +
				"			CASE\n" +
				"		WHEN ps.patientispregnant = 1 THEN\n" +
				"			TRUE\n" +
				"		ELSE\n" +
				"			FALSE\n" +
				"		END pregnant,\n" +
				"		ps.patientispregnant pregnantdesc,\n" +
				"		ps.patientheight height,\n" +
				"		ps.patientweight weight\n" +
				"	FROM\n" +
				"		tnim3_caseaccount ps\n" +
				"	WHERE\n" +
				"		ps.caseid = ca.caseid\n" +
				"					) prescreen_json\n" +
				"			)\n" +
				"		FROM\n" +
				"			tnim3_caseaccount ca\n" +
				"		JOIN tnim3_payermaster payer ON payer.payerid = ca.payerid\n" +
				"		WHERE\n" +
				"			caseid = "+cId+"\n" +
				"	) ca_json";
	}
	
	public static void main(String[] args) {
		CasepageUtils cpu = new CasepageUtils();
		
		Caseaccount ca = cpu.getCasepage(12342, "2ce80bf173d11bb27fd213a21d1a1b91");
		
		System.out.println(new Gson().toJson(ca));
	}
}
