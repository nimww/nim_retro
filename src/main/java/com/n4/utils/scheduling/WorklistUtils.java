package com.n4.utils.scheduling;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gson.Gson;
import com.n4.model.view.scheduling.AdjusterWorklist;
import com.n4.model.view.scheduling.NIMWorklistModel;
import com.n4.utils.Database;
import com.n4.utils.Utils;

public class WorklistUtils extends Utils {
	private LinkedHashMap<Integer, String> stateId = getStateId();
	private LinkedHashMap<Integer, String> assignedTo = getScheduler();
	private List<NIMWorklistModel> needsToBeScheduled = new ArrayList<NIMWorklistModel>();
	private List<NIMWorklistModel> appointmentFollowUp = new ArrayList<NIMWorklistModel>();
	private List<NIMWorklistModel> awaitingRx = new ArrayList<NIMWorklistModel>();
	private List<NIMWorklistModel> nextActionAlert = new ArrayList<NIMWorklistModel>();
	private LinkedHashMap<String, List<NIMWorklistModel>> nimWorklist = new LinkedHashMap<String, List<NIMWorklistModel>>();

	public List<AdjusterWorklist> getAdjWorklist(Integer userid){
		Database conn = new Database();
		ResultSet rs = conn.getRs(adjQuery(userid));
		List<AdjusterWorklist> wl = new ArrayList<AdjusterWorklist>();
		try {
			while (rs.next()) {
				wl = new Gson().fromJson(rs.getString(1), wl.getClass());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		conn.closeAll();
		return wl;
	}
	
	public LinkedHashMap<String, List<NIMWorklistModel>> getWorklist(Integer userid, String key) {
		if (verifyAPIkey(key)) {
			String query = "select * from n4_worklist where (isrxrw = false and isrxuploaded = true and isauthuploaded = true and issched = false and iscritdata = true) or assignedtoid = " + userid;
			Database conn = new Database();
			ResultSet rs = conn.getRs(query);

			try {
				while (rs.next()) {
					NIMWorklistModel wml = new NIMWorklistModel();
					wml.setPatient(rs.getString("patient"));
					wml.setAssignedTo(assignedTo.get(rs.getInt("assignedtoid")));
					wml.setScanpass(rs.getString("scanpass"));
					wml.setCaseid(rs.getString("caseid"));
					wml.setEncounterid(rs.getString("encounterid"));
					if (!rs.getBoolean("issched") && rs.getInt("assignedtoid")==userid) {
						wml.setPayer(rs.getString("payer"));
						wml.setState(stateId.get(rs.getInt("patientstateid")));
						wml.setStatus("");
						wml.setHours(new Long(getHoursPassed(rs.getTimestamp("receivedate"))).toString());
						needsToBeScheduled.add(wml);
					} else if (rs.getBoolean("issched") && rs.getBoolean("ispostappointment")) {
						wml.setPayer(rs.getString("payer"));
						wml.setState(stateId.get(rs.getInt("patientstateid")));
						wml.setStatus("");
						wml.setHours(new Long(getHoursPassed(rs.getTimestamp("receivedate"))).toString());
						appointmentFollowUp.add(wml);
					}
					if (!rs.getBoolean("isrxrw") && rs.getBoolean("isrxuploaded") && rs.getBoolean("isauthuploaded") && rs.getBoolean("iscritdata")) {
						wml.setState(stateId.get(rs.getInt("patientstateid")));
						wml.setStatus("Rx Ready for review");
						wml.setHours(new Long(getHoursPassed(rs.getTimestamp("receivedate"))).toString());
						awaitingRx.add(wml);
					}
					if (rs.getBoolean("isnextactiondate") && rs.getInt("assignedtoid")==userid) {
						wml.setNaNotes(rs.getString("nextactionnotes"));
						wml.setNad(rs.getTimestamp("nextactiondate"));
						nextActionAlert.add(wml);
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			conn.closeAll();
			nimWorklist.put("needsToBeScheduled", needsToBeScheduled);
			nimWorklist.put("appointmentFollowUp", appointmentFollowUp);
			nimWorklist.put("awaitingRx", awaitingRx);
			nimWorklist.put("nextActionAlert", nextActionAlert);
			
		} else {
			nimWorklist = null;
		}

		return nimWorklist;
	}

	private LinkedHashMap<Integer, String> getScheduler() {
		LinkedHashMap<Integer, String> schedulerId = new LinkedHashMap<Integer, String>();
		Database conn = new Database();

		ResultSet rs = conn.getRs("select userid, contactfirstname||' '||contactlastname assignedto from tuseraccount where lower(accounttype)~'scheduler'");

		try {
			while (rs.next()) {
				schedulerId.put(rs.getInt(1), rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return schedulerId;
	}

	private String adjQuery(Integer userid){
		return "SELECT\n" +
				"	array_to_json (\n" +
				"		ARRAY_AGG (row_to_json(adj_portal))\n" +
				"	)\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			ca.patientlastname || ', ' || ca.patientfirstname patient,\n" +
				"			ca.caseclaimnumber claim,\n" +
				"			to_char(rf.receivedate, 'mm/dd/yy') received,\n" +
				"			'status' status,\n" +
				"			ca.caseid\n" +
				"		FROM\n" +
				"			tnim3_encounter en\n" +
				"		JOIN tnim3_referral rf ON rf.referralid = en.referralid\n" +
				"		JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"		WHERE\n" +
				"			(\n" +
				"				ca.adjusterid = "+userid+"\n" +
				"				OR ca.nursecasemanagerid = "+userid+"\n" +
				"				OR ca.caseadministratorid = "+userid+"\n" +
				"				OR ca.caseadministrator2id = "+userid+"\n" +
				"				OR ca.caseadministrator3id = "+userid+"\n" +
				"				OR ca.caseadministrator4id = "+userid+"\n" +
				"			)\n" +
				"		order by rf.receivedate desc\n" +
				"	) adj_portal";
	}
	
	public static void main(String[] args) throws Exception {
		WorklistUtils SchedulingUtils = new WorklistUtils();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");// 2014-12-08
																				// 17:45:00
		// (PLCUtils.getNowDate(false).getTime()-myEO2.getNIM3_Referral().getReceiveDate().getTime())/(60*60*1000)
		Date rd = sdf.parse("2014-12-08 05:45:00 PM");
		System.out.println(SchedulingUtils.getHoursPassed(rd));

	}
}
