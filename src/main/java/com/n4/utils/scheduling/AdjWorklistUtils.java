package com.n4.utils.scheduling;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gson.Gson;
import com.n4.model.view.scheduling.AdjDashboardList;
import com.n4.model.view.scheduling.AdjDashboardModule;
import com.n4.utils.Database;

public class AdjWorklistUtils {
	Gson gson = new Gson();

	@SuppressWarnings("unchecked")
	public AdjDashboardList getDashboard(Integer adjId) {
		Database db = new Database(Database.RW);
		AdjDashboardList AdjDashboardList = new AdjDashboardList();
		ResultSet rs = db.getRs(query(adjId));

		try {
			while (rs.next()) {
				AdjDashboardList = gson.fromJson(rs.getString(1), AdjDashboardList.getClass());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		AdjDashboardList.setPending(cleanUp(AdjDashboardList.getPending()));
		AdjDashboardList.setScheduled(cleanUp(AdjDashboardList.getScheduled()));
		AdjDashboardList.setReportready(cleanUp(AdjDashboardList.getReportready()));
		AdjDashboardList.setCompleted(cleanUp(AdjDashboardList.getCompleted()));
		
		return AdjDashboardList;
	}

	public static void main(String[] args) {
		Database db = new Database(Database.RW);
		Gson gson = new Gson();
		AdjWorklistUtils AdjWorklistUtils = new AdjWorklistUtils();
		Object Object = new Object();
		ResultSet rs = db.getRs(AdjWorklistUtils.query(28362));

		try {
			while (rs.next()) {
				Object = gson.fromJson(rs.getString(1), Object.getClass());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println();
		System.out.println(gson.toJson(Object));
	}

	public List<AdjDashboardModule> cleanUp(List<AdjDashboardModule> adjDashboardModule) {
		List<AdjDashboardModule> newAdjDashboardModule = new ArrayList<AdjDashboardModule>();

		for (AdjDashboardModule adm : adjDashboardModule) {
			if (adm != null) {
				newAdjDashboardModule.add(adm);
			}
		}
		return newAdjDashboardModule;
	}

	public String query(int adjId) {
		return "SELECT\n" + "	row_to_json (home_json)\n" + "FROM\n" + "	(\n" + "		SELECT\n" + "			json_agg (\n" + "				(\n" + "					SELECT\n" + "						row_to_json (pending_json) pending\n" + "					FROM\n" + "						(\n" + "							SELECT\n" + "								ca.patientfirstname || ' ' || ca.patientlastname patient,\n" + "								ca.caseid,\n" + "								ca.caseclaimnumber,\n" + "								to_char(rf.receivedate,'mm/dd/yyyy') receivedate,\n" + "								en.encounterid,\n" + "								en.scanpass\n"
				+ "						) pending_json\n" + "					WHERE\n" + "						en.appointmentid = 0\n" + "				)\n" + "			) pending,\n" + "			json_agg (\n" + "				(\n" + "					SELECT\n" + "						row_to_json (scheduled_json) scheduled\n" + "					FROM\n" + "						(\n" + "							SELECT\n" + "								ca.patientfirstname || ' ' || ca.patientlastname patient,\n" + "								ca.caseid,\n" + "								ca.caseclaimnumber,\n" + "								to_char(rf.receivedate,'mm/dd/yyyy') receivedate,\n"
				+ "								en.encounterid,\n" + "								en.scanpass\n" + "						) scheduled_json\n" + "					WHERE\n" + "						en.appointmentid > 0\n" + "					AND en.reportfileid = 0\n" + "				)\n" + "			) scheduled,\n" + "			json_agg (\n" + "				(\n" + "					SELECT\n" + "						row_to_json (reportready_json) reportready\n" + "					FROM\n" + "						(\n" + "							SELECT\n" + "								ca.patientfirstname || ' ' || ca.patientlastname patient,\n" + "								ca.caseid,\n"
				+ "								ca.caseclaimnumber,\n" + "								to_char(rf.receivedate,'mm/dd/yyyy') receivedate,\n" + "								en.encounterid,\n" + "								en.scanpass\n" + "						) reportready_json\n" + "					WHERE\n" + "						en.appointmentid > 0\n" + "					AND en.reportfileid > 0\n" + "				)\n" + "			) reportready,\n" + "			json_agg (\n" + "				(\n" + "					SELECT\n" + "						row_to_json (completed_json) completed\n" + "					FROM\n" + "						(\n" + "							SELECT\n"
				+ "								ca.patientfirstname || ' ' || ca.patientlastname patient,\n" + "								ca.caseid,\n" + "								ca.caseclaimnumber,\n" + "								to_char(rf.receivedate,'mm/dd/yyyy') receivedate,\n" + "								en.encounterid,\n" + "								en.scanpass\n" + "						) completed_json\n" + "					WHERE\n" + "						en.appointmentid > 0\n" + "					AND en.reportfileid > 0\n" + "					AND en.encounterstatusid = 9\n" + "				)\n" + "			) completed\n" + "		FROM\n" + "			tnim3_encounter en\n"
				+ "		JOIN tnim3_referral rf ON rf.referralid = en.referralid\n" + "		JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" + "		WHERE\n" + "			(\n" + "				ca.adjusterid = "
				+ adjId + "\n" + "				OR ca.nursecasemanagerid = " + adjId + "\n" + "				OR rf.referringphysicianid = " + adjId + "\n" + "				OR rf.referredbycontactid = " + adjId + "\n" + "				OR ca.caseadministratorid = " + adjId + "\n" + "				OR ca.caseadministrator2id = " + adjId + "\n" + "				OR ca.caseadministrator3id = " + adjId + "\n" + "				OR ca.caseadministrator4id = " + adjId + "\n" + "			)\n" + "		AND encounterstatusid != 5\n" + "	) home_json";
	}
}
