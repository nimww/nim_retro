package com.n4.utils;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;

import com.n4.model.view.scheduling.Caseaccount;

public class Utils {
	private LinkedHashMap<Integer, String> stateId = new LinkedHashMap<Integer, String>();

	public boolean verifyAPIkey(String key) {
		Database db = new Database();

		String query = "select active from rest_api_token where token = '"+key+"'";
		
		boolean active = false;
		
		ResultSet rs = db.getRs(query);
		try {
			while (rs.next()) {
				active = rs.getBoolean(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		db.closeAll();
		
		return active;
	}

	public Utils() {
		stateId.put(1, "CA");
		stateId.put(2, "WA");
		stateId.put(3, "OR");
		stateId.put(4, "MT");
		stateId.put(5, "ID");
		stateId.put(6, "NV");
		stateId.put(7, "UT");
		stateId.put(8, "AZ");
		stateId.put(9, "WY");
		stateId.put(10, "CO");
		stateId.put(11, "NM");
		stateId.put(12, "ND");
		stateId.put(13, "SD");
		stateId.put(14, "NE");
		stateId.put(15, "KS");
		stateId.put(16, "OK");
		stateId.put(17, "TX");
		stateId.put(18, "MN");
		stateId.put(19, "IA");
		stateId.put(20, "MO");
		stateId.put(21, "AR");
		stateId.put(22, "LA");
		stateId.put(23, "WI");
		stateId.put(24, "IL");
		stateId.put(25, "MS");
		stateId.put(26, "MI");
		stateId.put(27, "IN");
		stateId.put(28, "KY");
		stateId.put(29, "TN");
		stateId.put(30, "AL");
		stateId.put(31, "OH");
		stateId.put(32, "GA");
		stateId.put(33, "FL");
		stateId.put(34, "ME");
		stateId.put(35, "NH");
		stateId.put(36, "VT");
		stateId.put(37, "NY");
		stateId.put(38, "PA");
		stateId.put(39, "WV");
		stateId.put(40, "VA");
		stateId.put(41, "NC");
		stateId.put(42, "SC");
		stateId.put(43, "MA");
		stateId.put(44, "RI");
		stateId.put(45, "CT");
		stateId.put(46, "NJ");
		stateId.put(47, "DE");
		stateId.put(48, "MD");
		stateId.put(49, "AK");
		stateId.put(50, "HI");
	}

	public long getHoursPassed(Date myStart) {
		return (getNow().getTime().getTime() - myStart.getTime()) / (60 * 60 * 1000);
	}

	private static Calendar getNow() {
		Calendar cal = java.util.Calendar.getInstance();
		cal.setTime(new java.util.Date());
		return cal;
	}

	public LinkedHashMap<Integer, String> getStateId() {
		return stateId;
	}
}
