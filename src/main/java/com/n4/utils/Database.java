package com.n4.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
	private Connection conn;
	private Statement statement;
	private ResultSet rs;
	public final static int TEST = 1;
	public final static int R = 2;
	public final static int RW = 3;
	public final static int AWS = 4;
	
	public Database() {
		if(Configuration.isTestEnviroment()){
			getConn(TEST);
		} else {
			getConn(RW);
		}
	}
	
	public Database(int dbType) {
		getConn(dbType);
	}
	
	public ResultSet getRs(String query){
		try {
			rs = statement.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	public void execUpdate(String query){
		try {
			statement.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private void getConn(int dbType){
		try {
			if(dbType==TEST){
				Class.forName(Configuration.JDBCdriverLocal);
				conn = DriverManager.getConnection(
						Configuration.HOSTLocal,
						Configuration.USERLocal,
						Configuration.PASSWORDLocal);
			} else if(dbType==RW){
				Class.forName(Configuration.JDBCdriver);
				conn = DriverManager.getConnection(
						Configuration.HOST,
						Configuration.USER,
						Configuration.PASSWORD);
			} else if(dbType==AWS){
				Class.forName(Configuration.AWSJDBCdriver);
				conn = DriverManager.getConnection(
						Configuration.AWSHOST,
						Configuration.AWSUSER,
						Configuration.AWSPASSWORD);
			} else {
//				Class.forName(Configuration.JDBCdriverSlave);
//				conn = DriverManager.getConnection(
//						Configuration.HOSTSlave,
//						Configuration.USERSlave,
//						Configuration.PASSWORDSlave);
			}

			statement = conn.createStatement();
			
		} catch (Exception eeee) {
			System.out.println(eeee);
		}
	}
	
	public void closeAll(){
		try{
			conn.close();
		} catch(Exception e){}
		try{
			statement.close();
		} catch(Exception e){}
		try{
			rs.close();
		} catch(Exception e){}
	}
}
