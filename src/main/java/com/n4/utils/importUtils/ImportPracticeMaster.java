package com.n4.utils.importUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gson.Gson;
import com.n4.model.api.geocode.Geocode;
import com.n4.model.collection.Practice;
import com.n4.model.collection.support.BackupPricing;
import com.n4.model.collection.support.ModalPricing;
import com.n4.model.collection.support.Modality;
import com.n4.model.collection.support.ModalityModel;
import com.n4.model.enums.*;
import com.n4.model.view.scheduling.Address;
import com.n4.model.view.scheduling.ContactAccount;
import com.n4.utils.Database;
import com.n4.utils.MongoTemplate;

public class ImportPracticeMaster implements MongoTemplate {
	private static final String API_KEY = "AIzaSyDaLVwN2ytziCPv7CvTFgFaY_6vHJgwRIU";

	public static void main(String[] args) {

		runJob();
	}

	public static void runJob() {
		//MongoTemplate.mongoOperation.dropCollection(Practice.class);
		Database db = new Database(Database.RW);
		Integer offset = 4000;
		
		ResultSet rs = db.getRs(getQuery(offset));
		System.out.println(getQuery(offset));
		try {
			while (rs.next()) {
				
				Practice practiceMaster = new Practice();
				Address address = new Address();
				ContactAccount contact = new ContactAccount();

				practiceMaster.setPracticeid(rs.getInt("practiceid"));
				
				contact.setCompany(rs.getString("practicename"));
				contact.setPhone(rs.getString("officephone"));
				contact.setFax(rs.getString("officefaxno"));
				contact.setEmail(rs.getString("officeemail"));
				
				address.setAddress(rs.getString("officeaddress1"));
				address.setSte(rs.getString("officeaddress2"));
				address.setCity(rs.getString("officecity"));
				address.setState(rs.getString("shortstate").trim());
				address.setZip(rs.getString("officezip"));
				contact.setAddress(address);
				
				practiceMaster.setContact(contact);
				practiceMaster.setContractStatus(contractStatus.get(rs.getInt("contractingstatusid")));
				
				List<BackupPricing> backupPricing = new ArrayList<BackupPricing>();
				backupPricing.add(new BackupPricing(rs.getInt("feeschedulerefid"),rs.getDouble("feepercentage")));
				backupPricing.add(new BackupPricing(rs.getInt("feescheduleoverrefid"),rs.getDouble("feeoverpercentage")));
				practiceMaster.setBackupPricing(backupPricing);
				
				LinkedHashMap<String, ModalPricing> modalPricing = new LinkedHashMap<String, ModalPricing>();
				modalPricing.put("mri", new ModalPricing(rs.getDouble("price_mod_mri_wo"), rs.getDouble("price_mod_mri_w"), rs.getDouble("price_mod_mri_wwo")));
				modalPricing.put("mri26", new ModalPricing(rs.getDouble("price_mod_mri_wo_26"), rs.getDouble("price_mod_mri_w_26"), rs.getDouble("price_mod_mri_wwo_26")));
				modalPricing.put("mritc", new ModalPricing(rs.getDouble("price_mod_mri_wo_tc"), rs.getDouble("price_mod_mri_w_tc"), rs.getDouble("price_mod_mri_wwo_tc")));
				modalPricing.put("ct", new ModalPricing(rs.getDouble("price_mod_ct_wo"), rs.getDouble("price_mod_ct_w"), rs.getDouble("price_mod_ct_wwo")));
				modalPricing.put("ct26", new ModalPricing(rs.getDouble("price_mod_ct_wo_26"), rs.getDouble("price_mod_ct_w_26"), rs.getDouble("price_mod_ct_wwo_26")));
				modalPricing.put("cttc", new ModalPricing(rs.getDouble("price_mod_ct_wo_tc"), rs.getDouble("price_mod_ct_w_tc"), rs.getDouble("price_mod_ct_wwo_tc")));
				modalPricing.put("ghmri", new ModalPricing(rs.getDouble("gh_price_mod_mri_wo"), rs.getDouble("gh_price_mod_mri_w"), rs.getDouble("gh_price_mod_mri_wwo")));
				modalPricing.put("ghmri26", new ModalPricing(rs.getDouble("gh_price_mod_mri_wo_26"), rs.getDouble("gh_price_mod_mri_w_26"), rs.getDouble("gh_price_mod_mri_wwo_26")));
				modalPricing.put("ghmritc", new ModalPricing(rs.getDouble("gh_price_mod_mri_wo_tc"), rs.getDouble("gh_price_mod_mri_w_tc"), rs.getDouble("gh_price_mod_mri_wwo_tc")));
				modalPricing.put("ghct", new ModalPricing(rs.getDouble("gh_price_mod_ct_wo"), rs.getDouble("gh_price_mod_ct_w"), rs.getDouble("gh_price_mod_ct_wwo")));
				modalPricing.put("ghct", new ModalPricing(rs.getDouble("gh_price_mod_ct_wo_26"), rs.getDouble("gh_price_mod_ct_w_26"), rs.getDouble("gh_price_mod_ct_wwo_26")));
				modalPricing.put("ghct", new ModalPricing(rs.getDouble("gh_price_mod_ct_wo_tc"), rs.getDouble("gh_price_mod_ct_w_tc"), rs.getDouble("gh_price_mod_ct_wwo_tc")));
				
				practiceMaster.setModalPricing(modalPricing);
				
				try {
					Geocode geocode = getGeocode(rs.getString("officeaddress1")+" "+rs.getString("officecity")+" "+rs.getString("shortstate")+" "+rs.getString("officezip"));
//					System.out.println("sdfsf "+geocode.getResults().get(0).getGeometry().getLocation().getLng()+" "+geocode.getResults().get(0).getGeometry().getLocation().getLat());
					Double[] dgeocode = {geocode.getResults().get(0).getGeometry().getLocation().getLng(),geocode.getResults().get(0).getGeometry().getLocation().getLat()}; 
					practiceMaster.setGeocode(dgeocode);
				} catch (Exception e) {
					System.out.println("GEOCODE FAil: "+rs.getInt("practiceid"));
					continue;
				}
				
				
				
				LinkedHashMap<ModalityType, Modality> modalityList = new LinkedHashMap<ModalityType, Modality>();
				Database db_in = new Database(Database.RW);
				ResultSet rs_in = db_in.getRs(getSubQuery(rs.getInt("practiceid")));
				try {
					while(rs_in.next()){
						
						Modality modality = new Modality();
						modality.setIsacr((rs_in.getInt("isacr")==1));
						modality.setAcrexpiration(rs_in.getDate("acrexpiration"));
						modality.setIsacr((rs_in.getInt("isacrverified")==1));
						modality.setAcrverifieddate(rs_in.getDate("acrverifieddate"));
						modality.setComments(rs_in.getString("comments"));
						
						ModalityModel mm = new ModalityModel();
						mm.setMriModelId(rs_in.getInt("mri_modelid"));
						mm.setClassLevel(rs_in.getInt("classlevel"));
						mm.setDesc(rs_in.getString("descriptionlong"));
						mm.setManufacturer(rs_in.getString("manufacturer"));
						mm.setModel(rs_in.getString("model"));
						mm.setOpen(rs_in.getBoolean("isopen"));
						modality.setModalityModel(mm);
						
						modalityList.put(ModalityType.valueOf(rs_in.getString("modalitytype")), modality);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				db_in.closeAll();
				practiceMaster.setModality(modalityList);
				MongoTemplate.mongoOperation.save(practiceMaster);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		db.closeAll();
	}
	private static LinkedHashMap<Integer, ContractStatus> contractStatus = new LinkedHashMap<Integer, ContractStatus>();
	static{
		contractStatus.put(0, ContractStatus.None);
		contractStatus.put(3, ContractStatus.Target);
		contractStatus.put(1, ContractStatus.OTA);
		contractStatus.put(2, ContractStatus.Contracted);
		contractStatus.put(11, ContractStatus.ContactedIPA);
		contractStatus.put(9, ContractStatus.ContractedStar);
		contractStatus.put(8, ContractStatus.ContractedSelect);
		contractStatus.put(7, ContractStatus.OON);
		contractStatus.put(4, ContractStatus.NotIntersted);
	}
	private static Geocode getGeocode(String address) throws MalformedURLException {
		Gson gson = new Gson();
		String apiURL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(address) + "&key=" + API_KEY;
		URL url = new URL(apiURL);
		System.out.println(apiURL);
		String json = "";
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
		for (String line; (line = reader.readLine()) != null;) {
		        json+=line;
		    }
		} catch (Exception e){
			
		}
		System.out.println("toJson: "+ gson.fromJson(json, Geocode.class));
		
		return gson.fromJson(json, Geocode.class);
	}
	private static String getSubQuery(Integer pid){
		return "SELECT\n" +
				"	REPLACE (\n" +
				"		REPLACE (\n" +
				"			REPLACE (tt.statusshort, '/', ''),\n" +
				"			' ',\n" +
				"			''\n" +
				"		),\n" +
				"		'.',\n" +
				"		''\n" +
				"	) modalitytype,\n" +
				"	tnim3_modality.isacr, tnim3_modality.acrexpiration, tnim3_modality.isacrverified, tnim3_modality.acrverifieddate, tnim3_modality.comments, tm.*\n" +
				"FROM\n" +
				"	tnim3_modality\n" +
				"JOIN tmri_modelli tm ON tm.mri_modelid = modalitymodelid\n" +
				"JOIN tmodalitytypeli tt ON tt.modalitytypeid = tnim3_modality.modalitytypeid\n" +
				"where tnim3_modality.practiceid = " + pid;
	}
	private static String getQuery(Integer offset){
		return "SELECT\n" +
				"	tpracticemaster.*, REPLACE (st.shortstate, '--', 'NA') shortstate\n" +
				"FROM\n" +
				"	tpracticemaster\n" +
				"JOIN tstateli st ON st.stateid = officestateid\n" +
				"WHERE\n" +
				"	contractingstatusid IN (1, 7, 2, 9, 8)\n" +
				"ORDER BY\n" +
				"	practiceid OFFSET "+offset+"\n" +
				"LIMIT 2000";
	}
	
}
