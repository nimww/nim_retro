package com.n4.utils.importUtils;

import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.n4.model.collection.PayerMaster;
import com.n4.model.view.scheduling.Address;
import com.n4.utils.Database;
import com.n4.utils.MongoTemplate;

public class ImportPayerMaster {

	public static void main(String[] args) {
		MongoTemplate.mongoOperation.dropCollection(PayerMaster.class);
		runJob();
	}
	
	public static void runJob(){
		Database db = new Database(Database.RW);
		
		//Log log = new Log(date, new ObjectId("53c98d5030048f39c366bac4"), "Import from psql to mongodb");
		
			ResultSet rs = db.getRs("select pm.*, replace(os.shortstate,'--','NA') ostate, replace(bs.shortstate,'--','NA') bstate from tnim3_payermaster pm join tstateli os on os.stateid = pm.officestateid join tstateli bs on bs.stateid = pm.billingstateid where parentpayerid = 0");
			try {
				while (rs.next()) {
					Address address = new Address();
					Contact contact = new Contact();
					Address baddress = new Address();
					Contact bcontact = new Contact();
					PayerMaster pm = new PayerMaster();
					
					address.setStreet(rs.getString("officeaddress1"));
					address.setSte(rs.getString("officeaddress2"));
					address.setCity(rs.getString("officecity"));
					address.setState(State.valueOf(rs.getString("ostate").trim()));
					address.setZip(rs.getString("officezip"));
					contact.setAddress(address);
					contact.setPhone(rs.getString("officephone"));
					contact.setFax(rs.getString("officefax"));
					contact.setEmail(rs.getString("officeemail"));
					contact.setContactName(rs.getString("contactname"));
					
					
					baddress.setStreet(rs.getString("billingaddress1"));
					baddress.setSte(rs.getString("billingaddress2"));
					baddress.setCity(rs.getString("billingcity"));
					baddress.setState(State.valueOf(rs.getString("bstate").trim()));
					baddress.setZip(rs.getString("billingzip"));
					bcontact.setAddress(address);
					bcontact.setPhone(rs.getString("billingphone"));
					bcontact.setFax(rs.getString("billingfax"));
					bcontact.setEmail(rs.getString("billingemail"));
					bcontact.setContactName(rs.getString("billingname"));
					
					//pm.setParentId(null);
					//pm.getLog().add(log);
					pm.setPayerId(rs.getInt("payerid"));
					pm.setParentPayerId(rs.getInt("parentpayerid"));
					pm.setContact(contact);
					pm.setBillingContact(bcontact);
					pm.setPayerName(rs.getString("payername"));
					pm.setNotes(rs.getString("importantnotes"));
					pm.setSalesDivision(rs.getString("salesdivision"));
					pm.setAcquisitionDivision(rs.getString("acquisitiondivision"));
					
					FStable fsTable = new FStable();
					FStable bfsTable = new FStable();
					
					fsTable.setPrimaryFS(rs.getInt("contract1_feescheduleid"));
					fsTable.setPrimaryFSPercentage(rs.getDouble("contract1_feepercentage"));
					fsTable.setSecondaryFS(rs.getInt("contract2_feescheduleid"));
					fsTable.setSecondaryFSPercentage(rs.getDouble("contract2_feepercentage"));
					fsTable.setTertiaryFS(rs.getInt("contract3_feescheduleid"));
					fsTable.setTertiaryFSPercentage(rs.getDouble("contract3_feepercentage"));
					pm.setContract(fsTable);
					
					bfsTable.setPrimaryFS(rs.getInt("bill1_feescheduleid"));
					bfsTable.setPrimaryFSPercentage(rs.getDouble("bill1_feepercentage"));
					bfsTable.setSecondaryFS(rs.getInt("bill2_feescheduleid"));
					bfsTable.setSecondaryFSPercentage(rs.getDouble("bill2_feepercentage"));
					bfsTable.setTertiaryFS(rs.getInt("bill3_feescheduleid"));
					bfsTable.setTertiaryFSPercentage(rs.getDouble("bill3_feepercentage"));
					pm.setBilling(bfsTable);
					
					pm.setSelectMriId(rs.getInt("selectmri_id"));
					pm.setSelectMriNotes(rs.getString("selectmri_notes"));

					Mongo.save(pm);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			rs = db.getRs("select pm.*, replace(os.shortstate,'--','NA') ostate, replace(bs.shortstate,'--','NA') bstate from tnim3_payermaster pm join tstateli os on os.stateid = pm.officestateid join tstateli bs on bs.stateid = pm.billingstateid where parentpayerid != 0");
			try {
				while (rs.next()) {
					Address address = new Address();
					Contact contact = new Contact();
					Address baddress = new Address();
					Contact bcontact = new Contact();
					PayerMaster pm = new PayerMaster();
					
					address.setStreet(rs.getString("officeaddress1"));
					address.setSte(rs.getString("officeaddress2"));
					address.setCity(rs.getString("officecity"));
					address.setState(State.valueOf(rs.getString("ostate").trim()));
					address.setZip(rs.getString("officezip"));
					contact.setAddress(address);
					contact.setPhone(rs.getString("officephone"));
					contact.setFax(rs.getString("officefax"));
					contact.setEmail(rs.getString("officeemail"));
					contact.setContactName(rs.getString("contactname"));
					
					
					baddress.setStreet(rs.getString("billingaddress1"));
					baddress.setSte(rs.getString("billingaddress2"));
					baddress.setCity(rs.getString("billingcity"));
					baddress.setState(State.valueOf(rs.getString("bstate").trim()));
					baddress.setZip(rs.getString("billingzip"));
					bcontact.setAddress(address);
					bcontact.setPhone(rs.getString("billingphone"));
					bcontact.setFax(rs.getString("billingfax"));
					bcontact.setEmail(rs.getString("billingemail"));
					bcontact.setContactName(rs.getString("billingname"));
					
					//pm.setParentId(null);
					//pm.getLog().add(log);
					pm.setId(new ObjectId());
					pm.setPayerId(rs.getInt("payerid"));
					pm.setParentPayerId(rs.getInt("parentpayerid"));
					pm.setContact(contact);
					pm.setBillingContact(bcontact);
					pm.setPayerName(rs.getString("payername"));
					pm.setNotes(rs.getString("importantnotes"));
					pm.setSalesDivision(rs.getString("salesdivision"));
					pm.setAcquisitionDivision(rs.getString("acquisitiondivision"));
					
					FStable fsTable = new FStable();
					FStable bfsTable = new FStable();
					
					fsTable.setPrimaryFS(rs.getInt("contract1_feescheduleid"));
					fsTable.setPrimaryFSPercentage(rs.getDouble("contract1_feepercentage"));
					fsTable.setSecondaryFS(rs.getInt("contract2_feescheduleid"));
					fsTable.setSecondaryFSPercentage(rs.getDouble("contract2_feepercentage"));
					fsTable.setTertiaryFS(rs.getInt("contract3_feescheduleid"));
					fsTable.setTertiaryFSPercentage(rs.getDouble("contract3_feepercentage"));
					pm.setContract(fsTable);
					
					bfsTable.setPrimaryFS(rs.getInt("bill1_feescheduleid"));
					bfsTable.setPrimaryFSPercentage(rs.getDouble("bill1_feepercentage"));
					bfsTable.setSecondaryFS(rs.getInt("bill2_feescheduleid"));
					bfsTable.setSecondaryFSPercentage(rs.getDouble("bill2_feepercentage"));
					bfsTable.setTertiaryFS(rs.getInt("bill3_feescheduleid"));
					bfsTable.setTertiaryFSPercentage(rs.getDouble("bill3_feepercentage"));
					pm.setBilling(bfsTable);
					
					pm.setSelectMriId(rs.getInt("selectmri_id"));
					pm.setSelectMriNotes(rs.getString("selectmri_notes"));
					
					Query query = new Query();
					query.addCriteria(Criteria.where("payerId").is(rs.getInt("parentpayerid")));
					System.out.println(query);
					try{
						PayerMaster parent = Mongo.getMongoOperation().findOne(query,PayerMaster.class);
						parent.getBranches().add(pm);
						Mongo.save(parent);
					} catch(Exception e){
					}
					
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	}

}
