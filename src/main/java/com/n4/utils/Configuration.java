package com.n4.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Properties;

public class Configuration {
	
//	private static String ConfigPath = "../n4-config.properties";
	
	public final static String DocumentPath = "/mnt/cloudstorage/nimdox/";
//	private static Properties prop = new Properties(properties.getProperties());
	
	public static final String OS = System.getProperty("os.name");
	public static final String dbType = "psql";
	public static final String JDBCdriver = "org.postgresql.Driver";
	public static final String HOST = "jdbc:postgresql://173.204.89.19:5432/v3_1?user=postgres&password=XdckqDG5CiQPncNqLe3c&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
	public static final String USER = "postgres";
	public static final String PASSWORD = "XdckqDG5CiQPncNqLe3c";
	
	public static final String AWSJDBCdriver = "org.postgresql.Driver";
	public static final String AWSHOST = "jdbc:postgresql://54.69.231.81:5432/test";
	public static final String AWSUSER = "postgres";
	public static final String AWSPASSWORD = "nim123password";
	
	public static final boolean DEBUG_MODE = false;
//	public static final String dbTypeSlave = prop.getProperty("dbTypeSlave");
//	public static final String JDBCdriverSlave = prop.getProperty("JDBCdriverSlave");
//	public static final String HOSTSlave = prop.getProperty("hostSlave");
//	public static final String USERSlave = prop.getProperty("userSlave");
//	public static final String PASSWORDSlave = prop.getProperty("passwordSlave");
	
	public static final String JDBCdriverLocal = "org.postgresql.Driver";
	public static final String HOSTLocal = "jdbc:postgresql://127.0.0.1:5432/v3_1";
	public static final String USERLocal = "postgres";
	public static final String PASSWORDLocal = "balls";//n4123password
	
	public static final String DATABASE_APPEND = "";
	
	public static boolean isTestEnviroment(){
		boolean isTest = false;
		if (OS.toLowerCase().contains("mac")){
			isTest = true;
		}
		
		return isTest;
	}
	
	public static void printLn(String s){
		if(Configuration.DEBUG_MODE){
			System.out.println(s);
		}
	}
	
//	public static final String isDev = prop.getProperty("dev");
	
//	private static class properties{
//    	private static Properties prop = new Properties();
//    	
//    	private static Properties getProperties(){
//    		try{
//    		prop.load(new FileInputStream(ConfigPath));
//    		} catch(Exception e){
//    			e.printStackTrace();
//    		}
//
//    		return prop;
//    	}
//    }
}
