package com.n4.utils;

import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


//Column	Required	Allowed		Special Char
//------	--------	-------		------------
//Seconds	YES			0-59		, - * /
//Minutes	YES			0-59		, - * /
//Hours		YES			0-23		, - * /
//DOM		YES			1-31		, - * ? / L W
//Month		YES			JAN-DEC		, - * /
//DOW		YES			SUN-SAT		, - * ? / L #
//Year		NO			1970-2099	, - * /

@Component
public class ScheduledTask {
//	
//	@Scheduled(cron = "0 15 18 25 DEC *")
//	public void importFS() {
//		FeeSchedule2 FeeSchedule2 = new FeeSchedule2();
//		System.out.println("importFS start: "+new Date());
//		FeeSchedule2.runJob();
//		System.out.println("importFS end: "+new Date());
//		
//	}

}
