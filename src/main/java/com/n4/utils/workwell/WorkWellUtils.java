package com.n4.utils.workwell;

import java.util.Date;
import java.util.List;

import com.n4.model.view.pt.PTBilling;
import com.n4.security.Utils;
import com.n4.utils.MongoTemplate;

public class WorkWellUtils implements MongoTemplate {
	public void getFs(Integer pid, String zip, Integer qty, String cpt, Date effDate, String auth){
		String result = "";
		Utils security = new Utils();
		if(security.checkAuth(auth)){
		} else {
			result = "{status : \"Not authorized\"}";
		}
	}
	
	public String submitPtBilling(PTBilling pTBilling){
		
		MongoTemplate.mongoOperation.save(pTBilling);
		return "OK";
	}
	
	public List<PTBilling> getPtBilling(){
		return MongoTemplate.mongoOperation.findAll(PTBilling.class);
	}
}
