package com.n4.model.view.accounting;

public class PrePaymentWorklist {
	String caseid;
	String patient;
	String claim;
	String payer;
	String dos;
	String imageCenter;
	String status;
	
	public String getPatient() {
		return patient;
	}
	public void setPatient(String patient) {
		this.patient = patient;
	}
	public String getPayer() {
		return payer;
	}
	public void setPayer(String payer) {
		this.payer = payer;
	}
	public String getDos() {
		return dos;
	}
	public void setDos(String dos) {
		this.dos = dos;
	}
	public String getImageCenter() {
		return imageCenter;
	}
	public void setImageCenter(String imageCenter) {
		this.imageCenter = imageCenter;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCaseid() {
		return caseid;
	}
	public void setCaseid(String caseid) {
		this.caseid = caseid;
	}
	public String getClaim() {
		return claim;
	}
	public void setClaim(String claim) {
		this.claim = claim;
	}
}
