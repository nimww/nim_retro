package com.n4.model.view.netdev;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.n4.utils.Database;

public class ModalManager {
	private String modal;
	private String contrast;
	
	public ModalManager() {
	}
	public ModalManager(String modal, String contrast) {
		this.modal = modal;
		this.contrast = contrast;
	}
	public ModalManager(Cpt cpt){
		queryFields(cpt.getCpt());
	}
	
	public String getModal() {
		return modal;
	}
	public void setModal(String modal) {
		this.modal = modal;
	}
	public String getContrast() {
		return contrast;
	}
	public void setContrast(String contrast) {
		this.contrast = contrast;
	}
	
	public void queryFields(String cpt){
		Database db = new Database(Database.RW);
		ResultSet rs = db.getRs("select replace(lower(modality),'mr','mri') as modality, replace(replace(replace(lower(contrast),'withandwithout','WithAndWithOut'),'without','WithOut'),'with','With') contrast from tcptwizard where cpt1 = '"+cpt+"' limit 1");
		PracticeQuery pq = null;
		try {
			while(rs.next()){
				modal = rs.getString("modality");
				contrast = rs.getString("contrast");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
