package com.n4.model.view.netdev;

public class Gmap {
	private Integer zoom;
	private Cords center;
	public Integer getZoom() {
		return zoom;
	}
	public void setZoom(Integer zoom) {
		this.zoom = zoom;
	}
	public Cords getCenter() {
		return center;
	}
	public void setCenter(Cords center) {
		this.center = center;
	}
	
}
