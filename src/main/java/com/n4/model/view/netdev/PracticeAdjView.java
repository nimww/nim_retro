package com.n4.model.view.netdev;

import java.util.List;

public class PracticeAdjView {
	private List<Markers> markers;
	private List<PracticeView> results;
	private Gmap gmap;
	public List<Markers> getMarkers() {
		return markers;
	}
	public void setMarkers(List<Markers> markers) {
		this.markers = markers;
	}
	public List<PracticeView> getResults() {
		return results;
	}
	public void setResults(List<PracticeView> results) {
		this.results = results;
	}
	public Gmap getGmap() {
		return gmap;
	}
	public void setGmap(Gmap gmap) {
		this.gmap = gmap;
	}
}
