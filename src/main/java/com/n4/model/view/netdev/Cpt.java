package com.n4.model.view.netdev;

public class Cpt {
	private String cpt;
	private Integer qty;
	private String modality;
	private String mod;
	private Double practiceCost;
	
	public String getCpt() {
		return cpt;
	}

	public void setCpt(String cpt) {
		this.cpt = cpt;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public String getMod() {
		return mod;
	}

	public void setMod(String mod) {
		this.mod = mod;
	}

	public Double getPracticeCost() {
		return practiceCost;
	}

	public void setPracticeCost(Double practiceCost) {
		this.practiceCost = practiceCost;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
}
