package com.n4.model.view.netdev;

import java.util.List;

public class PracticeQuery {
	private List<FS> payerfs;
	private String fulladdress;
	private String zip;
	private List<Cpt> cpts;
	private Double payerallow;
	public List<FS> getPayerfs() {
		return payerfs;
	}
	public void setPayerfs(List<FS> payerfs) {
		this.payerfs = payerfs;
	}
	public String getFulladdress() {
		return fulladdress;
	}
	public void setFulladdress(String fulladdress) {
		this.fulladdress = fulladdress;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public List<Cpt> getCpts() {
		return cpts;
	}
	public void setCpts(List<Cpt> cpts) {
		this.cpts = cpts;
	}
	public Double getPayerallow() {
		return payerallow;
	}
	public void setPayerallow(Double payerallow) {
		this.payerallow = payerallow;
	}
	
	
}