package com.n4.model.view.netdev;

import java.util.Comparator;

import org.springframework.data.mongodb.core.geo.Distance;

import com.n4.model.collection.Practice;


public class PracticeView implements Comparator<PracticeView> {
	private Integer id;
	private Cords cords;
	private String company;
	private Practice practice;
	private Distance distance;
	private Double cost;
	private String macScore;
	
	public Practice getPractice() {
		return practice;
	}
	public void setPractice(Practice practice) {
		this.practice = practice;
	}
	public Distance getDistance() {
		return distance;
	}
	public void setDistance(Distance distance) {
		this.distance = distance;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public String getMacScore() {
		return macScore;
	}
	public void setMacScore(String macScore) {
		this.macScore = macScore;
	}
	@Override
	public int compare(PracticeView arg0, PracticeView arg1) {
		return new Integer(arg1.macScore)-new Integer(arg0.macScore);
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Cords getCords() {
		return cords;
	}
	public void setCords(Cords cords) {
		this.cords = cords;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	
}
