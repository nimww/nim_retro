package com.n4.model.view.netdev;

public class FS {
	private Integer fsid;
	private Double fspercentage;
	public Integer getFsid() {
		return fsid;
	}
	public void setFsid(Integer fsid) {
		this.fsid = fsid;
	}
	public Double getFspercentage() {
		return fspercentage;
	}
	public void setFspercentage(Double fspercentage) {
		this.fspercentage = fspercentage;
	}
	
}
