package com.n4.model.view.netdev;

public class Markers {
	private Integer id;
	private Cords cords;
	private String company;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Cords getCords() {
		return cords;
	}
	public void setCords(Cords cords) {
		this.cords = cords;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company.replace(" ", "\u00A0");
	}
	
}
