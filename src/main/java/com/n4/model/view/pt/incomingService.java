package com.n4.model.view.pt;

public class incomingService {
	public String cpt;
	public String bp;
	public String mod;
	public String qty;
	public String diag1;
	public String diag2;
	public String diag3;
	public String diag4;

	public String getCpt() {
		return cpt;
	}
	public void setCpt(String cpt) {
		this.cpt = cpt;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}
	public String getMod() {
		return mod;
	}
	public void setMod(String mod) {
		this.mod = mod;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getDiag1() {
		return diag1;
	}
	public void setDiag1(String diag1) {
		this.diag1 = diag1;
	}
	public String getDiag2() {
		return diag2;
	}
	public void setDiag2(String diag2) {
		this.diag2 = diag2;
	}
	public String getDiag3() {
		return diag3;
	}
	public void setDiag3(String diag3) {
		this.diag3 = diag3;
	}
	public String getDiag4() {
		return diag4;
	}
	public void setDiag4(String diag4) {
		this.diag4 = diag4;
	}
	
}
