package com.n4.model.view.pt;

import java.util.List;

public class PTBilling {

	public String REFERRAL_SOURCE;
	public String REFERRAL_NOTES;
	public String patientfirst;
	public String patientlast;
	public String patientaddress;
	public String patientaddress2;
	public String patientcity;
	public String patientstate;
	public String patientzip;
	public String patientemail;
	public String patientphone;
	public String patientcellphone;
	public String patientdob;
	public String patientssn;

	public String patientavailability;

	public String employername;
	public String employerfax;
	public String employerphone;

	public String INJURY_DESCRIPTION;

	public String height;
	public String weight;
	public String gender;
	public String doi;
	public String onmeds;
	public String previousimage;
	public String pregnant;
	public String pregnantdescreption;
	List<incomingService> incomingServiceList;
	
	public String getREFERRAL_SOURCE() {
		return REFERRAL_SOURCE;
	}
	public void setREFERRAL_SOURCE(String rEFERRAL_SOURCE) {
		REFERRAL_SOURCE = rEFERRAL_SOURCE;
	}
	public String getREFERRAL_NOTES() {
		return REFERRAL_NOTES;
	}
	public void setREFERRAL_NOTES(String rEFERRAL_NOTES) {
		REFERRAL_NOTES = rEFERRAL_NOTES;
	}
	public String getPatientfirst() {
		return patientfirst;
	}
	public void setPatientfirst(String patientfirst) {
		this.patientfirst = patientfirst;
	}
	public String getPatientlast() {
		return patientlast;
	}
	public void setPatientlast(String patientlast) {
		this.patientlast = patientlast;
	}
	public String getPatientaddress() {
		return patientaddress;
	}
	public void setPatientaddress(String patientaddress) {
		this.patientaddress = patientaddress;
	}
	public String getPatientaddress2() {
		return patientaddress2;
	}
	public void setPatientaddress2(String patientaddress2) {
		this.patientaddress2 = patientaddress2;
	}
	public String getPatientcity() {
		return patientcity;
	}
	public void setPatientcity(String patientcity) {
		this.patientcity = patientcity;
	}
	public String getPatientstate() {
		return patientstate;
	}
	public void setPatientstate(String patientstate) {
		this.patientstate = patientstate;
	}
	public String getPatientzip() {
		return patientzip;
	}
	public void setPatientzip(String patientzip) {
		this.patientzip = patientzip;
	}
	public String getPatientemail() {
		return patientemail;
	}
	public void setPatientemail(String patientemail) {
		this.patientemail = patientemail;
	}
	public String getPatientphone() {
		return patientphone;
	}
	public void setPatientphone(String patientphone) {
		this.patientphone = patientphone;
	}
	public String getPatientcellphone() {
		return patientcellphone;
	}
	public void setPatientcellphone(String patientcellphone) {
		this.patientcellphone = patientcellphone;
	}
	public String getPatientdob() {
		return patientdob;
	}
	public void setPatientdob(String patientdob) {
		this.patientdob = patientdob;
	}
	public String getPatientssn() {
		return patientssn;
	}
	public void setPatientssn(String patientssn) {
		this.patientssn = patientssn;
	}
	public String getPatientavailability() {
		return patientavailability;
	}
	public void setPatientavailability(String patientavailability) {
		this.patientavailability = patientavailability;
	}
	public String getEmployername() {
		return employername;
	}
	public void setEmployername(String employername) {
		this.employername = employername;
	}
	public String getEmployerfax() {
		return employerfax;
	}
	public void setEmployerfax(String employerfax) {
		this.employerfax = employerfax;
	}
	public String getEmployerphone() {
		return employerphone;
	}
	public void setEmployerphone(String employerphone) {
		this.employerphone = employerphone;
	}
	public String getINJURY_DESCRIPTION() {
		return INJURY_DESCRIPTION;
	}
	public void setINJURY_DESCRIPTION(String iNJURY_DESCRIPTION) {
		INJURY_DESCRIPTION = iNJURY_DESCRIPTION;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public String getOnmeds() {
		return onmeds;
	}
	public void setOnmeds(String onmeds) {
		this.onmeds = onmeds;
	}
	public String getPreviousimage() {
		return previousimage;
	}
	public void setPreviousimage(String previousimage) {
		this.previousimage = previousimage;
	}
	public String getPregnant() {
		return pregnant;
	}
	public void setPregnant(String pregnant) {
		this.pregnant = pregnant;
	}
	public String getPregnantdescreption() {
		return pregnantdescreption;
	}
	public void setPregnantdescreption(String pregnantdescreption) {
		this.pregnantdescreption = pregnantdescreption;
	}
	public List<incomingService> getIncomingServiceList() {
		return incomingServiceList;
	}
	public void setIncomingServiceList(List<incomingService> incomingServiceList) {
		this.incomingServiceList = incomingServiceList;
	}

}
