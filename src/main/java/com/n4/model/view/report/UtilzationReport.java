package com.n4.model.view.report;

import java.util.List;

public class UtilzationReport {
	private List<Payers> payers;

	public List<Payers> getPayers() {
		return payers;
	}

	public void setPayers(List<Payers> payers) {
		this.payers = payers;
	}

}
