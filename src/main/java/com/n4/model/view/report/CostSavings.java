package com.n4.model.view.report;

public class CostSavings {
	private String type;
	private String quantity;
	private String statefs;
	private String nimfs;
	private String savings;
	private String percent;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getStatefs() {
		return statefs;
	}
	public void setStatefs(String statefs) {
		this.statefs = statefs;
	}
	public String getNimfs() {
		return nimfs;
	}
	public void setNimfs(String nimfs) {
		this.nimfs = nimfs;
	}
	public String getSavings() {
		return savings;
	}
	public void setSavings(String savings) {
		this.savings = savings;
	}
	public String getPercent() {
		return percent;
	}
	public void setPercent(String percent) {
		this.percent = percent;
	}
	
	
}
