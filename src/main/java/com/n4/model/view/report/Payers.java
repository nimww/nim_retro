package com.n4.model.view.report;

import java.util.List;

public class Payers {
	private String payername;
	private List<Referrals> referrals;
	public String getPayername() {
		return payername;
	}
	public void setPayername(String payername) {
		this.payername = payername;
	}
	public List<Referrals> getReferrals() {
		return referrals;
	}
	public void setReferrals(List<Referrals> referrals) {
		this.referrals = referrals;
	}
	
}
