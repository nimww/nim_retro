package com.n4.model.view.report;

public class Utilization {
	private String dateOne;
	private String dateTwo;
	private String viewAdmin;
	private String payerSelection;
	public String getDateOne() {
		return dateOne;
	}
	public void setDateOne(String dateOne) {
		this.dateOne = dateOne;
	}
	public String getDateTwo() {
		return dateTwo;
	}
	public void setDateTwo(String dateTwo) {
		this.dateTwo = dateTwo;
	}
	public String getViewAdmin() {
		return viewAdmin;
	}
	public void setViewAdmin(String viewAdmin) {
		this.viewAdmin = viewAdmin;
	}
	public String getPayerSelection() {
		return payerSelection;
	}
	public void setPayerSelection(String payerSelection) {
		this.payerSelection = payerSelection;
	}
	@Override
	public String toString() {
		return "Utilization [dateOne=" + dateOne + ", dateTwo=" + dateTwo + ", viewAdmin=" + viewAdmin + ", payerSelection=" + payerSelection + "]";
	}
	
}
