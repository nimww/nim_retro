package com.n4.model.view.report;

public class Referrals {
	private Caseadmin caseadmin;
	private Integer jan;
	private Integer feb;
	private Integer mar;
	private Integer apr;
	private Integer may;
	private Integer jun;
	private Integer jul;
	private Integer aug;
	private Integer sep;
	private Integer oct;
	private Integer nov;
	private Integer dec;
	public Caseadmin getCaseadmin() {
		return caseadmin;
	}
	public void setCaseadmin(Caseadmin caseadmin) {
		this.caseadmin = caseadmin;
	}
	public Integer getJan() {
		return jan;
	}
	public void setJan(Integer jan) {
		this.jan = jan;
	}
	public Integer getFeb() {
		return feb;
	}
	public void setFeb(Integer feb) {
		this.feb = feb;
	}
	public Integer getMar() {
		return mar;
	}
	public void setMar(Integer mar) {
		this.mar = mar;
	}
	public Integer getApr() {
		return apr;
	}
	public void setApr(Integer apr) {
		this.apr = apr;
	}
	public Integer getMay() {
		return may;
	}
	public void setMay(Integer may) {
		this.may = may;
	}
	public Integer getJun() {
		return jun;
	}
	public void setJun(Integer jun) {
		this.jun = jun;
	}
	public Integer getJul() {
		return jul;
	}
	public void setJul(Integer jul) {
		this.jul = jul;
	}
	public Integer getAug() {
		return aug;
	}
	public void setAug(Integer aug) {
		this.aug = aug;
	}
	public Integer getSep() {
		return sep;
	}
	public void setSep(Integer sep) {
		this.sep = sep;
	}
	public Integer getOct() {
		return oct;
	}
	public void setOct(Integer oct) {
		this.oct = oct;
	}
	public Integer getNov() {
		return nov;
	}
	public void setNov(Integer nov) {
		this.nov = nov;
	}
	public Integer getDec() {
		return dec;
	}
	public void setDec(Integer dec) {
		this.dec = dec;
	}
}
