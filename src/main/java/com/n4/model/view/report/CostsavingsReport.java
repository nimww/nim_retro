package com.n4.model.view.report;

public class CostsavingsReport {
	private String payername;
	private CostSavings cost_savings;
	public String getPayername() {
		return payername;
	}
	public void setPayername(String payername) {
		this.payername = payername;
	}
	public CostSavings getCost_savings() {
		return cost_savings;
	}
	public void setCost_savings(CostSavings cost_savings) {
		this.cost_savings = cost_savings;
	}
	
}
