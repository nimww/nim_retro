package com.n4.model.view.reports;

import java.util.List;

public class ReportingList {
	private List<AltReporting> altreporting;

	public List<AltReporting> getAltReporting() {
		return altreporting;
	}

	public void setAltReporting(List<AltReporting> altReporting) {
		this.altreporting = altReporting;
	}
	
	
}
