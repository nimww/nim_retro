package com.n4.model.view.reports;

public class Admin {
	private Integer userid;
	private Boolean sentreport;
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public Boolean getSentreport() {
		return sentreport;
	}
	public void setSentreport(Boolean sentreport) {
		this.sentreport = sentreport;
	}
	
}
