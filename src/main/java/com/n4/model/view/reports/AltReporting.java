package com.n4.model.view.reports;

import java.util.List;

public class AltReporting {
	private String scanpass;
	private Integer encounterstatusid;
	private Integer caseid;
	private Integer encounterid;
	private String receivedate;
	private Integer payerid;
	private Integer parentpayerid;
	private String payername;
	private List<Admin> admins;
	private String status;
	private Boolean isGood;
	public String getScanpass() {
		return scanpass;
	}
	public void setScanpass(String scanpass) {
		this.scanpass = scanpass;
	}
	public Integer getEncounterstatusid() {
		return encounterstatusid;
	}
	public void setEncounterstatusid(Integer encounterstatusid) {
		this.encounterstatusid = encounterstatusid;
	}
	public Integer getPayerid() {
		return payerid;
	}
	public void setPayerid(Integer payerid) {
		this.payerid = payerid;
	}
	public Integer getParentpayerid() {
		return parentpayerid;
	}
	public void setParentpayerid(Integer parentpayerid) {
		this.parentpayerid = parentpayerid;
	}
	public List<Admin> getAdmins() {
		return admins;
	}
	public void setAdmins(List<Admin> admins) {
		this.admins = admins;
	}
	public String getReceivedate() {
		return receivedate;
	}
	public void setReceivedate(String receivedate) {
		this.receivedate = receivedate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getIsGood() {
		return isGood;
	}
	public void setIsGood(Boolean isGood) {
		this.isGood = isGood;
	}
	public String getPayername() {
		return payername;
	}
	public void setPayername(String payername) {
		this.payername = payername;
	}
	public Integer getCaseid() {
		return caseid;
	}
	public void setCaseid(Integer caseid) {
		this.caseid = caseid;
	}
	public Integer getEncounterid() {
		return encounterid;
	}
	public void setEncounterid(Integer encounterid) {
		this.encounterid = encounterid;
	}
	
}
