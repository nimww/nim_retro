package com.n4.model.view.scheduling;

public class Practice {
	private String practiceid;
	private String practicename;
	private String address;
	private String ste;
	private String city;
	private String state;
	private String zip;
	private String phone;
	private String fax;
	public String getPracticeid() {
		return practiceid;
	}
	public void setPracticeid(String practiceid) {
		this.practiceid = practiceid;
	}
	public String getPracticename() {
		return practicename;
	}
	public void setPracticename(String practicename) {
		this.practicename = practicename;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getSte() {
		return ste;
	}
	public void setSte(String ste) {
		this.ste = ste;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
}
