package com.n4.model.view.scheduling;

public class AdjDashboardModule {
	private String patient;
	private Integer caseid;
	private String caseclaimnumber;
	private String receivedate;
	private Integer encounterid;
	private String scanpass;
	public String getPatient() {
		return patient;
	}
	public void setPatient(String patient) {
		this.patient = patient;
	}
	public Integer getCaseid() {
		return caseid;
	}
	public void setCaseid(Integer caseid) {
		this.caseid = caseid;
	}
	public String getCaseclaimnumber() {
		return caseclaimnumber;
	}
	public void setCaseclaimnumber(String caseclaimnumber) {
		this.caseclaimnumber = caseclaimnumber;
	}
	public String getReceivedate() {
		return receivedate;
	}
	public void setReceivedate(String receivedate) {
		this.receivedate = receivedate;
	}
	public Integer getEncounterid() {
		return encounterid;
	}
	public void setEncounterid(Integer encounterid) {
		this.encounterid = encounterid;
	}
	public String getScanpass() {
		return scanpass;
	}
	public void setScanpass(String scanpass) {
		this.scanpass = scanpass;
	}
}
