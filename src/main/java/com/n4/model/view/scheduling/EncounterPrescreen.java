package com.n4.model.view.scheduling;

public class EncounterPrescreen {
	private String patientavailability;
    private Boolean stat; 
    private Boolean requiresfilms; 
    private Boolean requiresageinjury; 
    private Boolean courtesy; 
    private Boolean requireshandcarrycd; 
    private Boolean retro; 
    private Boolean vip; 
    private Boolean requiresonlineimage;
	public String getPatientavailability() {
		return patientavailability;
	}
	public void setPatientavailability(String patientavailability) {
		this.patientavailability = patientavailability;
	}
	public Boolean getStat() {
		return stat;
	}
	public void setStat(Boolean stat) {
		this.stat = stat;
	}
	public Boolean getRequiresfilms() {
		return requiresfilms;
	}
	public void setRequiresfilms(Boolean requiresfilms) {
		this.requiresfilms = requiresfilms;
	}
	public Boolean getRequiresageinjury() {
		return requiresageinjury;
	}
	public void setRequiresageinjury(Boolean requiresageinjury) {
		this.requiresageinjury = requiresageinjury;
	}
	public Boolean getCourtesy() {
		return courtesy;
	}
	public void setCourtesy(Boolean courtesy) {
		this.courtesy = courtesy;
	}
	public Boolean getRequireshandcarrycd() {
		return requireshandcarrycd;
	}
	public void setRequireshandcarrycd(Boolean requireshandcarrycd) {
		this.requireshandcarrycd = requireshandcarrycd;
	}
	public Boolean getRetro() {
		return retro;
	}
	public void setRetro(Boolean retro) {
		this.retro = retro;
	}
	public Boolean getVip() {
		return vip;
	}
	public void setVip(Boolean vip) {
		this.vip = vip;
	}
	public Boolean getRequiresonlineimage() {
		return requiresonlineimage;
	}
	public void setRequiresonlineimage(Boolean requiresonlineimage) {
		this.requiresonlineimage = requiresonlineimage;
	}
}
