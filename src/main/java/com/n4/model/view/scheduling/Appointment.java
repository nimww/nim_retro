package com.n4.model.view.scheduling;

public class Appointment {
	private Integer appointmentid;
	private String appointmenttime;
	private String comments;
	private String status;
	private Practice practice;
	private ContactAccount scheduler;
	public Integer getAppointmentid() {
		return appointmentid;
	}
	public void setAppointmentid(Integer appointmentid) {
		this.appointmentid = appointmentid;
	}
	public String getAppointmenttime() {
		return appointmenttime;
	}
	public void setAppointmenttime(String appointmenttime) {
		this.appointmenttime = appointmenttime;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Practice getPractice() {
		return practice;
	}
	public void setPractice(Practice practice) {
		this.practice = practice;
	}
	public ContactAccount getScheduler() {
		return scheduler;
	}
	public void setScheduler(ContactAccount scheduler) {
		this.scheduler = scheduler;
	}
}
