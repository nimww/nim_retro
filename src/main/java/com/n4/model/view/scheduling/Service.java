package com.n4.model.view.scheduling;

public class Service {
	private Integer serviceid; 
    private String cpt; 
    private String cptmodifier; 
    private String cptqty; 
    private String cptbodypart;
    private String dcpt1; 
    private String dcpt2;
    private String dcpt3; 
    private String dcpt4; 
    private String status;
    private String servicetype;
	public Integer getServiceid() {
		return serviceid;
	}
	public void setServiceid(Integer serviceid) {
		this.serviceid = serviceid;
	}
	public String getCpt() {
		return cpt;
	}
	public void setCpt(String cpt) {
		this.cpt = cpt;
	}
	public String getCptmodifier() {
		return cptmodifier;
	}
	public void setCptmodifier(String cptmodifier) {
		this.cptmodifier = cptmodifier;
	}
	public String getCptqty() {
		return cptqty;
	}
	public void setCptqty(String cptqty) {
		this.cptqty = cptqty;
	}
	public String getCptbodypart() {
		return cptbodypart;
	}
	public void setCptbodypart(String cptbodypart) {
		this.cptbodypart = cptbodypart;
	}
	public String getDcpt1() {
		return dcpt1;
	}
	public void setDcpt1(String dcpt1) {
		this.dcpt1 = dcpt1;
	}
	public String getDcpt2() {
		return dcpt2;
	}
	public void setDcpt2(String dcpt2) {
		this.dcpt2 = dcpt2;
	}
	public String getDcpt3() {
		return dcpt3;
	}
	public void setDcpt3(String dcpt3) {
		this.dcpt3 = dcpt3;
	}
	public String getDcpt4() {
		return dcpt4;
	}
	public void setDcpt4(String dcpt4) {
		this.dcpt4 = dcpt4;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getServicetype() {
		return servicetype;
	}
	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	} 
    
    
}
