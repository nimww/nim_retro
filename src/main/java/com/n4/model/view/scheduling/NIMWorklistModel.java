package com.n4.model.view.scheduling;

import java.util.Date;

public class NIMWorklistModel {
	private String patient;
	private String payer;
	private String state;
	private String scanpass;
	private String status;
	private String hours;
	private String caseid;
	private String encounterid;
	private Date nad;
	private String naNotes;
	private String assignedTo;
	
	public String getPatient() {
		return patient;
	}
	public void setPatient(String patient) {
		this.patient = patient;
	}
	public String getPayer() {
		return payer;
	}
	public void setPayer(String payer) {
		this.payer = payer;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getScanpass() {
		return scanpass;
	}
	public void setScanpass(String scanpass) {
		this.scanpass = scanpass;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public String getCaseid() {
		return caseid;
	}
	public void setCaseid(String caseid) {
		this.caseid = caseid;
	}
	public Date getNad() {
		return nad;
	}
	public void setNad(Date nad) {
		this.nad = nad;
	}
	public String getNaNotes() {
		return naNotes;
	}
	public void setNaNotes(String naNotes) {
		this.naNotes = naNotes;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getEncounterid() {
		return encounterid;
	}
	public void setEncounterid(String encounterid) {
		this.encounterid = encounterid;
	}
}
