package com.n4.model.view.scheduling;

import java.util.List;

public class AdjDashboardList {
	List<AdjDashboardModule> pending;
	List<AdjDashboardModule> scheduled;
	List<AdjDashboardModule> reportready;
	List<AdjDashboardModule> completed;
	public List<AdjDashboardModule> getPending() {
		return pending;
	}
	public void setPending(List<AdjDashboardModule> pending) {
		this.pending = pending;
	}
	public List<AdjDashboardModule> getScheduled() {
		return scheduled;
	}
	public void setScheduled(List<AdjDashboardModule> scheduled) {
		this.scheduled = scheduled;
	}
	public List<AdjDashboardModule> getReportready() {
		return reportready;
	}
	public void setReportready(List<AdjDashboardModule> reportready) {
		this.reportready = reportready;
	}
	public List<AdjDashboardModule> getCompleted() {
		return completed;
	}
	public void setCompleted(List<AdjDashboardModule> completed) {
		this.completed = completed;
	}
	
}
