package com.n4.model.view.scheduling;

import java.util.List;

public class Encounter {
	private Integer encounterid; 
    private String scanpass; 
    private Integer encountertypeid; 
    private String dateofservice; 
    private Boolean isnextaction;
    private String nextactiondate; 
	private String nextactionnotes; 
	private EncounterPrescreen prescreen;
	private Boolean rxreview;
    private Boolean rpreview;
    private List<Document> documents;
    private List<Appointment> appointments;
    private List<Commtrack> commtracks;
    private List<Service> services;
    private List<Notification> notifications;
	public Integer getEncounterid() {
		return encounterid;
	}
	public void setEncounterid(Integer encounterid) {
		this.encounterid = encounterid;
	}
	public String getScanpass() {
		return scanpass;
	}
	public void setScanpass(String scanpass) {
		this.scanpass = scanpass;
	}
	public Integer getEncountertypeid() {
		return encountertypeid;
	}
	public void setEncountertypeid(Integer encountertypeid) {
		this.encountertypeid = encountertypeid;
	}
	public String getDateofservice() {
		return dateofservice;
	}
	public void setDateofservice(String dateofservice) {
		this.dateofservice = dateofservice;
	}
	public String getNextactiondate() {
		return nextactiondate;
	}
	public void setNextactiondate(String nextactiondate) {
		this.nextactiondate = nextactiondate;
	}
	public String getNextactionnotes() {
		return nextactionnotes;
	}
	public void setNextactionnotes(String nextactionnotes) {
		this.nextactionnotes = nextactionnotes;
	}
	public EncounterPrescreen getPrescreen() {
		return prescreen;
	}
	public void setPrescreen(EncounterPrescreen prescreen) {
		this.prescreen = prescreen;
	}
	public Boolean getRxreview() {
		return rxreview;
	}
	public void setRxreview(Boolean rxreview) {
		this.rxreview = rxreview;
	}
	public Boolean getRpreview() {
		return rpreview;
	}
	public void setRpreview(Boolean rpreview) {
		this.rpreview = rpreview;
	}
	public List<Document> getDocuments() {
		return documents;
	}
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	public List<Appointment> getAppointments() {
		return appointments;
	}
	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}
	public List<Service> getServices() {
		return services;
	}
	public void setServices(List<Service> services) {
		this.services = services;
	}
	public List<Commtrack> getCommtracks() {
		return commtracks;
	}
	public void setCommtracks(List<Commtrack> commtracks) {
		this.commtracks = commtracks;
	}
	public List<Notification> getNotifications() {
		return notifications;
	}
	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}
	public Boolean getIsnextaction() {
		return isnextaction;
	}
	public void setIsnextaction(Boolean isnextaction) {
		this.isnextaction = isnextaction;
	}
}
