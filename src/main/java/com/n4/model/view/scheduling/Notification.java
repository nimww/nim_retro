package com.n4.model.view.scheduling;

import java.util.Comparator;

public class Notification  implements Comparator<Notification>{
	private Integer notificationsort;
	private String userid;
	private ContactAccount user;
	private String role;
	private Boolean conf;
	private Boolean sp;
	private Boolean rp;
	@Override
	public int compare(Notification arg0, Notification arg1) {
		return arg0.notificationsort-arg1.notificationsort;
	}

	public Integer getNotificationsort() {
		return notificationsort;
	}

	public void setNotificationsort(Integer notificationsort) {
		this.notificationsort = notificationsort;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public ContactAccount getUser() {
		return user;
	}

	public void setUser(ContactAccount user) {
		this.user = user;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Boolean getConf() {
		return conf;
	}

	public void setConf(Boolean conf) {
		this.conf = conf;
	}

	public Boolean getSp() {
		return sp;
	}

	public void setSp(Boolean sp) {
		this.sp = sp;
	}

	public Boolean getRp() {
		return rp;
	}

	public void setRp(Boolean rp) {
		this.rp = rp;
	}
}
