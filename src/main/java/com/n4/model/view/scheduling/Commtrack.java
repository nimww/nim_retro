package com.n4.model.view.scheduling;

public class Commtrack {
	private Integer commtrackid;
	private String createdate;
	private String msgbody;
	private Integer commreferenceid;
	private ContactAccount ctuser;
	public Integer getCommtrackid() {
		return commtrackid;
	}
	public void setCommtrackid(Integer commtrackid) {
		this.commtrackid = commtrackid;
	}
	public String getCreatedate() {
		return createdate;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public String getMsgbody() {
		return msgbody;
	}
	public void setMsgbody(String msgbody) {
		this.msgbody = msgbody;
	}
	public Integer getCommreferenceid() {
		return commreferenceid;
	}
	public void setCommreferenceid(Integer commreferenceid) {
		this.commreferenceid = commreferenceid;
	}
	public ContactAccount getCtuser() {
		return ctuser;
	}
	public void setCtuser(ContactAccount ctuser) {
		this.ctuser = ctuser;
	}
}
