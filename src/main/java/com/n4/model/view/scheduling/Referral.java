package com.n4.model.view.scheduling;

import java.util.List;

public class Referral {
	private Integer referralid;
	private ContactAccount referringphysician;
	private ContactAccount referredbycontact;
	private String referralstatus;
	private String receivedate;
	private Integer rxfileid; 
    private Integer orderfileid; 
    private String referralmethod;
	private List<Document> documents;
	private List<Encounter> encounters;
	public Integer getReferralid() {
		return referralid;
	}
	public void setReferralid(Integer referralid) {
		this.referralid = referralid;
	}
	public ContactAccount getReferringphysician() {
		return referringphysician;
	}
	public void setReferringphysician(ContactAccount referringphysician) {
		this.referringphysician = referringphysician;
	}
	public ContactAccount getReferredbycontact() {
		return referredbycontact;
	}
	public void setReferredbycontact(ContactAccount referredbycontact) {
		this.referredbycontact = referredbycontact;
	}
	public String getReferralstatus() {
		return referralstatus;
	}
	public void setReferralstatus(String referralstatus) {
		this.referralstatus = referralstatus;
	}
	public String getReceivedate() {
		return receivedate;
	}
	public void setReceivedate(String receivedate) {
		this.receivedate = receivedate;
	}
	public Integer getRxfileid() {
		return rxfileid;
	}
	public void setRxfileid(Integer rxfileid) {
		this.rxfileid = rxfileid;
	}
	public Integer getOrderfileid() {
		return orderfileid;
	}
	public void setOrderfileid(Integer orderfileid) {
		this.orderfileid = orderfileid;
	}
	public String getReferralmethod() {
		return referralmethod;
	}
	public void setReferralmethod(String referralmethod) {
		this.referralmethod = referralmethod;
	}
	public List<Document> getDocuments() {
		return documents;
	}
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	public List<Encounter> getEncounters() {
		return encounters;
	}
	public void setEncounters(List<Encounter> encounters) {
		this.encounters = encounters;
	}
}
