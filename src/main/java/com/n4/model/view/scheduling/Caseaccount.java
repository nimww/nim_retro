package com.n4.model.view.scheduling;

import java.util.List;

import com.n4.security.Encrypt;

public class Caseaccount {
	private String caseid;
	private String caseclaimnumber;
	private Integer payerid;
	private String clienttype;
	private String payername;
	private String payernotes;
	private String dateofinjury;
	private String dateofbirth;
	private String ssn;
	private String gender;
	private String injurydescription;
	private String notes;
	private ContactAccount patient;
	private ContactAccount employer;
	private ContactAccount assignedto;
	private ContactAccount adjuster;
	private ContactAccount nursecasemanager;
	private ContactAccount caseadministrator1;
	private ContactAccount caseadministrator2;
	private ContactAccount caseadministrator3;
	private ContactAccount caseadministrator4;
	private CasePrescreen prescreen;
	private List<Referral> referrals;
	
	public String getCaseid() {
		return caseid;
	}
	public void setCaseid(String caseid) {
		this.caseid = caseid;
	}
	public String getCaseclaimnumber() {
		return caseclaimnumber;
	}
	public void setCaseclaimnumber(String caseclaimnumber) {
		this.caseclaimnumber = caseclaimnumber;
	}
	public Integer getPayerid() {
		return payerid;
	}
	public void setPayerid(Integer payerid) {
		this.payerid = payerid;
	}
	public String getPayername() {
		return payername;
	}
	public void setPayername(String payername) {
		this.payername = payername;
	}
	public String getPayernotes() {
		return payernotes;
	}
	public void setPayernotes(String payernotes) {
		this.payernotes = payernotes;
	}
	public String getDateofinjury() {
		return dateofinjury;
	}
	public void setDateofinjury(String dateofinjury) {
		this.dateofinjury = dateofinjury;
	}
	public String getInjurydescription() {
		return injurydescription;
	}
	public void setInjurydescription(String injurydescription) {
		this.injurydescription = injurydescription;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public ContactAccount getPatient() {
		return patient;
	}
	public void setPatient(ContactAccount patient) {
		this.patient = patient;
	}
	public ContactAccount getEmployer() {
		return employer;
	}
	public void setEmployer(ContactAccount employer) {
		this.employer = employer;
	}
	public ContactAccount getAssignedto() {
		return assignedto;
	}
	public void setAssignedto(ContactAccount assignedto) {
		this.assignedto = assignedto;
	}
	public ContactAccount getAdjuster() {
		return adjuster;
	}
	public void setAdjuster(ContactAccount adjuster) {
		this.adjuster = adjuster;
	}
	public ContactAccount getNursecasemanager() {
		return nursecasemanager;
	}
	public void setNursecasemanager(ContactAccount nursecasemanager) {
		this.nursecasemanager = nursecasemanager;
	}
	public ContactAccount getCaseadministrator1() {
		return caseadministrator1;
	}
	public void setCaseadministrator1(ContactAccount caseadministrator1) {
		this.caseadministrator1 = caseadministrator1;
	}
	public ContactAccount getCaseadministrator2() {
		return caseadministrator2;
	}
	public void setCaseadministrator2(ContactAccount caseadministrator2) {
		this.caseadministrator2 = caseadministrator2;
	}
	public ContactAccount getCaseadministrator3() {
		return caseadministrator3;
	}
	public void setCaseadministrator3(ContactAccount caseadministrator3) {
		this.caseadministrator3 = caseadministrator3;
	}
	public ContactAccount getCaseadministrator4() {
		return caseadministrator4;
	}
	public void setCaseadministrator4(ContactAccount caseadministrator4) {
		this.caseadministrator4 = caseadministrator4;
	}
	public CasePrescreen getPrescreen() {
		return prescreen;
	}
	public void setPrescreen(CasePrescreen prescreen) {
		this.prescreen = prescreen;
	}
	public List<Referral> getReferrals() {
		return referrals;
	}
	public void setReferrals(List<Referral> referrals) {
		this.referrals = referrals;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public String getSsn() {
		return Encrypt.decrypt_DESEDE_String(ssn);
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getClienttype() {
		return clienttype;
	}
	public void setClienttype(String clienttype) {
		this.clienttype = clienttype;
	}
}
