package com.n4.model.view.scheduling;

public class CasePrescreen {
	private Boolean claustrophobic;
    private Boolean onmeds;
    private String onmedsdesc;
    private Boolean hasmetal;
    private String hasmetaldesc;
    private Boolean hadpreviousimage;
    private String hadpreviousimagedesc;
    private Boolean pregnant;
    private String pregnantdesc;
    private String height;
    private String weight;
	
    public Boolean getClaustrophobic() {
		return claustrophobic;
	}
	public void setClaustrophobic(Boolean claustrophobic) {
		this.claustrophobic = claustrophobic;
	}
	public Boolean getOnmeds() {
		return onmeds;
	}
	public void setOnmeds(Boolean onmeds) {
		this.onmeds = onmeds;
	}
	public String getOnmedsdesc() {
		return onmedsdesc;
	}
	public void setOnmedsdesc(String onmedsdesc) {
		this.onmedsdesc = onmedsdesc;
	}
	public Boolean getHasmetal() {
		return hasmetal;
	}
	public void setHasmetal(Boolean hasmetal) {
		this.hasmetal = hasmetal;
	}
	public String getHasmetaldesc() {
		return hasmetaldesc;
	}
	public void setHasmetaldesc(String hasmetaldesc) {
		this.hasmetaldesc = hasmetaldesc;
	}
	public Boolean getHadpreviousimage() {
		return hadpreviousimage;
	}
	public void setHadpreviousimage(Boolean hadpreviousimage) {
		this.hadpreviousimage = hadpreviousimage;
	}
	public String getHadpreviousimagedesc() {
		return hadpreviousimagedesc;
	}
	public void setHadpreviousimagedesc(String hadpreviousimagedesc) {
		this.hadpreviousimagedesc = hadpreviousimagedesc;
	}
	public Boolean getPregnant() {
		return pregnant;
	}
	public void setPregnant(Boolean pregnant) {
		this.pregnant = pregnant;
	}
	public String getPregnantdesc() {
		return pregnantdesc;
	}
	public void setPregnantdesc(String pregnantdesc) {
		this.pregnantdesc = pregnantdesc;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
}
