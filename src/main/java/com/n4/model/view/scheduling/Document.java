package com.n4.model.view.scheduling;

public class Document {
	private String documentid;
    private String filename; 
    private String uniquecreatedate;
    private String docname; 
    private ContactAccount uploader;
	public String getDocumentid() {
		return documentid;
	}
	public void setDocumentid(String documentid) {
		this.documentid = documentid;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getUniquecreatedate() {
		return uniquecreatedate;
	}
	public void setUniquecreatedate(String uniquecreatedate) {
		this.uniquecreatedate = uniquecreatedate;
	}
	public String getDocname() {
		return docname;
	}
	public void setDocname(String docname) {
		this.docname = docname;
	}
	public ContactAccount getUploader() {
		return uploader;
	}
	public void setUploader(ContactAccount uploader) {
		this.uploader = uploader;
	}
    
    
}
