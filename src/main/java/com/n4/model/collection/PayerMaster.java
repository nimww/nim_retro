package com.n4.model.collection;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.n4.model.mongo.collection.support.*;
import com.n4.utils.Configuration;
@Document(collection = "payerMaster")
public class PayerMaster {
	@Id
	private ObjectId id;
	private ObjectId parentId;
	private List<Log> log = new ArrayList<Log>();
	private String payerName;
	private int payerId;
	private int parentPayerId;
	private Contact contact;
	private Contact billingContact;
	private String notes;
	private ModalPricing mri;
	private ModalPricing ct;
	private ModalPricing billingMri;
	private ModalPricing billingCt;
	private List<FStable> contract;
	private List<FStable> billing;
	private Integer selectMriId;
	private String selectMriNotes;
	
	private String salesDivision;
	private String acquisitionDivision;
	
	private List<PayerMaster> branches = new ArrayList<PayerMaster>();
	private PayerConfiguration payerConfiguration;
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public ObjectId getParentId() {
		return parentId;
	}
	public void setParentId(ObjectId parentId) {
		this.parentId = parentId;
	}
	public List<Log> getLog() {
		return log;
	}
	public void setLog(List<Log> log) {
		this.log = log;
	}
	public int getPayerId() {
		return payerId;
	}
	public void setPayerId(int payerId) {
		this.payerId = payerId;
	}
	public int getParentPayerId() {
		return parentPayerId;
	}
	public void setParentPayerId(int parentPayerId) {
		this.parentPayerId = parentPayerId;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public Contact getBillingContact() {
		return billingContact;
	}
	public void setBillingContact(Contact billingContact) {
		this.billingContact = billingContact;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public ModalPricing getMri() {
		return mri;
	}
	public void setMri(ModalPricing mri) {
		this.mri = mri;
	}
	public ModalPricing getCt() {
		return ct;
	}
	public void setCt(ModalPricing ct) {
		this.ct = ct;
	}
	public ModalPricing getBillingMri() {
		return billingMri;
	}
	public void setBillingMri(ModalPricing billingMri) {
		this.billingMri = billingMri;
	}
	public ModalPricing getBillingCt() {
		return billingCt;
	}
	public void setBillingCt(ModalPricing billingCt) {
		this.billingCt = billingCt;
	}
	
	public Integer getSelectMriId() {
		return selectMriId;
	}
	public void setSelectMriId(Integer selectMriId) {
		this.selectMriId = selectMriId;
	}
	public String getSelectMriNotes() {
		return selectMriNotes;
	}
	public void setSelectMriNotes(String selectMriNotes) {
		this.selectMriNotes = selectMriNotes;
	}
	public String getSalesDivision() {
		return salesDivision;
	}
	public void setSalesDivision(String salesDivision) {
		this.salesDivision = salesDivision;
	}
	public String getAcquisitionDivision() {
		return acquisitionDivision;
	}
	public void setAcquisitionDivision(String acquisitionDivision) {
		this.acquisitionDivision = acquisitionDivision;
	}
	public String getPayerName() {
		return payerName;
	}
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	public List<PayerMaster> getBranches() {
		return branches;
	}
	public void setBranches(List<PayerMaster> branches) {
		this.branches = branches;
	}
	public PayerConfiguration getPayerConfiguration() {
		return payerConfiguration;
	}
	public void setPayerConfiguration(PayerConfiguration payerConfiguration) {
		this.payerConfiguration = payerConfiguration;
	}
	
}
