package com.n4.model.collection.support;

public class ModalityModel {
	private int mriModelId;
	private String manufacturer;
	private String model;
	private String desc;
	private Double tesla;
	private Boolean open;
	private int classLevel;
	private boolean accomodatesclaus;
	public int getMriModelId() {
		return mriModelId;
	}
	public void setMriModelId(int mriModelId) {
		this.mriModelId = mriModelId;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Double getTesla() {
		return tesla;
	}
	public void setTesla(Double tesla) {
		this.tesla = tesla;
	}
	public Boolean getOpen() {
		return open;
	}
	public void setOpen(Boolean open) {
		this.open = open;
	}
	public int getClassLevel() {
		return classLevel;
	}
	public void setClassLevel(int classLevel) {
		this.classLevel = classLevel;
	}
	public boolean isAccomodatesclaus() {
		return accomodatesclaus;
	}
	public void setAccomodatesclaus(boolean accomodatesclaus) {
		this.accomodatesclaus = accomodatesclaus;
	}
}
