package com.n4.model.collection.support;

public class ModalPricing {
	private double withOut;
	private double with;
	private double withAndWithOut;
	public ModalPricing() {
	}
	public ModalPricing(double withOut, double with, double withAndWithOut) {
		this.withOut = withOut;
		this.with = with;
		this.withAndWithOut = withAndWithOut;
	}
	public double getWithOut() {
		return withOut;
	}
	public void setWithOut(double withOut) {
		this.withOut = withOut;
	}
	public double getWith() {
		return with;
	}
	public void setWith(double with) {
		this.with = with;
	}
	public double getWithAndWithOut() {
		return withAndWithOut;
	}
	public void setWithAndWithOut(double withAndWithOut) {
		this.withAndWithOut = withAndWithOut;
	}
}
