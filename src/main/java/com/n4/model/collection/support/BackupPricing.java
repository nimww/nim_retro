package com.n4.model.collection.support;

public class BackupPricing {
	private Integer fsId;
	private Double fsPercentage;
	public BackupPricing() {
	}
	public BackupPricing(Integer fsId, Double fsPercentage) {
		super();
		this.fsId = fsId;
		this.fsPercentage = fsPercentage;
	}
	public Integer getFsId() {
		return fsId;
	}
	public void setFsId(Integer fsId) {
		this.fsId = fsId;
	}
	public Double getFsPercentage() {
		return fsPercentage;
	}
	public void setFsPercentage(Double fsPercentage) {
		this.fsPercentage = fsPercentage;
	}
}
