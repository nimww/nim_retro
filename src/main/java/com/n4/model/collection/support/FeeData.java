package com.n4.model.collection.support;

import java.util.Comparator;
import java.util.Date;

public class FeeData implements Comparator<FeeData> {
	Double fee;
	Date effDate;

	public FeeData() {
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Date getEffDate() {
		return effDate;
	}

	public void setEffDate(Date effDate) {
		this.effDate = effDate;
	}

	@Override
	public int compare(FeeData arg0, FeeData arg1) {
		long t1 = arg1.getEffDate().getTime();
		long t2 = arg0.getEffDate().getTime();
		if (t2 > t1)
			return 1;
		else if (t1 > t2)
			return -1;
		else
			return 0;
	}
}
