package com.n4.model.collection.support;

import java.util.Date;
import com.n4.model.enums.ModalityType;

public class Modality {
	private ModalityModel modalityModel;
	private Boolean isacr;
	private Date acrexpiration;
	private Integer isacrverified;
	private Date acrverifieddate;
	private String comments;
	public ModalityModel getModalityModel() {
		return modalityModel;
	}
	public void setModalityModel(ModalityModel modalityModel) {
		this.modalityModel = modalityModel;
	}
	public Boolean getIsacr() {
		return isacr;
	}
	public void setIsacr(Boolean isacr) {
		this.isacr = isacr;
	}
	public Date getAcrexpiration() {
		return acrexpiration;
	}
	public void setAcrexpiration(Date acrexpiration) {
		this.acrexpiration = acrexpiration;
	}
	public Integer getIsacrverified() {
		return isacrverified;
	}
	public void setIsacrverified(Integer isacrverified) {
		this.isacrverified = isacrverified;
	}
	public Date getAcrverifieddate() {
		return acrverifieddate;
	}
	public void setAcrverifieddate(Date acrverifieddate) {
		this.acrverifieddate = acrverifieddate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}

} 
