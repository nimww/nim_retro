package com.n4.model.collection;

import java.util.LinkedHashMap;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.n4.model.collection.support.FeeData;

@Document(collection = "fs")
public class FS {

	@Id
	private ObjectId id;
	// rc -> fsid -> CptModifier -> cpt = FeeData
	private LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, List<FeeData>>>>> fee = new LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, List<FeeData>>>>>();
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, List<FeeData>>>>> getFee() {
		return fee;
	}
	public void setFee(LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, List<FeeData>>>>> fee) {
		this.fee = fee;
	}
	
	
}
