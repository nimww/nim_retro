package com.n4.model.collection;

import java.util.LinkedHashMap;
import java.util.List;

import com.n4.model.collection.support.BackupPricing;
import com.n4.model.collection.support.ModalPricing;
import com.n4.model.collection.support.Modality;
import com.n4.model.enums.ContractStatus;
import com.n4.model.enums.ModalityType;
import com.n4.model.view.scheduling.ContactAccount;

public class Practice {
	private Integer practiceid;
	private ContactAccount contact;
	private ContractStatus contractStatus;
	private String notes;
	private String salesDivision;
	private List<BackupPricing> backupPricing;
	private LinkedHashMap<String, ModalPricing> modalPricing;
	private LinkedHashMap<ModalityType, Modality> modality;
	private Double[] geocode;
	
	public Integer getPracticeid() {
		return practiceid;
	}
	public void setPracticeid(Integer practiceid) {
		this.practiceid = practiceid;
	}
	public Double[] getGeocode() {
		return geocode;
	}
	public void setGeocode(Double[] geocode) {
		this.geocode = geocode;
	}
	public ContactAccount getContact() {
		return contact;
	}
	public void setContact(ContactAccount contact) {
		this.contact = contact;
	}
	public ContractStatus getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(ContractStatus contractStatus) {
		this.contractStatus = contractStatus;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getSalesDivision() {
		return salesDivision;
	}
	public void setSalesDivision(String salesDivision) {
		this.salesDivision = salesDivision;
	}
	public List<BackupPricing> getBackupPricing() {
		return backupPricing;
	}
	public void setBackupPricing(List<BackupPricing> backupPricing) {
		this.backupPricing = backupPricing;
	}
	public LinkedHashMap<String, ModalPricing> getModalPricing() {
		return modalPricing;
	}
	public void setModalPricing(LinkedHashMap<String, ModalPricing> modalPricing) {
		this.modalPricing = modalPricing;
	}
	public LinkedHashMap<ModalityType, Modality> getModality() {
		return modality;
	}
	public void setModality(LinkedHashMap<ModalityType, Modality> modality) {
		this.modality = modality;
	}
}
