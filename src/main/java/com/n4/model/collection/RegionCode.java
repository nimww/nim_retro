package com.n4.model.collection;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "regionCode")
public class RegionCode {
	@Id
	private ObjectId id;
	private int fsid;
	private String rc;
	private String zip;
	private Date effDate;
	public RegionCode() {
	}
	public RegionCode(ObjectId id, int fsid, String rc, String zip, Date effDate) {
		this.id = id;
		this.fsid = fsid;
		this.rc = rc;
		this.zip = zip;
		this.effDate = effDate;
	}
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public int getFsid() {
		return fsid;
	}
	public void setFsid(int fsid) {
		this.fsid = fsid;
	}
	public String getRc() {
		return rc;
	}
	public void setRc(String rc) {
		this.rc = rc;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public Date getEffDate() {
		return effDate;
	}
	public void setEffDate(Date effDate) {
		this.effDate = effDate;
	}
	
}
