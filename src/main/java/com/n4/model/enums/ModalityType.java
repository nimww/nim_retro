package com.n4.model.enums;

public enum ModalityType {
	None(0),
	MRI(1),
	CT(2),
	FL(4),
	MG(5),
	NM(6),
	US(7),
	XR(8),
	EMG(9),
	PETCT(10),
	DXA(3),
	SPECCT(11),
	BreastMRI(12),
	VirtColo(13),
	CaScoring(14);
	
	private int typeId;
	
	private ModalityType(int i){
		this.typeId = i;
	}

	public int getTypeId() {
		return typeId;
	}
}
