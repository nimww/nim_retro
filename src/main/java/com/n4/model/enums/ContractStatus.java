package com.n4.model.enums;

public enum ContractStatus {
	None(0),Target(3),OTA(1),Contracted(2), ContactedIPA(11),
	ContractedStar(9),ContractedSelect(8),OON(7),NotIntersted(4);
	
	private int status;
	
	private ContractStatus(int i){
		this.status = i;
	}

	public int getStatus() {
		return status;
	}
}