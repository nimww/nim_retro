package com.n4.model.api.geocode;

public class Geometry {
	Location location;

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
}
