package com.n4.model.api.geocode;

import java.util.List;

public class Geocode {
	private List<Results> results;

	public List<Results> getResults() {
		return results;
	}

	public void setResults(List<Results> results) {
		this.results = results;
	}

}