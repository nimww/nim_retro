package com.n4.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for Scheduling Application
 * @author hoppho
 *
 */
@Controller
@RequestMapping(value = "/test")
public class TestController {
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/prepay", method = RequestMethod.GET)
	public String home() {
		
		return "/test/PrePaymentApp";
	}
	
	@RequestMapping(value = "/template/getWorklist", method = RequestMethod.GET)
	public String getWorklist() {
		
		return "/test/WorklistTemplate";
	}
	
	@RequestMapping(value = "/template/getPaymentForm", method = RequestMethod.GET)
	public String getPaymentForm() {
		
		return "/test/PaymentFormTemplate";
	}
	
}
