package com.n4.controller;

import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.n4.model.diTest.CaseaccountBasic;
import com.n4.model.diTest.ditest;
import com.n4.model.view.accounting.PrePaymentWorklist;
import com.n4.model.view.scheduling.AdjDashboardList;
import com.n4.model.view.scheduling.AdjusterWorklist;
import com.n4.model.view.scheduling.Caseaccount;
import com.n4.model.view.scheduling.NIMWorklistModel;
import com.n4.utils.Utils;
import com.n4.utils.accounting.PrePaymentUtils;
import com.n4.utils.scheduling.AdjWorklistUtils;
import com.n4.utils.scheduling.CasepageUtils;
import com.n4.utils.scheduling.WorklistUtils;

/**
 * Controller for RESTful request
 * @author hoppho
 *
 */
@Controller
@RequestMapping(value = "/rest")
public class SchedulingRest {
	
	
	@RequestMapping(value = "/scheduling/getWorklist/{userid}", method = RequestMethod.POST)
	@ResponseBody
	public LinkedHashMap<String, List<NIMWorklistModel>> getWorkList(@RequestParam("key") String key, @PathVariable Integer userid, HttpServletResponse res) {
		WorklistUtils SchedulingUtils = new WorklistUtils();
		LinkedHashMap<String, List<NIMWorklistModel>> getWorklist = SchedulingUtils.getWorklist(userid, key);
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		if(getWorklist==null){
			res.setStatus(401);
		}
		return getWorklist;
	}
	
	@RequestMapping(value = "/scheduling/adjWorklist/{userid}", method = RequestMethod.POST)
	@ResponseBody
	public List<AdjusterWorklist> getWorkListAdj(@RequestParam("key") String key, @PathVariable Integer userid, HttpServletResponse res) {
		WorklistUtils SchedulingUtils = new WorklistUtils();
		List<AdjusterWorklist> getWorklist = SchedulingUtils.getAdjWorklist(userid);
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		if(getWorklist==null){
			res.setStatus(401);
		}
		return getWorklist;
	}
	
	@RequestMapping(value = "/scheduling/adjDashboard/{userid}", method = RequestMethod.POST)
	@ResponseBody
	public Object adjDashboard(@RequestParam("key") String key, @PathVariable Integer userid, HttpServletResponse res) {
		Utils utils = new Utils();
		
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		if (utils.verifyAPIkey(key)) {
			com.n4.utils.scheduling.AdjWorklistUtils AdjWorklistUtils = new AdjWorklistUtils();
			return AdjWorklistUtils.getDashboard(userid);
		} else{
			res.setStatus(401);
		}
		return null;
	}

	@RequestMapping(value = "/scheduling/getCasepage/{cId}", method = RequestMethod.POST)
	@ResponseBody
	public Caseaccount getCasepage(@RequestParam("key") String key, @PathVariable String cId, HttpServletResponse res) {
		CasepageUtils CasepageUtils = new CasepageUtils();
		Caseaccount ca = CasepageUtils.getCasepage(new Integer(cId), key);
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		if(ca==null){
			res.setStatus(401);
		}
		
		return ca;
	}
	@RequestMapping(value = "/scheduling/getCasepageLite/{cId}", method = RequestMethod.POST)
	@ResponseBody
	public Caseaccount getCasepageLite(@RequestParam("key") String key, @PathVariable String cId, HttpServletResponse res) {
		CasepageUtils CasepageUtils = new CasepageUtils();
		Caseaccount ca = CasepageUtils.getCasepageLite(new Integer(cId), key);
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		if(ca==null){
			res.setStatus(401);
		}
		
		return ca;
	}
	
//	@RequestMapping(value = "/accounting/getPrePaidWorkList.json", method = RequestMethod.GET)
//	@ResponseBody
//	public List<PrePaymentWorklist> getPrePaidWorkList() {
//		PrePaymentUtils PrePaymentUtils = new PrePaymentUtils();
//		return PrePaymentUtils.getWorklist();
//	}
	
//	@RequestMapping(value = "/test/getCasepage/{cId}", method = RequestMethod.GET)
//	@ResponseBody
//	public CaseaccountBasic getTestCasepage(@PathVariable String cId, HttpServletResponse res) {
//		CaseaccountBasic ca = ditest.getCasepage(123);
//		res.addHeader("Access-Control-Allow-Origin", "*");
//		res.addHeader("Access-Control-Allow-Methods", "GET");
//		return ca;
//	}
}
