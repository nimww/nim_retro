package com.n4.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.n4.model.view.netdev.FSview;
import com.n4.model.view.netdev.PracticeAdjView;
import com.n4.model.view.netdev.PracticeView;
import com.n4.utils.Utils;
import com.n4.utils.netdev.FSUtils;
import com.n4.utils.netdev.NetdevUtils;

/**
 * Controller for RESTful request
 * 
 * @author hoppho
 *
 */
@Controller
@RequestMapping(value = "/rest/netdev")
public class NetDevRest {
	/**
	 * Get FS
	 * @param key api key
	 * @param zip zip of patient / IC
	 * @param fsid fs to lookup. optional. defaults 1 if none if provided.
	 * @param mod modifier to be considered. optional, defaults to "Global"
	 * @param cpt code to look up
	 * @param effDate yyyymmdd
	 * @return
	 */
	@RequestMapping(value = "/getFs.json", method = RequestMethod.POST)
	@ResponseBody
	public FSview getFS(@RequestParam("key") String key, @RequestParam("zip") String zip, @RequestParam(value = "fsid", required = false) Integer fsid, @RequestParam(value = "effDate", required = false) String effDate, 
			@RequestParam(value = "mod", required = false) String mod, @RequestParam(value = "qty", required = false) Integer qty, @RequestParam("cpt") String cpt, HttpServletResponse res) {
		Utils utils = new Utils();
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		FSview fsView = new FSview();
		if (utils.verifyAPIkey(key)) {
			Double fee = null;
			FSUtils FSUtils = new FSUtils();
			if (fsid == null) {
				fsid = 1;
			}
			if (mod == null) {
				mod = "Global";
			}
			if (qty == null) {
				qty = 1;
			}
			Double tempFee = FSUtils.getFee(FSUtils.getRc(zip, fsid), fsid, mod, cpt, effDate);
			fee = (tempFee==null ? 0d : tempFee) * qty;
			fsView.setFee(fee);
			System.out.println("tempFee: "+tempFee);
			if(tempFee==null || tempFee==0d){
				fsView.setStatus(FSUtils.checkError(zip,fsid, mod, cpt, effDate));
			} else{
				fsView.setStatus("OK");
			}
			
		} else {
			res.setStatus(401);
			fsView.setFee(0d);
			fsView.setStatus("Unauthorized");
		}
		return fsView;
	}
	
	@RequestMapping(value = "/practices.json", method = RequestMethod.POST)
	@ResponseBody
	public List<PracticeView> getPractices(@RequestParam("key") String key, @RequestParam("miles") Integer miles, @RequestParam(value = "eid") Integer eId, HttpServletResponse res) {
		Utils utils = new Utils();
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		List<PracticeView> pvList = new ArrayList<PracticeView>();
		if (utils.verifyAPIkey(key)) {
			NetdevUtils NetdevUtils = new NetdevUtils();
			pvList = NetdevUtils.getPractice(miles, eId);
			
		} else {
			res.setStatus(401);
			return null;
		}
		return pvList;
	}
	
	@RequestMapping(value = "/adjPractices.json", method = RequestMethod.POST)
	@ResponseBody
	public PracticeAdjView adjPractices(@RequestParam("key") String key, @RequestParam("address") String address, @RequestParam(value = "zip") String zip, HttpServletResponse res) {
		Utils utils = new Utils();
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		PracticeAdjView practiceAdjView = new PracticeAdjView();
		if (utils.verifyAPIkey(key)) {
			NetdevUtils NetdevUtils = new NetdevUtils();
			practiceAdjView = NetdevUtils. getPracticeAdjView(address, zip);
		} else {
			res.setStatus(401);
			return null;
		}
		return practiceAdjView;
	}

}
