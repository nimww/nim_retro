package com.n4.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.n4.model.view.report.CostsavingsReport;
import com.n4.model.view.report.Utilization;
import com.n4.model.view.report.UtilzationReport;
import com.n4.utils.Database;

/**
 * Controller for Scheduling Application
 * @author hoppho
 *
 */
@Controller
@RequestMapping(value = "/rest/report")
public class ReportRest {
	
	
	@RequestMapping(value = "/allpayers", method = RequestMethod.GET)
	@ResponseBody
	public String allPayers(HttpServletResponse res) {
		Database db = new Database();
		res.setContentType("application/json");
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "GET");
		ResultSet rs = db.getRs("select * from all_payers");
		
		try {
			while(rs.next()){
				return rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return "{}";
	}
	
	@RequestMapping(value = "/getUtilization", method = RequestMethod.POST)
	@ResponseBody
	//public String getUtilization(HttpServletResponse res,  @RequestParam("dateOne") String dateOne,  @RequestParam("dateTwo") String dateTwo,  @RequestParam("viewAdmin") String viewAdmin,  @RequestParam("payerSelection") String myLocations) {
	public UtilzationReport getUtilization(HttpServletResponse res,  @ModelAttribute Utilization utilization) {
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		
		String[] payers = utilization.getPayerSelection().split(",");
		List<String> PA = new ArrayList<String>();
		List<String> CH = new ArrayList<String>();
		
		if(payers.length > 0){
			for(String payer : payers){
				System.out.println(payer);
				String[] p = payer.split("-");
				System.out.println(p[1]);
				if(p[0].equals("PA")){
					PA.add(p[1]);
				} else if (p[0].equals("CH")){
					CH.add(p[1]);
				}
			}
		}
		
		String pa = "";
		int c = 0;
		for(String s : PA){
			pa+=(c==0? s:","+s);
			c++;
		}
		String ch = "";
		c = 0;
		for(String s : CH){
			ch+=(c==0? s:","+s);
			c++;
		}
		Database db = new Database();
		String query = "select * from sales_utilization('"+utilization.getDateOne()+"', Array["+ch+"]::int[], Array["+pa+"]::int[], "+utilization.getViewAdmin()+")";
		System.out.println(query);
		
		ResultSet rs = db.getRs(query);
//		List<UtilzationReport> utilzationReport = new ArrayList<UtilzationReport>();
		try {
			while(rs.next()){
				return new Gson().fromJson(rs.getString(1), UtilzationReport.class) ;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/getCostsavings", method = RequestMethod.POST)
	@ResponseBody
	//public String getUtilization(HttpServletResponse res,  @RequestParam("dateOne") String dateOne,  @RequestParam("dateTwo") String dateTwo,  @RequestParam("viewAdmin") String viewAdmin,  @RequestParam("payerSelection") String myLocations) {
	public List<CostsavingsReport> getCostsavings(HttpServletResponse res,  @ModelAttribute Utilization utilization) {
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		
		String[] payers = utilization.getPayerSelection().split(",");
		List<String> PA = new ArrayList<String>();
		List<String> CH = new ArrayList<String>();
		
		if(payers.length > 0){
			for(String payer : payers){
				System.out.println(payer);
				String[] p = payer.split("-");
				System.out.println(p[1]);
				if(p[0].equals("PA")){
					PA.add(p[1]);
				} else if (p[0].equals("CH")){
					CH.add(p[1]);
				}
			}
		}
		
		String pa = "";
		int c = 0;
		for(String s : PA){
			pa+=(c==0? s:","+s);
			c++;
		}
		String ch = "";
		c = 0;
		for(String s : CH){
			ch+=(c==0? s:","+s);
			c++;
		}
		Database db = new Database();
		String query = "select * from cost_savings('"+utilization.getDateOne()+"', Array["+ch+"]::int[], Array["+pa+"]::int[])";
		System.out.println(query);
		
		ResultSet rs = db.getRs(query);
		List<CostsavingsReport> CostsavingsReport = new ArrayList<CostsavingsReport>();
		try {
			while(rs.next()){
				return new Gson().fromJson(rs.getString(1), CostsavingsReport.getClass()) ;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
