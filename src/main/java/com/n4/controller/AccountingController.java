package com.n4.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for Scheduling Application
 * @author hoppho
 *
 */
@Controller
@RequestMapping(value = "/accounting")
public class AccountingController {
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/prepay", method = RequestMethod.GET)
	public String home() {
		
		return "/accounting/PrePaymentApp";
	}
	
	@RequestMapping(value = "/template/getWorklist", method = RequestMethod.GET)
	public String getWorklist() {
		
		return "/accounting/WorklistTemplate";
	}
	
	@RequestMapping(value = "/template/getPaymentForm", method = RequestMethod.GET)
	public String getPaymentForm() {
		
		return "/accounting/PaymentFormTemplate";
	}
	
}
