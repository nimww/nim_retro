package com.n4.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.n4.model.view.admin.UserView;
import com.n4.model.view.netdev.FSview;
import com.n4.utils.Utils;
import com.n4.utils.admin.AdminUtils;
import com.n4.utils.netdev.FSUtils;

/**
 * Controller for RESTful request
 * 
 * @author hoppho
 *
 */
@Controller
@RequestMapping(value = "/rest/admin")
public class AdminRest {
	/**
	 * Checks if user creds are valid
	 */
	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	@ResponseBody
	public UserView checkAuth(@RequestParam("key") String key, @RequestParam("name") String name, @RequestParam("password") String password, HttpServletResponse res) {
		Utils utils = new Utils();
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		if (utils.verifyAPIkey(key)) {
			AdminUtils AdminUtils = new AdminUtils();
			return AdminUtils.checkAuth(name, password);
			
		} else {
			res.setStatus(403);
		}
		return null;
	}
	
	@RequestMapping(value = "/ua", method = RequestMethod.POST)
	@ResponseBody
	public UserView getUA(@RequestParam("key") String key, @RequestParam("id") Integer id, HttpServletResponse res) {
		Utils utils = new Utils();
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		if (utils.verifyAPIkey(key)) {
			AdminUtils AdminUtils = new AdminUtils();
			return AdminUtils.getUA(id);
			
		} else {
			res.setStatus(403);
		}
		return null;
	}
	
	@RequestMapping(value = "/allUa.json", method = RequestMethod.POST)
	@ResponseBody
	public String allua(@RequestParam("key") String key, HttpServletResponse res) {
		Utils utils = new Utils();
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		res.setContentType("application/json");
		if (utils.verifyAPIkey(key)) {
			AdminUtils AdminUtils = new AdminUtils();
			return AdminUtils.getAllUa();
			
		} else {
			res.setStatus(403);
		}
		return null;
	}

}
