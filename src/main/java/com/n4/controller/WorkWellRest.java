package com.n4.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;

import com.google.gson.Gson;
import com.n4.model.view.netdev.PracticeView;
import com.n4.model.view.pt.PTBilling;
import com.n4.model.view.report.Utilization;
import com.n4.utils.Utils;
import com.n4.utils.netdev.NetdevUtils;
import com.n4.utils.workwell.WorkWellUtils;

@Controller
@RequestMapping(value = "/rest/workwell")
public class WorkWellRest {
	
	
	@RequestMapping(value = "/getFs", method = RequestMethod.POST)
	@ResponseBody
	public String getWorkList(@PathVariable Integer userid, HttpServletResponse res) {

		
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		return null;
	}
	
	@RequestMapping(value = "/submitPtBilling", method = RequestMethod.POST)
	@ResponseBody
	public String submitPtBilling(@RequestParam("key") String key, HttpServletResponse res, @ModelAttribute PTBilling pTBilling) {
		Utils utils = new Utils();
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods", "POST");
		String resMsg = null;
		if (utils.verifyAPIkey(key)) {
			WorkWellUtils workWellUtils = new WorkWellUtils();
			resMsg = workWellUtils.submitPtBilling(pTBilling);
		} else {
			res.setStatus(401);
			return null;
		}
		
		return resMsg;
	}
	
	@RequestMapping(value = "/submitPtBillingTest", method = RequestMethod.POST)
	@ResponseBody
	public String submitPtBillingTest(@RequestParam("key") String key, HttpServletResponse res, @ModelAttribute PTBilling pTBilling) {
		
		return new Gson().toJson(pTBilling);
	}
	
	@RequestMapping(value = "/viewPtBilling", method = RequestMethod.GET)
	public String submitPtBillingView(Model model) {
		
		model.addAttribute("pt", new WorkWellUtils().getPtBilling());
		return "workwell/ptBillingTest";
	}
	
}
