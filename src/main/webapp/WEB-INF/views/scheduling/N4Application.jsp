<%@ include file="../jstl_tags.jsp"%>
<c:set var="acp" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html data-ng-app="">
<head>
<title>NextImage Dashboard</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<style type="text/css">
.max-cell {
	max-width: 200px !important;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}

#case-list .navbar-nav>li>a, #case-list .navbar-brand, #case-list  .navbar
	{
	padding-top: 0px;
	padding-bottom: 0px;
	min-height: 0px;
}

#case-list .navbar-brand {
	height: 0px;
}

li.dropdown, li.dropdown>a {
	padding: 0px 2px;
	color: white;
}

li.dropdown.btn-success>a:hover {
	background-color: rgb(76, 174, 76) !important;
}

.nav .open>a, .nav .open.btn-success>a:hover, .nav .open>a:focus {
	background-color: rgb(76, 174, 76) !important;
}

li.dropdown.btn-primary>a:hover {
	background-color: #3071a9 !important;
}

.nav .open>a, .nav .open.btn-primary>a:hover, .nav .open>a:focus {
	background-color: #3071a9 ! important;
}

.nav .btn {
	border: 1px solid;
}

.width-90 {
	width: 90%;
}

.panel-heading {
	padding: 4px 10px;
}
</style>
</head>
<body data-ng-controller="nimController">
	<div class="container-fluid">
		<div class="row">
			<h3>NextImage</h3>
		</div>

		<div class="row">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<span class="navbar-brand">Worklist</span>
					</div>
					<ul class="nav navbar-nav">
						<li data-ng-class="{'active':navWorklist.isNeedsScheduling}" data-ng-click="navWorklistFn('isNeedsScheduling')"><a href="#">Needs Scheduling ({{needsSchedulingList.length}})</a></li>
						<li data-ng-class="{'active':navWorklist.isAppointmentFollowUp}" data-ng-click="navWorklistFn('isAppointmentFollowUp')"><a href="#">Appointment Follow Up ({{appointmentFollowUpList.length}})</a></li>
						<li data-ng-class="{'active':navWorklist.isAwaitingRx}" data-ng-click="navWorklistFn('isAwaitingRx')"><a href="#">Awaiting Rx ({{awaitingRxList.length}})</a></li>
						<li data-ng-class="{'active':navWorklist.isNextActionAlert}" data-ng-click="navWorklistFn('isNextActionAlert')"><a href="#">Next Action Alert ({{nextActionAlertList.length}})</a></li>
					</ul>
				</div>
			</nav>
		</div>

		<div class="row" id="case-list" data-ng-show="caseList.length>0">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<ul class="nav navbar-nav">
						<li data-ng-repeat="case in caseList.slice().reverse()"><a href="#" data-ng-click="openCase(case.caseId,case.patientName)">{{case.patientName}}</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
	
	<div class="container-fluid" id="casePageContainer" data-ng-show="isShowCasePageContainer">
	<div class="col-sm-4">
		<div class="row">
			<h4>
				{{casepage.patient.firstName}} {{casepage.patient.lastName}}- <small>Work Comp Strategic Solutions</small>
			</h4>
			<hr style="margin-top: 12px; margin-bottom: 0px; border-top: 1px solid #DDD;" />
		</div>
		<div class="row">
			<h5>Case Info:</h5>
		</div>
		<div class="row">
			<div class="collapse navbar-collapse bs-example-js-navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="btn btn-danger btn-xs">STAT</li>
					<li class="btn btn-warning btn-xs">Claustrophobic</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<address>
				<h5>Address:</h5>
				{{casepage.patient.address.street}}<br> {{casepage.patient.address.city}}, {{casepage.patient.address.state}} {{casepage.patient.address.zip}}<br> P: {{casepage.patient.phone}} <br> C: {{casepage.patient.cellPhone}}
			</address>
		</div>
		<div class="row">
			<h5>Patient Info:</h5>
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-3">
						<span class="pull-right">SSN:</span>
					</div>
					<div class="col-sm-9">{{casepage.patient.ssn}}</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<span class="pull-right">DOB:</span>
					</div>
					<div class="col-sm-9">{{casepage.patient.dateOfBirth}}</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<span class="pull-right">DOI:</span>
					</div>
					<div class="col-sm-9">{{casepage.dateOfInjury}}</div>
				</div>

			</div>
			<div class="col-sm-6"></div>
		</div>
		<div class="row">
			<h5>Case Notes:</h5>
			<ul class="list-group">
				<li class="list-group-item width-90">{{casepage.notes}}</li>
			</ul>
		</div>
		<div class="row">
			<h5>Payer Notes:</h5>
			<ul class="list-group">
				<li class="list-group-item width-90">Prescreened IW & is avail in the pm any time</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-8">
		<div class="row">
			<div role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#rf1" aria-controls="home" role="tab" data-toggle="tab">Referral 1</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="rf1">
						<div class="row">
							<h5>
								Receive Date: <small>12/12/1912</small>
							</h5>
						</div>
						<div class="row">
							<div class="collapse navbar-collapse bs-example-js-navbar-collapse">
								<ul class="nav navbar-nav">
									<li class="btn btn-primary btn-xs">Approved</li>
									<li class="btn btn-success btn-xs dropdown"><a id="auth" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Auth <span class="caret"></span>
									</a>
										<ul class="dropdown-menu" role="menu" aria-labelledby="auth">
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Upload</a></li>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">View</a></li>
										</ul></li>
									<li class="btn btn-success  dropdown"><a id="rx" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Rx <span class="caret"></span>
									</a>
										<ul class="dropdown-menu" role="menu" aria-labelledby="rx">
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Upload</a></li>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">View</a></li>
										</ul></li>
								</ul>
							</div>
						</div>
						<div class="row">
							<h5>SP12345ASD</h5>
							<hr style="margin: 0px 0px 5px 0px;">
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="collapse navbar-collapse bs-example-js-navbar-collapse">
									<ul class="nav navbar-nav">
										<li class="btn btn-xs">NetDev</li>
										<li class="btn btn-warning btn-xs">NetDev Waiting</li>
										<li class="btn btn-success btn-xs">RxRw</li>
										<li class="btn btn-primary btn-xs dropdown"><a id="auth" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Report <span class="caret"></span>
										</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="auth">
												<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Upload</a></li>
												<li role="presentation"><a role="menuitem" tabindex="-1" href="#">View</a></li>
											</ul></li>
										<li class="btn btn-primary btn-xs dropdown"><a id="auth" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> All Docs <span class="caret"></span>
										</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="auth">
												<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Upload</a></li>
												<li role="presentation"><a role="menuitem" tabindex="-1" href="#">View</a></li>
											</ul></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<h5>Services</h5>
								<ul>
									<li>Cras justo odio</li>
									<li>Dapibus ac facilisis in</li>
									<li>Morbi leo risus</li>
									<li>Porta ac consectetur ac</li>
									<li>Vestibulum at eros</li>
								</ul>
							</div>
							<div class="col-sm-6">
								<h5>Confirmation</h5>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Order</th>
											<th>Scanpass</th>
											<th>Report</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Adjuster</td>
											<td><button class="btn btn-xs">Send</button></td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
										</tr>
										<tr>
											<td>MD</td>
											<td><button class="btn btn-xs">Send</button></td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
										</tr>
										<tr>
											<td>Image Center</td>
											<td>&nbsp;</td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
											<td>&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-sm-6">
								<h5>Appointment</h5>
								<p>No Appointments</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<h5>CommTracks</h5>
								<div class="panel panel-info">
									<div class="panel-heading">
										<h5 class="panel-title">CT title</h5>
									</div>
									<div class="panel-body">CT content</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<script>
		function nimController($scope, $http, $log) {

			$http.get('${acp}/rest/scheduling/getWorklist').success(function(data) {
				$scope.needsSchedulingList = data['needsToBeScheduled'];
				$log.log($scope.needsSchedulingList);
				$scope.appointmentFollowUpList = data['appointmentFollowUp'];
				$scope.awaitingRxList = data['awaitingRx'];
				$scope.nextActionAlertList = data['nextActionAlert'];
			});
			
			$scope.isShowWorklistContainer = true;
			$scope.isShowCasePageContainer = false;

			$scope.caseList = new Array();
			$scope.casepage;
			$scope.navWorklist = {
				isNeedsScheduling : true,
				isAppointmentFollowUp : false,
				isAwaitingRx : false,
				isNextActionAlert : false
			};

			$scope.navWorklistFn = function(wl) {
				$scope.rmNavActive();
				$scope.navWorklist[wl] = true;
				$scope.isShowWorklistContainer = true;
				$scope.isShowCasePageContainer = false;
			};
			
			$scope.rmNavActive = function() {
				$scope.navWorklist = {
					isNeedsScheduling : false,
					isAppointmentFollowUp : false,
					isAwaitingRx : false,
					isNextActionAlert : false
				};
			};
			$scope.openCase = function(caseId, patientName) {
				$http.get('${acp}/rest/scheduling/getCasepage/'+caseId).success(function(data) {
					$scope.casepage = data;
				});
				var viewedCase = new Object();
				viewedCase.caseId = caseId;
				viewedCase.patientName = patientName;
				var isDupe = -1;
				
				for(var x = 0; x<$scope.caseList.length; x++){
					if($scope.caseList[x].caseId == caseId){
						isDupe = x;
						break;
					}
				}
				
				var maxCaseList = 10;
				if (isDupe > -1) {
					$scope.caseList.splice(isDupe, 1);
					$scope.caseList.push(viewedCase);
					if ($scope.caseList.length > maxCaseList) {
						$scope.caseList.reverse().pop();
						$scope.caseList.reverse();
					}
				} else {
					$scope.caseList.push(viewedCase);
					if ($scope.caseList.length > maxCaseList) {
						$scope.caseList.reverse().pop();
						$scope.caseList.reverse();
					}
				}
				$scope.rmNavActive();
				$scope.isShowWorklistContainer = false;
				$scope.isShowCasePageContainer = true;
			}
		};
	</script>

</body>
</html>