<div class="container-fluid" id="casePageContainer" data-ng-show="isShowCasePageContainer">
	<div class="col-sm-4">
		<div class="row">
			<h4>
				{{casepage.patient.firstName}} - <small>Work Comp Strategic Solutions</small>
			</h4>
			<hr style="margin-top: 12px; margin-bottom: 0px; border-top: 1px solid #DDD;" />
		</div>
		<div class="row">
			<h5>Case Info:</h5>
		</div>
		<div class="row">
			<div class="collapse navbar-collapse bs-example-js-navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="btn btn-danger btn-xs">STAT</li>
					<li class="btn btn-warning btn-xs">Claustrophobic</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<address>
				<h5>Address:</h5>
				795 Folsom Ave, Suite 600<br> San Francisco, CA 94107<br> P: (123) 456-7890 <br> C: (123) 456-7890
			</address>
		</div>
		<div class="row">
			<h5>Patient Info:</h5>
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-3">
						<span class="pull-right">SSN:</span>
					</div>
					<div class="col-sm-9">555-33-1111</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<span class="pull-right">DOB:</span>
					</div>
					<div class="col-sm-9">12/12/1990</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<span class="pull-right">DOI:</span>
					</div>
					<div class="col-sm-9">12/12/1990</div>
				</div>

			</div>
			<div class="col-sm-6"></div>
		</div>
		<div class="row">
			<h5>Case Notes:</h5>
			<ul class="list-group">
				<li class="list-group-item width-90">Prescreened IW & is avail in the pm any time</li>
			</ul>
		</div>
		<div class="row">
			<h5>Payer Notes:</h5>
			<ul class="list-group">
				<li class="list-group-item width-90">Prescreened IW & is avail in the pm any time</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-8">
		<div class="row">
			<div role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#rf1" aria-controls="home" role="tab" data-toggle="tab">Referral 1</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="rf1">
						<div class="row">
							<h5>
								Receive Date: <small>12/12/1912</small>
							</h5>
						</div>
						<div class="row">
							<div class="collapse navbar-collapse bs-example-js-navbar-collapse">
								<ul class="nav navbar-nav">
									<li class="btn btn-primary btn-xs">Approved</li>
									<li class="btn btn-success btn-xs dropdown"><a id="auth" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Auth <span class="caret"></span>
									</a>
										<ul class="dropdown-menu" role="menu" aria-labelledby="auth">
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Upload</a></li>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">View</a></li>
										</ul></li>
									<li class="btn btn-success  dropdown"><a id="rx" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Rx <span class="caret"></span>
									</a>
										<ul class="dropdown-menu" role="menu" aria-labelledby="rx">
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Upload</a></li>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">View</a></li>
										</ul></li>
								</ul>
							</div>
						</div>
						<div class="row">
							<h5>SP12345ASD</h5>
							<hr style="margin: 0px 0px 5px 0px;">
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="collapse navbar-collapse bs-example-js-navbar-collapse">
									<ul class="nav navbar-nav">
										<li class="btn btn-xs">NetDev</li>
										<li class="btn btn-warning btn-xs">NetDev Waiting</li>
										<li class="btn btn-success btn-xs">RxRw</li>
										<li class="btn btn-primary btn-xs dropdown"><a id="auth" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Report <span class="caret"></span>
										</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="auth">
												<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Upload</a></li>
												<li role="presentation"><a role="menuitem" tabindex="-1" href="#">View</a></li>
											</ul></li>
										<li class="btn btn-primary btn-xs dropdown"><a id="auth" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> All Docs <span class="caret"></span>
										</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="auth">
												<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Upload</a></li>
												<li role="presentation"><a role="menuitem" tabindex="-1" href="#">View</a></li>
											</ul></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<h5>Services</h5>
								<ul>
									<li>Cras justo odio</li>
									<li>Dapibus ac facilisis in</li>
									<li>Morbi leo risus</li>
									<li>Porta ac consectetur ac</li>
									<li>Vestibulum at eros</li>
								</ul>
							</div>
							<div class="col-sm-6">
								<h5>Confirmation</h5>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Order</th>
											<th>Scanpass</th>
											<th>Report</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Adjuster</td>
											<td><button class="btn btn-xs">Send</button></td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
										</tr>
										<tr>
											<td>MD</td>
											<td><button class="btn btn-xs">Send</button></td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
										</tr>
										<tr>
											<td>Image Center</td>
											<td>&nbsp;</td>
											<td><button class="btn btn-xs btn-warning">Send</button></td>
											<td>&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-sm-6">
								<h5>Appointment</h5>
								<p>No Appointments</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<h5>CommTracks</h5>
								<div class="panel panel-info">
									<div class="panel-heading">
										<h5 class="panel-title">CT title</h5>
									</div>
									<div class="panel-body">CT content</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>