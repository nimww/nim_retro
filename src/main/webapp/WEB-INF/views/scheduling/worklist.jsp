<div class="container-fluid" id="worklistContainer" data-ng-show="isShowWorklistContainer">

	<div class="col-sm-8">
		<div class="col-sm-12" data-ng-show="navWorklist.isNeedsScheduling">
			<h4>Needs Scheduling ({{needsSchedulingList.length}})</h4>
			<hr>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Payer</th>
						<th>Scheduler</th>
						<th>Patient</th>
						<th>State</th>
						<th>Scanpass</th>
						<th>Status</th>
						<th>Hours</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="needsScheduling in needsSchedulingList">
						<td>{{ needsScheduling.payer }}</td>
						<td>{{ needsScheduling.assignedTo }}</td>
						<td>{{ needsScheduling.patient }}</td>
						<td>{{ needsScheduling.state }}</td>
						<td>{{ needsScheduling.scanpass }}</td>
						<td>{{ needsScheduling.status }}</td>
						<td>{{ needsScheduling.hours }}</td>
						<td>
							<button class="btn" data-ng-click="openCase(needsScheduling.patient)">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Action
							</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-sm-12" data-ng-show="navWorklist.isAppointmentFollowUp">
			<h4>Post Appointment ({{appointmentFollowUpList.length}})</h4>
			<hr>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Scheduler</th>
						<th>Patient</th>
						<th>State</th>
						<th>Scanpass</th>
						<th>Status</th>
						<th>Hours</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="appointmentFollowUp in appointmentFollowUpList">
						<td>{{ appointmentFollowUp.assignedTo }}</td>
						<td>{{ appointmentFollowUp.patient }}</td>
						<td>{{ appointmentFollowUp.state }}</td>
						<td>{{ appointmentFollowUp.scanpass }}</td>
						<td>{{ appointmentFollowUp.status }}</td>
						<td>{{ appointmentFollowUp.hours }}</td>
						<td>
							<button class="btn" data-ng-click="openCase(appointmentFollowUp.patient)">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Action
							</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-sm-12" data-ng-show="navWorklist.isAwaitingRx">
			<h4>Awaiting Rx ({{awaitingRxList.length}})</h4>
			<hr>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Patient</th>
						<th>State</th>
						<th>Scanpass</th>
						<th>Status</th>
						<th>Hours</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="awaitingRx in awaitingRxList">
						<td>{{ awaitingRx.patient }}</td>
						<td>{{ awaitingRx.state }}</td>
						<td>{{ awaitingRx.scanpass }}</td>
						<td>{{ awaitingRx.status }}</td>
						<td>{{ awaitingRx.hours }}</td>
						<td>
							<button class="btn" data-ng-click="openCase(awaitingRx.patient)">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Action
							</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-sm-12" data-ng-show="navWorklist.isNextActionAlert">
			<h4>Next Action Alert ({{nextActionAlertList.length}})</h4>
			<hr>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Patient</th>
						<th>Scanpass</th>
						<th>Action Due</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="nextActionAlert in nextActionAlertList">
						<td>{{ nextActionAlert.patient }}</td>
						<td>{{ nextActionAlert.scanpass }}</td>
						<td>{{ nextActionAlert.nad }}</td>
						<td>{{ nextActionAlert.hours }}</td>
						<td>
							<button class="btn" data-ng-click="openCase(nextActionAlert.patient)">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Action
							</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>
</div>