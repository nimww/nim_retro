<div class="container">
	<h3>PrePayment Application</h3>
	<div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Patient</th>
					<th>Claim</th>
					<th>Payer</th>
					<th>DOS</th>
					<th>Image Center</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<tr data-ng-repeat="p in pp.prePayList">
					<td>
						<!-- <a href="#/add/{{ p.caseid }}" class="btn btn-primary" > -->
						<a data-ui-sref="addPrePay({caseid:p.caseid})" class="btn btn-primary" >
							<span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Edit
						</a>
					</td>
					<td>{{ p.patient }}</td>
					<td>{{ p.claim }}</td>
					<td class="max-cell">{{ p.payer }}</td>
					<td>{{ p.dos }}</td>
					<td>{{ p.imageCenter }}</td>
					<td>{{ p.status }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>