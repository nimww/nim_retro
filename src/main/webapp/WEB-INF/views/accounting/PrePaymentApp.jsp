<%@ include file="../jstl_tags.jsp"%>
<c:set var="acp" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html data-ng-app="prePayApp">
<head>
<title>Payer Exception Application</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular-route.min.js"></script>
<script src="${acp }/ui-router.js"></script>
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<style type="text/css">
.max-cell {
	max-width: 200px !important;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
</style>
</head>

<body>
	<div data-ui-view></div>
	<!-- <div data-ng-view></div> -->
	<script>
		(function() {
		var app = angular.module('prePayApp', ['ui.router'])
			.config([ '$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {
				$urlRouterProvider.otherwise('/');
				
				$stateProvider.state('index',{
					url : '/',
					templateUrl : '${acp}/accounting/template/getWorklist',
					controller : 'prePayListCtrl',
					controllerAs : 'pp',
					resolve: {
						getPrepayList : ['$http', function($http){
							return $http.get('${acp}/rest/accounting/getPrePaidWorkList.json').then(function(repsonse){
								return repsonse.data;
							})
						}]
					}
				}).state('addPrePay', {
					url : '/add/:caseid',
					templateUrl : '${acp}/accounting/template/getPaymentForm',
					controller : 'addPrePayCtrl',
					controllerAs : 'a'
				});

			} ]);
			
			app.controller('prePayListCtrl', ['getPrepayList', function(getPrepayList) {
				var prePay = this;
				
				prePay.prePayList = getPrepayList;
				
			} ])
			.controller('addPrePayCtrl', ['$stateParams', '$state', function($stateParams,$state) {
				var addPrePay = this;
				addPrePay.caseid = $stateParams.caseid;
				//alert(addPrePay.caseid);
				//$state.go('index');
			} ]);
			
		})();

		// Working
		/* (function() {
			var app = angular.module('prePayApp', [ 'ngRoute' ]).config([ '$routeProvider', function($routeProvider) {
				$routeProvider.when('/', {
					controller : 'prePayListCtrl',
					controllerAs : 'pp',
					templateUrl : '${acp}/accounting/template/getWorklist'
				}).when('/add/:caseid', {
					controller : 'addPrePayCtrl',
					controllerAs : 'a',
					templateUrl : '${acp}/accounting/template/getPaymentForm'
				}).otherwise({
					redirectTo : '/'
				});

			} ]);

			app.controller('prePayListCtrl', [ 'dataFactory', function(dataFactory) {
				var prePay = this;
				console.log(dataFactory.getPrepayList());
				dataFactory.getPrepayList().then(function(result) {
					prePay.prePayList = result.data;
					console.log(prePay.prePayList);
				});
			} ]).controller('addPrePayCtrl', [ 'dataFactory', '$routeParams', function(dataFactory, $routeParams) {
				var addPrePay = this;
				addPrePay.caseid = $routeParams.caseid;
			} ]);

			app.factory('dataFactory', [ '$http', function($http) {
				return {
					getPrepayList : function() {
						return $http.get('${acp}/rest/accounting/getPrePaidWorkList.json').then(function(data) {
							return data;
						});
					}
				}
			} ]);

		})(); */
	</script>

</body>
</html>