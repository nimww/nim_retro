<%@ include file="../jstl_tags.jsp"%>
<c:set var="acp" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html data-ng-app="">
<head>
<title>NextImage Dashboard</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

</head>
<body>
	<div class="container">
		<div class="row">
			<h3>PT Billing Test</h3>
		</div>
	</div>

	<div class="container">
		<div class=row"">
			<div class="col-sm-12">
				<table class="table table-condensed">
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Address</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Payer ID</th>
						<th>Date of Service</th>
						<th>Authorization</th>
						<th>List of Service</th>
						<th>Provider State</th>
					</tr>
					<c:forEach var="i" items="${pt }">
						<tr>
							<td>${i.firstName}</td>
							<td>${i.lastName}</td>
							<td>${i.address }</td>
							<td>${i.city }</td>
							<td>${i.state }</td>
							<td>${i.zip }</td>
							<td>${i.payerId }</td>
							<td>${i.dateOfService }</td>
							<td>${i.authorization }</td>
							<td><ul><c:forEach var="s" items="${i.listOfService}"><li>${s}</li></c:forEach></ul></td>
							<td>${i.providerState}</td>
						</tr>
					</c:forEach>


				</table>
			</div>
		</div>
	</div>

</body>
</html>